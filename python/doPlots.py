from ROOT import TH1D, TH2D, THStack, TCanvas, TGraphErrors, TLegend, TLatex, TFile, gROOT, gStyle, kRed, kBlue, kGreen, kCyan, kOrange, kMagenta, kOpenCircle, kOpenTriangleUp, kOpenTriangleDown, kOpenSquare, kBlack, TPaveText, TPad, TGaxis, kOpenCross, kOpenDiamondCross, kFullTriangleUp, kFullTriangleDown, kFullSquare, kFullCross, kYellow, kAzure, Double, TArrow, TLine
from importlib import import_module
import numpy as np

eta_edges_rebin = np.array([-2.616, -2.068, -1.52, -1.37 , -0.822, -0.274,  0.274,  0.822,  1.37, 1.52, 2.068, 2.616])
eta_edges       = np.array([-2.516, -2.267, -2.018, -1.769, -1.52,
                            -1.37      , -1.12090909, -0.87181818, -0.62272727, -0.37363636,
                            -0.12454545,  0.12454545,  0.37363636,  0.62272727,  0.87181818,
                            1.12090909,  1.37, 1.52,
                            1.769, 2.018, 2.267, 2.516])

output_plots_dir='~/project/plots_VH'
ratio_min=0.5
ratio_max=1.5

lum = {}
lum['data15'] = 3.2
lum['data16'] = 32.9
lum['data1516'] = 36.21
lum['data17'] = 43.59
lum['data1617'] = 77.2
lum['data151617'] = 79.8
lum['data18'] = 58.45
lum['data15161718'] = 138.25

#--------------------------------------------------------------------------------
def fillhist(pthist, ptarray, scaleF):
    minH = pthist.GetBinLowEdge(1)
    maxH = pthist.GetBinLowEdge(pthist.GetNbinsX())
    thMax = pthist.GetBinWidth(pthist.GetNbinsX())
    for v in ptarray:
        if v<minH:
            pthist.Fill(minH)
        elif v>maxH+thMax:
            pthist.Fill(maxH)
        else:            
            pthist.Fill(v*scaleF)
    # return pthist
            
def fillhistwg(pthist, ptarray, wgarray, scaleF):
    minH  = pthist.GetBinLowEdge(1)
    maxH  = pthist.GetBinLowEdge(pthist.GetNbinsX())
    thMax = pthist.GetBinWidth(pthist.GetNbinsX())
    for v, wg in zip(ptarray, wgarray):
        if v<minH:
            pthist.Fill(minH, wg)
        elif v>maxH+thMax:
            pthist.Fill(maxH, wg)
        else:          
            pthist.Fill(v*scaleF, wg)
    # return pthist
def fill2Dhist(pthist, ptarray, etaarray, scaleF):
    minH = pthist.GetXaxis().GetBinLowEdge(1)
    maxH  = pthist.GetXaxis().GetBinLowEdge(pthist.GetNbinsX())
    thMax = pthist.GetXaxis().GetBinWidth(pthist.GetNbinsX())
    for v, eta in zip(ptarray, etaarray):
        if v<minH:
            pthist.Fill(minH, eta)
        elif v>maxH+thMax:
            pthist.Fill(maxH, eta)
        else:            
            pthist.Fill(v*scaleF, eta)
    # return pthist
            
def fill2Dhistwg(pthist, ptarray, etaarray, wgarray, scaleF):
    minH = pthist.GetXaxis().GetBinLowEdge(1)
    maxH  = pthist.GetXaxis().GetBinLowEdge(pthist.GetNbinsX())
    thMax = pthist.GetXaxis().GetBinWidth(pthist.GetNbinsX())
    for v, eta, wg in zip(ptarray, etaarray, wgarray):
        if v<minH:
            pthist.Fill(minH, eta, wg)
        elif v>maxH+thMax:
            pthist.Fill(maxH, eta, wg)
        else:          
            pthist.Fill(v*scaleF, eta, wg)
    # return pthist

def fillHistsStack(hists_stack, data_stack, names_stack, var_name, var, wg_name, weight=True):
    for name, df1 in zip(names_stack, data_stack):
        # print("len(df) = {} name = {}, weight = {}".format(len(df1.index), name, weight))
        if '_eta' not in var_name:
            h10 = TH1D(name+"{}".format(var_name),";{}".format(var.label), var.bins, var.min, var.max)
        else:
            edges = eta_edges
            if 'rebin' in var_name:
                edges = eta_edges_rebin
            nbins = len(edges) - 1
            h10 = TH1D(name+"{}".format(var_name),";{}".format(var.label), nbins, edges)

        # print("{}".format(h10))
        # print("len(v) = {}".format(len(vv_v)))
        # print("len(wg) = {}".format(len(wg_v)))
        # print("filling...")
        if len(df1.index) != 0:
            if var.var in df1:
                if weight:
                    fillhistwg(h10, df1["{}".format(var.var)], df1[wg_name], var.scaleFactor) 
                else:
                    fillhist(h10, df1["{}".format(var.var)], var.scaleFactor) 
            else:
                print("[doPlots::fillHistsStack] var: {} not in df: {}".format(var_name, name))
        hists_stack.append(h10.Clone())
        # print("{}".format(h10))

    # print("[doPlotsROOT::fillHistsStack] end")
    
#--------------------------------------------------------------------------------
#
#--------------------------------------------------------------------------------
def craeteDfListForStack(data, df_stack, labels_stack, names_stack):
     # 1,2 anti-tau
    nAntiTau = [1,2]
    for i in range(2):
        df = data.copy()
        df = df[df['nAntiObj'] == nAntiTau[i]]
        df = df[df['nAntiTau'] == nAntiTau[i]]
        if not df.empty:
            df_stack.append(df.copy())
            labels_stack.append("#bar{#tau}_%i"%(nAntiTau[i]))
            names_stack.append("hs_fakes_antiTau_{}_".format(nAntiTau[i]))

    # 1,2 anti-lep
    nAntiLep = [1,2]
    for i in range(2):
        df = data.copy()
        df = df[(df["nAntiObj"] == nAntiLep[i])]
        df = df[(df["nAntiLep"] == nAntiLep[i])] 
        if not df.empty:
            df_stack.append(df.copy())
            labels_stack.append("#bar{lep}_%i"%(nAntiLep[i]))
            names_stack.append("hs_fakes_antiLep_{}_".format(nAntiLep[i]))

    # 1,2 anti-tau + 1 anti-lep
    for i in range(2):
        df = data.copy()
        df = df[(df["nAntiObj"] == nAntiTau[i]+1)]
        df = df[(df["nAntiTau"] == nAntiTau[i])]
        if not df.empty:
            df_stack.append(df.copy())
            labels_stack.append("#bar{#tau}_%i_#bar{lep}_1"%(nAntiTau[i]))
            names_stack.append("hs_fakes_antiTau_{}_antiLep_1_".format(nAntiTau[i]))

    # 1 anti-tau + 2 anti-lep
    for i in range(1):
        df = data.copy()
        df = df[(df["nAntiObj"] == 3)]
        df = df[(df["nAntiTau"] == 1)]
        df = df[(df["nAntiLep"] == 2)] #should be not necessary
        if not df.empty:
            df_stack.append(df.copy())
            labels_stack.append("#bar{#tau}_1_#bar{lep}_2")
            names_stack.append("hs_fakes_antiTau_1_antiLep_2_")

    # 3 anti-tau
    for i in range(1):
        df = data.copy()
        df = df[(df["nAntiObj"] == 3)]
        df = df[(df["nAntiTau"] == 3)]
        if not df.empty:
            df_stack.append(df)
            labels_stack.append("#bar{#tau}_3")
            names_stack.append("hs_fakes_antiTau_3_")

    # 3 anti-tau
    for i in range(1):
        df = data.copy()
        df = df[(df["nAntiObj"] == 3)]
        df = df[(df["nAntiLep"] == 3)]
        if not df.empty:
            df_stack.append(df.copy())
            labels_stack.append("#bar{lep}_3")
            names_stack.append("hs_fakes_antiLep_3_")

    # 4 antiobj in total: 2,1 anti-tau + 2,3 anti-lep
    nAntiLep[0] = 2
    nAntiLep[1] = 3
    nAntiTau[0] = 2
    nAntiTau[1] = 1
    # nAntiLep = [2,3]
    # nAntiTau = [2,1]

    for i in range(2):
        df = data.copy()
        df = df[(df["nAntiObj"] == 4)]
        df = df[(df["nAntiLep"] == nAntiLep[i])]
        df = df[(df["nAntiTau"] == nAntiTau[i])]
        if not df.empty:
            df_stack.append(df.copy())
            labels_stack.append("#bar{#tau}_%i_#bar{lep}_%i"%(nAntiTau[i], nAntiLep[i]))
            names_stack.append("hs_fakes_antiTau_{}_antiLep_{}_".format(nAntiTau[i], nAntiLep[i]))

    # #debug stream
    # for i in range(1):
    #     df = data.copy()
    #     df = df[(df["lep_tau_eta"] <  0.12)]
    #     df = df[(df["lep_tau_eta"] > -0.12)]
    #     if not df.empty:
    #         df_stack.append(df.copy())
    #         labels_stack.append("lep_{#tau}_debug")
    #         names_stack.append("hs_fakes_debug")

#-------------------------------------------------------------------------------
def paveText(text, left=0.15, right=0.5, bottom=0.9, top=0.94, size=0.045, align=13):
    pt = TPaveText(left, bottom, right, top, "NDC")
    pt.SetBorderSize(0)
    pt.SetFillStyle(0)
    pt.SetLineStyle(0)
    
    text = pt.AddText(text)
    text.SetTextSize(size)
    text.SetTextAlign(align)
    
    return pt

#--------------------------------------------------------------------------------
def plot_cutflow(vv, vfake, vlabels, tag, channel):
    gROOT.SetBatch()
    gROOT.ProcessLine("{gErrorIgnoreLevel = kWarning;}")
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    colours = [kRed, kBlue, kGreen, kCyan+1]
    markers = [kOpenCircle, kOpenTriangleUp, kOpenTriangleDown, kOpenSquare]
    
    rootFile = TFile("{}/plots_{}_{}_presel_hists.root".format(output_plots_dir, tag, channel), "UPDATE")
    fileName = rootFile.GetName()

    # name     = tag+"_"+channel+"_cutflow"
    # nameFake = tag+"_"+channel+"_Fake_cutflow"
    name     = channel+"_cutflow"
    nameFake = channel+"_Fake_cutflow"
    gr       = TH1D("{}".format(name)    , "", len(vv)   , 0, len(vv)   )
    grFake   = TH1D("{}".format(nameFake), "", len(vfake), 0, len(vfake))

    gr.SetNameTitle(name,name)
    grFake.SetNameTitle(nameFake, nameFake)
    
    gr.SetMarkerColor(colours[0])
    gr.SetLineColor(colours[0])
    grFake.SetMarkerColor(colours[1])
    grFake.SetLineColor(colours[1])
    gr.SetMarkerStyle(markers[0])
    grFake.SetMarkerStyle(markers[1])

    if len(vv) != len(vfake):
        print("[doPlotsROOT::plot_cutlflow] data cutflow and fake cutflow have different lenght: vData = {}, vFake = {}".format(len(vv),len(vfake)))
        return

    for i in range(0,len(vv)):
        gr.SetBinContent(i+1, vv[i]) 
        gr.SetBinError(i+1, 0)
        gr.GetXaxis().SetBinLabel(i+1, vlabels[i])
        grFake.SetBinContent(i+1, vfake[i]) 
        grFake.SetBinError(i+1, 0)
        grFake.GetXaxis().SetBinLabel(i+1, vlabels[i])
        
    
    c = TCanvas("c_"+name, "c_"+name, 800, 700)
    c.SetLeftMargin(0.15)
    c.SetBottomMargin(0.10)
    c.SetRightMargin(0.05)
    c.SetTopMargin(0.05)
    c.SetLogy()
    
    gr.GetYaxis().SetTitle("Events")
    gr.Draw("P")
    grFake.Draw("Psame")
    
    atlas_label = paveText("#font[72]{ATLAS} #font[42]{Preliminary}")
    atlas_label.Draw()
      
    tar = tag+", "+channel #"data15, ZHhadhad"
    extra_labels = [tar]

    latex = TLatex()
    latex.SetNDC()
    latex.SetTextFont(43)
    latex.SetTextSize(24)
    latex.SetTextAlign(31)
    th = 0.04
    tx = 0.90
    #ty = 1.0-0.15-0.055
    ty = 1.0-0.05-0.055
    for l in extra_labels:
        latex.DrawLatex(tx,ty,l)
        ty-=th
    
    legend = TLegend(0.2, 0.96, 0.9, 1)
    legend.SetNColumns(2)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.SetTextFont(43)
    legend.SetTextSize(20)
    
    labels = ["data","fakes"]
    graphs = [gr, grFake]
    for i,h in enumerate(graphs):
        legend.AddEntry(h, labels[i], "lp")
    legend.Draw()

    # c.Print("plots/plot_{}_{}_{}.pdf".format(tag, channel, name))
    gr.Write()
    grFake.Write()
    # c.Write()
    del c

    rootFile.Close()
    print("file: {} saved!".format(fileName));

#--------------------------------------------------------------------------------
def plot_ROOT(dfData, dfFake, tag, channel, title, tag2):
    import os
    gROOT.SetBatch()
    gROOT.ProcessLine("{gErrorIgnoreLevel = kWarning;}")
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    colours = [kRed, kBlue, kGreen, kCyan+1]
    markers = [kOpenCircle, kOpenTriangleUp, kOpenTriangleDown, kOpenSquare]
 
    vars  = import_module("plot_vars").vars

    # lum = {}
    # lum['data15'] = 3.2
    # lum['data16'] = 32.9
    # lum['data17'] = 44.3
    # lum['data1617'] = 77.2
    # lum['data151617'] = 80.4
    # lum['data18'] = 59.9

    output_dir  = '/home/gp364/project/plots_VH/{}'.format(tag2)
    if not os.path.exists("{}".format(output_dir)):
        os.makedirs("{}".format(output_dir))
    output_dir  = output_dir+'/{}'.format(channel)
    if not os.path.exists("{}".format(output_dir)):
        os.makedirs("{}".format(output_dir))

    rootFile = TFile("{}/plots_{}_{}_presel_hists.root".format(output_dir, tag, channel), "RECREATE")
    fileName = rootFile.GetName()
    
    
    dfList  = [dfData, dfFake]
    labels  = ["data","fakes"]

    c  = TCanvas("c_", "c_"  , 800, 900)
    c.SetLeftMargin(0.15)
    c.SetBottomMargin(0.15)
    c.SetRightMargin(0.05)
    c.SetTopMargin(0.05)
    #
    cn = TCanvas("cn_", "cn_", 800, 700)
    cn.SetLeftMargin(0.15)
    cn.SetBottomMargin(0.10)
    cn.SetRightMargin(0.05)
    cn.SetTopMargin(0.05)
 

    for var_name, var in vars.items():
        hists = []
        # print("[doPlotsROOT::plot_ROOT] var = {}".format(var_name))
        if var.ratio == True:
            c.cd()    
            c.SetGridy() # vertical grid
        else:
            cn.cd()
            cn.SetLogy(var.logy)
            cn.SetGridy()

        for i, data in enumerate(dfList):
            if data.empty:
                continue
            
            if var.var not in data:
                print("[doPlotsROOT::plot_ROOT_stack] var: {} not found in dataframe".format(var.var))
                continue

            #create the histogram
            if '_eta' not in var_name:
                h= TH1D("{}_{}".format(labels[i],var_name), ";{}".format(var.label), var.bins, var.min, var.max)
            else:
                edges = eta_edges
                if 'rebin' in var_name:
                    edges = eta_edges_rebin
                nbins = len(edges) - 1
                h= TH1D("{}_{}".format(labels[i],var_name), ";{}".format(var.label), nbins, edges)
            h.Sumw2()
        
            #fill the histogram
            if var_name != "weight_ff":
                if 'nFakes' in var_name: # == "nFakes":
                    #get the list of the event ids w/o duplicates
                    dfcopy = data.copy()
                    dfcopy = dfcopy.groupby(['eventId']).size().reset_index(name='count')
                    fillhist(h, dfcopy["count"], var.scaleFactor)
                else:
                    if var.weight == '':
                        fillhist(h, data["{}".format(var.var)], var.scaleFactor) 
                    else:
                        fillhistwg(h, data["{}".format(var.var)], data["{}".format('weight_ff')], var.scaleFactor) 
            else:
                fillhist(h, data["{}".format(var.var)], var.scaleFactor) 
            
            if var.blind:
                #the following assumes constant binning of the histogram
                binBlindMin = int( (var.blindMin - h.GetXaxis().GetBinLowEdge(1))/h.GetBinWidth(1) )
                binBlindMax = int( (var.blindMax - h.GetXaxis().GetBinLowEdge(1))/h.GetBinWidth(1) )
                for k in range(binBlindMin, binBlindMax):
                    if k <= h.GetNbinsX():
                        h.SetBinContent(k+1, 0)

            h.GetXaxis().SetTitle(var.label.replace('.', ' '))
            h.GetXaxis().SetTitleSize(0.042)
            h.GetXaxis().SetLabelSize(0.037)
            h.GetXaxis().SetTitleOffset(1.2)
            
            binSize = h.GetXaxis().GetBinWidth(1)
            
            h.GetYaxis().SetTitle("Entries/{:0.2f}".format(binSize));
            h.GetYaxis().SetTitleSize(0.042)
            h.GetYaxis().SetLabelSize(0.037)
            
            h.SetDirectory(0)
            hists.append(h)
            
            rootFile.cd()
            h.Write()

        if var.ratio == True:
            # Upper plot will be in pad1
            pad1_name = "pad1_{}".format(var_name)
            if var.rebin:
                pad1_name = pad1_name + "_rebinned"
            pad1 = TPad(pad1_name, "pad1", 0, 0.3, 1, 1.0);
            # pad1.SetBottomMargin(0); # Upper and lower plot are joined
            pad1.SetLeftMargin(0.15)
            pad1.SetBottomMargin(0.) #10)
            pad1.SetRightMargin(0.05)
            pad1.SetTopMargin(0.05)
            # Pad1.SetGridx();         # Vertical grid
            pad1.Draw();             # Draw the upper pad: pad1
            pad1.cd();               # pad1 becomes the current pad

        #search for the min and max y
        miny = float('inf')
        maxy = -float('inf')
        for h in hists:
            miny = min(miny, h.GetBinContent(h.GetMinimumBin()))
            maxy = max(maxy, h.GetBinContent(h.GetMaximumBin()))
            miny = max(miny, 1e-6)
        
        for i,h in enumerate(hists):
            h.SetLineColor(colours[i])
            h.SetLineWidth(2)
            h.SetMarkerStyle(markers[i])
            h.SetMarkerColor(colours[i])
            h.SetMinimum(miny)
            if var.logy:
                h.SetMaximum((maxy/miny)**1.5 * miny)
            else:
                h.SetMaximum(1.5 * maxy)
            
            h.Draw("E0 same")
        
      
        atlas_label = paveText("#font[72]{ATLAS} #font[42]{Preliminary}")
        atlas_label.Draw()
        lumi_label = paveText("#font[42]{#sqrt{s}=13 TeV, #int L dt= %2.1f fb^{-1}}"%(lum[tag]), bottom=0.84, top=0.85, size=0.04)
        lumi_label.Draw()
        tar = tag #+", "+channel #"data15, ZHhadhad"
        #        sel = var.weight
        extra_labels = [tar, title]
       
        latex = TLatex()
        latex.SetNDC()
        latex.SetTextFont(43)
        latex.SetTextSize(24)
        latex.SetTextAlign(31)
        th = 0.04
        tx = 0.90
        #ty = 1.0-0.15-0.055
        ty = 1.0-0.05-0.055
        for l in extra_labels:
            latex.DrawLatex(tx,ty,l)
            ty-=th
        
        #legend = TLegend(0.7, 0.83, 0.935, 0.935)
        # legend = TLegend(0.2, 0.96, 0.9, 1)
        legend = TLegend(0.66, 0.60, 0.935, 0.805)
        # legend.SetNColumns(2)
        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        legend.SetFillStyle(0)
        legend.SetTextFont(43)
        legend.SetTextSize(20)

        for i,h in enumerate(hists):
            if i ==0: 
                legend.AddEntry(h, labels[i]+": %6.2f"%(h.GetEntries()), "lp")
            if i == 1:
                legend.AddEntry(h, labels[i]+": %6.2f"%(h.Integral()), "lp")
        legend.Draw()
        
        if var.ratio == True:
            c.cd()          # Go back to the main canvas before defining pad2
            pad2_name = "pad2_{}".format(var_name)
            if var.rebin:
                pad2_name = pad1_name + "_rebinned"
            pad2 = TPad(pad2_name, "pad2", 0, 0.05, 1, 0.3)

            pad2.SetLeftMargin(0.15)
            pad2.SetBottomMargin(0.35)
            pad2.SetRightMargin(0.05)
            pad2.SetTopMargin(0.0)
            #        pad2.SetTopMargin(0)
            #pad2.SetBottomMargin(0.2)
            pad2.SetGridy() # vertical grid
            pad2.Draw()
            pad2.cd()
            # Define the ratio plot
            # print("{}".format(hists[0]))
            if hists[0]:
                try:
                    h3 = hists[0].Clone("h3_{}_{}_{}".format(tag, channel, var_name))             
                    h3.SetLineColor(kBlack)
                    h3.SetMarkerColor(kBlack)
                    h3.SetMinimum(0.)  # Define Y ..
                    h3.SetMaximum(2.) # .. range         
                    #        h3.Sumw2()
                    h3.SetStats(0)      # No statistics on lower plot
                    if len(hists) > 1:
                        h3.Divide(hists[1])
                    h3.SetMarkerStyle(21)
                    h3.GetYaxis().SetTitle("Data/fakes");
                    h3.GetYaxis().SetTitleSize(.13) #0.042)
                    h3.GetYaxis().SetLabelSize(.12) #0.037)
                    h3.GetYaxis().SetNdivisions(8)
                    h3.GetYaxis().SetTitleOffset(.38)
                    #
                    h3.GetXaxis().SetTitle(var.label.replace('.', ' '))
                    h3.GetXaxis().SetTitleSize(.15) #0.042)
                    h3.GetXaxis().SetLabelSize(.12) #0.037)
                    h3.GetXaxis().SetTitleOffset(1.)
                    #
                    #createdH3=True
                    h3.Draw("ep")
                except:
                    print("[doPlotsROOT::plot_ROOT_fromTFile] hist[0] = {} not defined?".format(hists[0]))


        c.Print("{}/plot_{}_{}_{}_presel.pdf".format(output_dir, tag, channel, var_name))
        c.Write()

        # c.Print("plots/plot_{}_{}_{}_presel.pdf".format(tag, channel, var.var))
        # c.Write()
        # del c
    
    rootFile.Close()
    print("file: {} saved!".format(fileName));



#--------------------------------------------------------------------------------
def plot_ROOT_fromTFile(input_dir, output_dir, fName, tag, channel, title, outfname):
    gROOT.SetBatch()
    gROOT.ProcessLine("{gErrorIgnoreLevel = kWarning;}")
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    colours = [kRed, kBlue, kGreen, kCyan+1]
    markers = [kOpenCircle, kOpenTriangleUp, kOpenTriangleDown, kOpenSquare]
 
    vars  = import_module("plot_vars").vars

    rootFile = TFile(output_dir+"/plots_{}_{}_{}_hists.root".format(outfname, tag, channel), "RECREATE")
    fileName = rootFile.GetName()
    
    inputFile = TFile.Open(fName)
    print("input file: {}".format(inputFile))
    labels  = ["data","fakes"]
    # lum = {}
    # lum['data15'] = 3.2
    # lum['data16'] = 32.9
    # lum['data17'] = 44.3
    # lum['data1617'] = 77.2
    # lum['data151617'] = 80.4
    # lum['data18'] = 59.9

    yScale = [1.8, 1.5]
    c  = TCanvas("c_"+outfname+tag, "c_", 800, 900) #700)
    cn =  TCanvas("cn_"+outfname+tag, "cn_", 800, 700)
    print("opened canvas c = {}".format(c))
    #    legend = TLegend(0.7, 0.83, 0.935, 0.935)
    legend = TLegend(0.7, 0.78, 0.935, 0.885)
    # legend = TLegend(0.2, 0.96, 0.9, 1)
    #    legend.SetNColumns(2)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.SetTextFont(43)
    legend.SetTextSize(20)
 
    hists = []
    for var_name, var in vars.items():
        hists.clear()  
        gROOT.Reset();
        if var.ratio == True:
            c.cd()    
            c.SetGridy() # vertical grid

        else:
            cn.SetLeftMargin(0.15)
            cn.SetBottomMargin(0.10)
            cn.SetRightMargin(0.05)
            cn.SetTopMargin(0.05)
            cn.cd()
            cn.SetLogy(var.logy)
            cn.SetGridy()
            #        print("step 1")

        for label  in labels:
            print("label = {} var_name = {}".format(label, var_name))
            #create the histogram
            try:
                h= inputFile.Get("{}_{}".format(label,var_name)) #, ";{}".format(var.label), var.bins, var.min, var.max)            
            except:
                print("h = {}_{} not found?".format(label,var_name))
                continue
            # h.Sumw2()
            if 'nFakes' in var_name and label=='data':
                continue
            #fill the histogram
            # fillhistwg(h, data["{}".format(var_name)], data["{}".format("weight_ff")]) 
            if var.blind and h.GetEntries() > 0:
                #the following assumes constant binning of the histogram
                binBlindMin = int( (var.blindMin - h.GetXaxis().GetBinLowEdge(1))/h.GetBinWidth(1) )
                binBlindMax = int( (var.blindMax - h.GetXaxis().GetBinLowEdge(1))/h.GetBinWidth(1) )
                for k in range(binBlindMin, binBlindMax):
                    if k <= h.GetNbinsX():
                        h.SetBinContent(k+1, 0)
                        h.SetBinError(k+1,0.)
            #            print("step 1.5")
            print("{}".format(h))
            h.GetXaxis().SetTitle(var.label.replace('.', ' '))
            h.GetXaxis().SetTitleSize(0.042)
            h.GetXaxis().SetLabelSize(0.037)
            h.GetXaxis().SetTitleOffset(1.2)
            #            try:
                #print("min = {} max = ={}".format(var.min, var.max))
            print("{}".format(h.GetXaxis()))
            h.GetXaxis().SetRangeUser(var.min, var.max)
            #except:
            #    print("h={}".format(h))
            
            binSize = h.GetXaxis().GetBinWidth(1)
            
            h.GetYaxis().SetTitle("Entries/({:0.2f} {})".format(binSize, var.unit));
            h.GetYaxis().SetTitleSize(0.042)
            h.GetYaxis().SetLabelSize(0.037)
            
            h.SetDirectory(0)
            hists.append(h)
            #print("step 2")

            rootFile.cd()
            h.Write()

        #search for the min and max y
        miny = float('inf')
        maxy = -float('inf')
        lastBinX = -1
        for h in hists:
            miny = min(miny, h.GetBinContent(h.GetMinimumBin()))
            maxy = max(maxy, h.GetBinContent(h.GetMaximumBin()))
            lastBinX = max(lastBinX, h.FindLastBinAbove())
            # miny = max(miny, 1e-6)
        if miny<0:
            miny = miny *1.10
        # else:
        #            c.SetLogy(var.logy)
        # print("step 3")

        if var.logy and miny<0:
            if var.ratio == True:
                c.SetLogy(False)
            else:
                cn.SetLogy(False)
                
        if var.ratio == True:
            # Upper plot will be in pad1
            pad1_name = "pad1_{}".format(var_name)
            if var.rebin:
                pad1_name = pad1_name + "_rebinned"
            pad1 = TPad(pad1_name, "pad1", 0, 0.3, 1, 1.0);
            # pad1.SetBottomMargin(0); # Upper and lower plot are joined
            pad1.SetLeftMargin(0.15)
            pad1.SetBottomMargin(0.) #10)
            pad1.SetRightMargin(0.05)
            pad1.SetTopMargin(0.05)
            # Pad1.SetGridx();         # Vertical grid
            pad1.Draw();             # Draw the upper pad: pad1
            pad1.cd();               # pad1 becomes the current pad


        for i,h in enumerate(hists):
            h.SetLineColor(colours[i])
            h.SetLineWidth(2)
            h.SetMarkerStyle(markers[i])
            h.SetMarkerColor(colours[i])
            if var.logy and miny<1:
                miny=1
            h.SetMinimum(miny)

            if var.logy:
                h.SetMaximum((maxy/miny)**yScale[1] * miny)
            else:
                if miny < 0:
                    h.SetMaximum(yScale[0] * maxy)
                else:
                    h.SetMaximum(yScale[1] * maxy)
                
            # if var_name == "nFakes":
            #     if lastBinX >0:
            #         h.GetXaxis().SetRangeUser(0, h.GetBinCenter(1+lastBinX))
            if i ==0 :
                h.Draw("EO")
            else:
                h.Draw("E0 same")

        atlas_label = paveText("#font[72]{ATLAS} #font[42]{Preliminary}")
        atlas_label.Draw()
        lumi_label = paveText("#font[42]{#sqrt{s}=13 TeV, #int L dt= %2.1f fb^{-1}}"%(lum[tag]), bottom=0.84, top=0.85, size=0.04)
        lumi_label.Draw()
        tar = tag #"data15, ZHhadhad"
        #        sel = var.weight
        extra_labels = [tar, title] #channel]
       
        latex = TLatex()
        latex.SetNDC()
        latex.SetTextFont(43)
        latex.SetTextSize(24)
        latex.SetTextAlign(31)
        th = 0.04
        tx = 0.90
        #ty = 1.0-0.15-0.055
        ty = 1.0-0.05-0.055
        for l in extra_labels:
            latex.DrawLatex(tx,ty,l)
            ty-=th
        
        #legend = TLegend(0.7, 0.83, 0.935, 0.935)
        # legend = TLegend(0.2, 0.96, 0.9, 1)
        # legend.SetNColumns(2)
        # legend.SetBorderSize(0)
        # legend.SetFillColor(0)
        # legend.SetFillStyle(0)
        # legend.SetTextFont(43)
        # legend.SetTextSize(20)
        # print("step 4")
        legend.Clear()
        for i,h in enumerate(hists):
            if i ==0: 
                legend.AddEntry(h, labels[i]+": %6.2f"%(h.GetEntries()), "lp")
            if i == 1:
                legend.AddEntry(h, labels[i]+": %6.2f"%(h.Integral()), "lp")
                
        legend.Draw()

        # hists[0].GetYaxis().SetLabelSize(0.)
        # axis = TGaxis( -5, 20, -5, 220, 20,220,510,"")
        # axis.SetLabelFont(43) # Absolute font size in pixel (precision 3)
        # axis.SetLabelSize(15)
        # axis.Draw()
        # print("step 5")
        
        createdH3=False
        if var.ratio == True:
            c.cd()          # Go back to the main canvas before defining pad2
            pad2_name = "pad2_{}".format(var_name)
            if var.rebin:
                pad2_name = pad1_name + "_rebinned"
            pad2 = TPad(pad2_name, "pad2", 0, 0.05, 1, 0.3)

            pad2.SetLeftMargin(0.15)
            pad2.SetBottomMargin(0.30)
            pad2.SetRightMargin(0.05)
            pad2.SetTopMargin(0.0)
            #        pad2.SetTopMargin(0)
            # pad2.SetBottomMargin(0.2)
            pad2.SetGridy() # vertical grid
            pad2.Draw()
            pad2.cd()
            # Define the ratio plot
            print("{}".format(hists[0]))
            if hists[0]:
                try:
                    h3 = hists[0].Clone("h3_{}_{}_{}".format(tag, channel, var_name))                   
                    h3.SetLineColor(kBlack)
                    h3.SetMarkerColor(kBlack)
                    h3.SetMinimum(0.)  # Define Y ..
                    h3.SetMaximum(2.) # .. range         
                    #        h3.Sumw2()
                    h3.SetStats(0)      # No statistics on lower plot
                    if len(hists) > 1:
                        h3.Divide(hists[1])
                    h3.SetMarkerStyle(21)
                    h3.GetYaxis().SetTitle("Data/fakes");
                    h3.GetYaxis().SetTitleSize(.13) #0.042)
                    h3.GetYaxis().SetLabelSize(.12) #0.037)
                    h3.GetYaxis().SetNdivisions(8)
                    h3.GetYaxis().SetTitleOffset(.38)
                    #
                    h3.GetXaxis().SetTitle(var.label.replace('.', ' '))
                    h3.GetXaxis().SetTitleSize(.15) #0.042)
                    h3.GetXaxis().SetLabelSize(.12) #0.037)
                    h3.GetXaxis().SetTitleOffset(1.)
                    #
                    createdH3=True
                    h3.Draw("ep")
                except:
                    print("[doPlotsROOT::plot_ROOT_fromTFile] hist[0] = {} not defined?".format(hists[0]))
        print("printing /plot_{}_{}_{}_{}_presel.pdf".format(outfname, tag, channel, var_name))
        if var.ratio == True:
            c.Print(output_dir+"/plot_{}_{}_{}_{}_presel.pdf".format(outfname, tag, channel, var_name))
            if var.rebin == True:
                for h in hists:
                    h.RebinX(var.rebinFactor)
                    binSize = h.GetXaxis().GetBinWidth(1)
                    h.GetYaxis().SetTitle("Entries/({:0.2f} {})".format(binSize, var.unit));

                    if var.logy:
                        h.SetMaximum((maxy/miny)**yScale[1] * miny)
                    else:
                        if miny < 0:
                            h.SetMaximum(yScale[0] * maxy*var.rebinFactor)
                        else:
                            h.SetMaximum(yScale[1] * maxy*var.rebinFactor)
                if hists[0]:
                    h3 = hists[0].Clone("h3_{}_rebinned".format(var_name))
                    h3.SetLineColor(kBlack)
                    h3.SetMarkerColor(kBlack)
                    h3.SetMinimum(0.)  # Define Y ..
                    h3.SetMaximum(2.) # .. range         
                    #        h3.Sumw2()
                    h3.SetStats(0)      # No statistics on lower plot
                    if len(hists) > 1:
                        h3.Divide(hists[1])
                    h3.SetMarkerStyle(21)
                    h3.GetYaxis().SetTitle("Data/fakes");
                    h3.GetYaxis().SetTitleSize(.13) #0.042)
                    h3.GetYaxis().SetLabelSize(.12) #0.037)
                    h3.GetYaxis().SetNdivisions(8)
                    h3.GetYaxis().SetTitleOffset(.38)
                    #
                    h3.GetXaxis().SetTitle(var.label.replace('.', ' '))
                    h3.GetXaxis().SetTitleSize(.15) #0.042)
                    h3.GetXaxis().SetLabelSize(.12) #0.037)
                    h3.GetXaxis().SetTitleOffset(1.)
                    pad2.cd()
                    h3.Draw("ep")
                #
                pad1.Update()
                pad2.Update()
                c.Print(output_dir+"/plot_{}_{}_{}_{}_rebinned_presel.pdf".format(outfname, tag, channel, var_name))
        else:
            cn.Print(output_dir+"/plot_{}_{}_{}_{}_presel.pdf".format(outfname, tag, channel, var.var))
            if var.rebin == True:
                for h in hists:
                    h.RebinX(var.rebinFactor)
                    binSize = h.GetXaxis().GetBinWidth(1)
                    h.GetYaxis().SetTitle("Entries/({:0.2f} {})".format(binSize, var.unit))
                    if var.logy:
                        h.SetMaximum((maxy/miny)**yScale[1] * miny)
                    else:
                        if miny < 0:
                            h.SetMaximum(yScale[0] * maxy*var.rebinFactor)
                        else:
                            h.SetMaximum(yScale[1] * maxy*var.rebinFactor)
                pad1.Update()
                cn.Print(output_dir+"/plot_{}_{}_{}_{}_rebinned_presel.pdf".format(outfname, tag, channel, var_name))

        if createdH3:
            h3 = 0
            del h3
        # rootFile.cd()
        #        c.Write()
        #del c
        #del pad1
        #del pad2
    rootFile.Close()
    inputFile.Close()
    print("file: {} saved!".format(fileName));
#--------------------------------------------------------------------------------
def plot_cutflow_fromTFile(input_dir, output_dir, fName, tag, channel, title):
    gROOT.SetBatch()
    gROOT.ProcessLine("{gErrorIgnoreLevel = kWarning;}")
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    colours = [kRed, kBlue, kGreen, kCyan+1]
    markers = [kOpenCircle, kOpenTriangleUp, kOpenTriangleDown, kOpenSquare]
    
    rootFile = TFile(output_dir+"/plots_{}_cutflow_{}_presel_hists.root".format(tag, channel), "UPDATE")
    fileName = rootFile.GetName()
    # print("[plot_cutflow_fromTFile] fName = {}".format(fName))
    inputFile = TFile.Open(fName)

    # name     = tag+"_"+channel+"_cutflow"
    # nameFake = tag+"_"+channel+"_Fake_cutflow"
    name     = channel+"_cutflow"
    nameFake = channel+"_Fake_cutflow"
    gr       = inputFile.Get("{}".format(name)    )
    grFake   = inputFile.Get("{}".format(nameFake))

    gr.SetNameTitle(name,name)
    grFake.SetNameTitle(nameFake, nameFake)
    
    gr.SetMarkerColor(colours[0])
    gr.SetLineColor(colours[0])
    grFake.SetMarkerColor(colours[1])
    grFake.SetLineColor(colours[1])
    gr.SetMarkerStyle(markers[0])
    grFake.SetMarkerStyle(markers[1])

    c = TCanvas("c_"+name+tag, "c_"+name, 800, 900)# 700)
    # c.SetLeftMargin(0.15)
    # c.SetBottomMargin(0.10)
    # c.SetRightMargin(0.05)
    # c.SetTopMargin(0.05)
    # c.SetGridy()

    miny = float('inf')
    maxy = -float('inf')
    hists = [gr, grFake]
    for h in hists:
        miny = min(miny, h.GetBinContent(h.GetMinimumBin()))
        maxy = max(maxy, h.GetBinContent(h.GetMaximumBin()))
        miny = min(miny, 10)

    print("maxy = {}, miny = {}".format(maxy, miny))
    if miny < 0:
        miny = 1

    if miny>0: 
        c.SetLogy()
        gr.SetMaximum((maxy/miny)**1.2 * miny)
    else:
        gr.SetMaximum(maxy)

    pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
    # pad1.SetBottomMargin(0); # Upper and lower plot are joined
    pad1.SetLeftMargin(0.15)
    pad1.SetBottomMargin(0.) #10)
    pad1.SetRightMargin(0.05)
    pad1.SetTopMargin(0.05)
    pad1.SetGridy();         # Vertical grid
    pad1.SetLogy()

    pad1.Draw();             # Draw the upper pad: pad1
    pad1.cd();               # pad1 becomes the current pad
    
    gr.SetMinimum(miny)
    gr.GetYaxis().SetTitle("Events")
    gr.Draw("P")
    grFake.Draw("Psame")
    
    atlas_label = paveText("#font[72]{ATLAS} #font[42]{Preliminary}")
    atlas_label.Draw()
      
    tar = tag  #"data15, ZHhadhad"
    extra_labels = [tar, title] #channel]

    latex = TLatex()
    latex.SetNDC()
    latex.SetTextFont(43)
    latex.SetTextSize(24)
    latex.SetTextAlign(31)
    th = 0.04
    tx = 0.90
    #ty = 1.0-0.15-0.055
    ty = 1.0-0.05-0.055
    for l in extra_labels:
        latex.DrawLatex(tx,ty,l)
        ty-=th
    
    legend = TLegend(0.2, 0.96, 0.9, 1)
    legend.SetNColumns(2)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.SetTextFont(43)
    legend.SetTextSize(20)
    
    labels = ["data","fakes"]
    graphs = [gr, grFake]
    legend.AddEntry(gr, labels[0]+": %6.2f"%(gr.GetEntries()), "lp")
    legend.AddEntry(grFake, labels[1]+": %6.2f"%(grFake.Integral()), "lp")
        # for i,h in enumerate(graphs):
        # legend.AddEntry(h, labels[i], "lp")
    legend.Draw()

    c.cd()          # Go back to the main canvas before defining pad2
    pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
    pad2.SetLeftMargin(0.15)
    pad2.SetBottomMargin(0.30)
    pad2.SetRightMargin(0.05)
    pad2.SetTopMargin(0.0)
    #        pad2.SetTopMargin(0)
    # pad2.SetBottomMargin(0.2)
    pad2.SetGridy() # vertical grid
    pad2.Draw()
    pad2.cd()

    # Define the ratio plot
    h3 = gr.Clone("h3")
    h3.SetLineColor(kBlack)
    h3.SetMarkerColor(kBlack)
    h3.SetMinimum(0.)  # Define Y ..
    h3.SetMaximum(2.) # .. range
    
        #        h3.Sumw2()
    h3.SetStats(0)      # No statistics on lower plot
    h3.Divide(grFake)
    h3.SetMarkerStyle(21)
    h3.GetYaxis().SetTitle("Data/fakes");
    h3.GetYaxis().SetTitleSize(.13) #0.042)
    h3.GetYaxis().SetLabelSize(.12) #0.037)
    h3.GetYaxis().SetNdivisions(8)
    h3.GetYaxis().SetTitleOffset(.38)

    #h3.GetXaxis().SetTitle(var.label.replace('.', ' '))
    h3.GetXaxis().SetTitleSize(.15) #0.042)
    h3.GetXaxis().SetLabelSize(.12) #0.037)
    h3.GetXaxis().SetTitleOffset(1.)
    
    h3.Draw("ep")

    rootFile.cd()
    print("printing /plot_{}_{}.pdf".format(tag, name))
    c.Print(output_dir+"/plot_{}_{}.pdf".format(tag, name))
    gr.Write()
    grFake.Write()
    c.Write()
    #    del c

    rootFile.Close()
    print("file: {} saved!".format(fileName));
    inputFile.Close()



#--------------------------------------------------------------------------------
def plot_ROOT_stack(dfData, dfMCSignal, dfFake, 
                    df_bkg_stack, names_stack, labels_stack, 
                    tag, channel, title, tag2, sel_region, name_tag='stack', add_sys=False):
    import os
    gROOT.SetBatch()
    gROOT.ProcessLine("{gErrorIgnoreLevel = kWarning;}")
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    colours = [kRed, kBlue, kGreen, kCyan+1, kOrange, kMagenta, kGreen+2, kYellow, kBlue+2, kOrange-8, kBlack,  kMagenta+2]
    markers = [kOpenCircle, kOpenTriangleUp, kOpenTriangleDown, kOpenSquare, 
               kOpenCross, kOpenDiamondCross,  kFullTriangleUp, kFullTriangleDown, kFullSquare, kFullCross]
 
    vars  = import_module("plot_vars").vars
    output_dir  = '{}'.format(tag2)+'/stack_hists'
    if not os.path.exists("{}".format(output_dir)):
        os.makedirs("{}".format(output_dir))
    output_dir  = output_dir +'/{}'.format(sel_region)
    if not os.path.exists("{}".format(output_dir)):
        os.makedirs("{}".format(output_dir))

    rootFile = TFile("{}/plots_{}_presel_hists_{}.root".format(output_dir, channel, name_tag), "RECREATE")
    fileName = rootFile.GetName()
    if channel == 'etau':
        wg_mc = 'totalWeight_Ele'
    elif channel == 'mutau':
        wg_mc = 'totalWeight_Mu'
    wg_mc_sys0 = wg_mc+"_sys0"
    wg_mc_sys1 = wg_mc+"_sys1"
    # lum = {}
    # lum['data15'] = 3.2
    # lum['data16'] = 32.9
    # lum['data1516'] = 36.21
    # lum['data17'] = 43.59
    # lum['data1617'] = 77.2
    # lum['data151617'] = 79.8
    # lum['data18'] = 58.45
    # lum['data15161718'] = 138.25

    yScale = [1.8, 1.7, 1.15]

    dfList  = [dfData, dfFake, dfMCSignal]
    # labels  = ["Data","Bkg", "Signal[ggF+VBF+VH]#times10"]
    labels  = ["Data","Bkg", "Signal#times10"]

    #    craeteDfListForStack(dfFake, df_bkg_stack, labels_stack, names_stack)

    c  = TCanvas("c_stack"+name_tag, "c_stack"+name_tag  , 800, 900)
    c.SetLeftMargin(0.15)
    c.SetBottomMargin(0.15)
    c.SetRightMargin(0.05)
    c.SetTopMargin(0.05)
    #
    cn = TCanvas("cn_stack"+name_tag, "cn_stack"+name_tag, 800, 700)
    cn.SetLeftMargin(0.15)
    cn.SetBottomMargin(0.10)
    cn.SetRightMargin(0.05)
    cn.SetTopMargin(0.05)

    printIntegrals = True
    for var_name, var in vars.items():
        # if "onetau" in var_name or "twotau" in var_name:
        #     continue
        hists = []
        # print("[doPlotsROOT::plot_ROOT] var = {}".format(var_name))
        # c.SetLogy(var.logy)
        if var.ratio == True:
            c.cd()    
            c.SetGridy() # vertical grid
        else:
            cn.cd()
            cn.SetLogy(var.logy)
            cn.SetGridy()

        if len(dfList)>0:
            data = dfList[0]
            mc   = dfList[1]
            if var.var not in data and  var.var not in mc:
                print("[doPlotsROOT::plot_ROOT_stack] var: {} not found in dataframe".format(var.var))
                continue
        hs = THStack("stack_{}_{}_{}".format(labels[1],var_name, name_tag),
                     "stack_{}_{}_{}".format(labels[1],var_name, name_tag))
        hists_stack = []
        # print("[plot_ROOT_stack] start filling variable: {}, rebin={}".format(var.var, var.rebin))

        #        for i, data in enumerate(dfList):
        for i in range(len(dfList)):
            data = dfList[i].copy()
            if data.empty:
                continue
            
            #create the histogram
            # print("[plot_ROOT_stack] step 0: a {}, {}".format(i, var.var))
            hname = "{}_{}_{}".format(labels[i],var_name,name_tag)
            if var.rebin:
                hname = hname + "_rebinned"
            if '_eta' not in var_name:
                h= TH1D(hname,
                        ";{}".format(var.label), var.bins, var.min, var.max)
                if add_sys and labels[i] == labels[1]:
                    h_sys0 = TH1D(hname+"_sys0",
                                  ";{}".format(var.label), var.bins, var.min, var.max)
                    h_sys1 = TH1D(hname+"_sys1",
                                  ";{}".format(var.label), var.bins, var.min, var.max)
            else:
                edges = eta_edges
                if 'rebin' in var_name:
                    edges = eta_edges_rebin
                nbins = len(edges) - 1
                h= TH1D(hname, ";{}".format(var.label), nbins, edges)
                if add_sys and labels[i] == labels[1]:
                    h_sys0 = TH1D(hname+"_sys0", ";{}".format(var.label), nbins, edges)
                    h_sys1 = TH1D(hname+"_sys1", ";{}".format(var.label), nbins, edges)
                
            h.Sumw2()
            if add_sys and labels[i] == labels[1]:
                h_sys0.Sumw2()
                h_sys1.Sumw2()
            
            # print("[plot_ROOT_stack] step 0: b {} {}".format(i, h))
            #fill the histogram

            if var.var in data:
                if labels[i] == labels[0]: #'Data':
                    fillhist(h, data["{}".format(var.var)], var.scaleFactor) 
                elif labels[i] == labels[1]: #"Bkg":
                    fillhistwg(h, data["{}".format(var.var)], data[wg_mc], var.scaleFactor)
                    fillHistsStack(hists_stack, df_bkg_stack, names_stack, var_name, var, wg_mc)
                    if add_sys:
                        fillhistwg(h_sys0, data["{}".format(var.var)], data[wg_mc_sys0], var.scaleFactor)
                        fillhistwg(h_sys1, data["{}".format(var.var)], data[wg_mc_sys1], var.scaleFactor)
                        # if var_name == "ditau_vis_mass":
                        #     print("[doPlots::make_stack]       pT         evts   ")     
                        #     for l in range(h_sys.GetNbinsX()):
                        #         print("[doPlots::make_stack] {:10.2f} {:10.2f}".format(h_sys.GetBinCenter(l+1), h_sys.GetBinContent(l+1)))
                elif  labels[i] == labels[2]: #"MC signal":
                    fillhistwg(h, data["{}".format(var.var)], data[wg_mc], var.scaleFactor)
            
                if var.blind:
                    #the following assumes constant binning of the histogram
                    binBlindMin = int( (var.blindMin - h.GetXaxis().GetBinLowEdge(1))/h.GetBinWidth(1) )
                    binBlindMax = int( (var.blindMax - h.GetXaxis().GetBinLowEdge(1))/h.GetBinWidth(1) )
                    for k in range(binBlindMin, binBlindMax):
                        if k <= h.GetNbinsX():
                            h.SetBinContent(k+1, 0)

            # print("[plot_ROOT_stack] step 1: {}, {}".format(i, var.var))
            # print("{}".format(h))
            h.GetXaxis().SetTitle(var.label.replace('.', ' '))
            h.GetXaxis().SetTitleSize(0.042)
            h.GetXaxis().SetLabelSize(0.037)
            h.GetXaxis().SetTitleOffset(1.2)
            
            binSize = h.GetXaxis().GetBinWidth(1)
            
            h.GetYaxis().SetTitle("Entries/({:0.2f} {})".format(binSize, var.unit))
            h.GetYaxis().SetTitleSize(0.042)
            h.GetYaxis().SetLabelSize(0.037)
            # print("[plot_ROOT_stack] step 1: a")

            h.SetDirectory(0)
            hists.append(h.Clone())
            # print("[plot_ROOT_stack] step 1: b {}".format(i))
                        
            rootFile.cd()
            h.Write()
            del h
            del data
            # print("[plot_ROOT_stack] step 1: c {}".format(i))

        # print("[plot_ROOT_stack] step 2")
        #search for the min and max y
        miny = float('inf')
        maxy = -float('inf')
        for h0 in hists:
            miny = min(miny, h0.GetBinContent(h0.GetMinimumBin()))
            maxy = max(maxy, h0.GetBinContent(h0.GetMaximumBin()))
            miny = max(miny, 1e-6)

        integrals = []
        errors    = []
        sys0Errors = []
        sys1Errors = []
        sysErrors = []
        for i, h1 in enumerate(hists_stack):
            h1.SetLineColor(colours[i+2])
            # h0.SetLineWidth(2)
            # h1.SetMarkerStyle(markers[i+2])
            # h1.SetMarkerColor(colours[i+2])
            h1.SetFillColor(colours[i+2])
            maxy = max(maxy, h1.GetBinContent(h1.GetMaximumBin()))
            miny = min(miny, h1.GetBinContent(h1.GetMinimumBin()))
            integrals.append(h1.Integral())
            err = Double(0)
            area = h1.IntegralAndError(0, h1.GetNbinsX(), err)
            errors.append(err)
            sys0 = 0.
            sys1 = 0.
            if add_sys:
                sys0   = (area - sum(df_bkg_stack[i][wg_mc_sys0]))/max(1e-10, area)
                sys1   = (area - sum(df_bkg_stack[i][wg_mc_sys1]))/max(1e-10, area)

            # print("[] area = {:6.2f} sys0 = {:6.2f} sys1 = {:6.2f}".format(area, sys0, sys1))
            sysTot = np.sqrt((err/max(1e-10,area))**2+sys0**2 + sys1**2)*area
            sys0Errors.append(sys0*area)
            sys1Errors.append(sys1*area)
            sysErrors.append(sysTot)
            hs.Add(h1)
        hs.Write()
        
        if var.logy and miny<=0:
            if var.ratio == True:
                c.SetLogy(False)
            elif miny == 0:
                miny = 1
            else:
                cn.SetLogy(False)
             
        if var.ratio == True:
            # Upper plot will be in pad1
            pad1_name = "pad1_{}".format(var_name)
            if var.rebin:
                pad1_name = pad1_name + "_rebinned"
            pad1 = TPad(pad1_name, "pad1", 0, 0.3, 1, 1.0);
            # pad1.SetBottomMargin(0); # Upper and lower plot are joined
            pad1.SetLeftMargin(0.15)
            pad1.SetBottomMargin(0.) #10)
            pad1.SetRightMargin(0.05)
            pad1.SetTopMargin(0.05)
            # Pad1.SetGridx();         # Vertical grid
            pad1.Draw();             # Draw the upper pad: pad1
            pad1.cd();               # pad1 becomes the current pad

        # print("[plot_ROOT_stack] step 3")
        h_data   = hists[0]
        h_fake   = hists[1]
        if (len(hists)>2):
            h_mc_sig = hists[2]
            h_mc_sig.SetLineColor(colours[0])
            h_mc_sig.SetLineWidth(2)
        else:
            h_mc_sig = 0
        # h_mc_sig.SetMarkerStyle(markers[0])
        # h_mc_sig.SetMarkerColor(colours[0])
        h_data.SetLineColor(colours[10])
        h_data.SetLineWidth(2)
        h_data.SetMarkerStyle(markers[9])
        h_data.SetMarkerColor(colours[10])
        h_fake.SetLineWidth(2)
        # h_fake.SetMarkerStyle(markers[1])
        # h_fake.SetMarkerColor(colours[1])
        h_fake.SetLineColor(colours[1])
        h_fake.SetFillColor(colours[1])
        h_fake.SetFillStyle(3004)
        if var.logy and miny>0:
            h_data.SetMaximum((maxy/miny)**yScale[1] * miny)
            h_data.SetMinimum(miny)
        else:
            if miny < 0:
                h_data.SetMaximum(yScale[0] * maxy)
                h_data.SetMinimum(yScale[2] * miny)
            else:
                h_data.SetMaximum(yScale[1] * maxy)
                h_data.SetMinimum(miny)

        h_data.Draw("EO")
        hs.Draw("hist same")
        #hs.Draw()
        h_fake.Draw("E2 same")
        if h_mc_sig != 0:
            h_mc_sig.Draw("hist same")
        h_data.Draw("EO same")
      
        atlas_label = paveText("#font[72]{ATLAS} #font[42]{Preliminary}")
        atlas_label.Draw()
        lumi_label = paveText("#font[42]{#sqrt{s}=13 TeV, #int L dt= %2.1f fb^{-1}}"%(lum[tag]), bottom=0.84, top=0.85, size=0.04)
        lumi_label.Draw()
        tar = tag #+", "+channel #"data15, ZHhadhad"
        #        sel = var.weight
        extra_labels = [title]
       
        latex = TLatex()
        latex.SetNDC()
        latex.SetTextFont(43)
        latex.SetTextSize(24)
        latex.SetTextAlign(31)
        th = 0.04
        tx = 0.29 #90
        #ty = 1.0-0.15-0.055
        ty = 0.75 #1.0-0.05-0.055
        for l in extra_labels:
            latex.DrawLatex(tx,ty,l)
            ty-=th
        
        #legend = TLegend(0.7, 0.83, 0.935, 0.935)
        legend = TLegend(0.66, 0.54, 0.935, 0.945)
        # legend = TLegend(0.2, 0.96, 0.9, 1)
        #        legend.SetNColumns(2)
        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        legend.SetFillStyle(0)
        legend.SetTextFont(43)
        legend.SetTextSize(20)

        legend.AddEntry(h_data, labels[0]+": %6.2f"%(h_data.GetEntries()), "lep")
        legend.AddEntry(h_fake, labels[1]+": %6.2f"%(h_fake.Integral()), "lfp")
        if printIntegrals == True:
            print("[doPlots]          bkg                  integral          error         sys0          sys1         sysTot ")       
        for i,h0 in enumerate(hists_stack):
            legend.AddEntry(h0, labels_stack[i]+": %6.2f"%(integrals[i]), "f")
            if printIntegrals == True:
                print("[doPlots] {:25s} {:14.3f} {:14.3f} {:14.3f} {:14.3f} {:14.3f}".format(labels_stack[i], integrals[i], errors[i], sys0Errors[i], sys1Errors[i], sysErrors[i]))
        printIntegrals = False

        if h_mc_sig != 0:
            legend.AddEntry(h_mc_sig, labels[2]+": %6.2f"%(h_mc_sig.Integral()), "f")
        legend.Draw()

        

        if var.ratio == True:
            c.cd()          # Go back to the main canvas before defining pad2
            pad2_name = "pad2_{}".format(var_name)
            if var.rebin:
                pad2_name = pad1_name + "_rebinned"
            pad2 = TPad(pad2_name, "pad2", 0, 0.05, 1, 0.3)

            pad2.SetLeftMargin(0.15)
            pad2.SetBottomMargin(0.35)
            pad2.SetRightMargin(0.05)
            pad2.SetTopMargin(0.0)
            #        pad2.SetTopMargin(0)
            #pad2.SetBottomMargin(0.2)
            pad2.SetGridy() # vertical grid
            pad2.Draw()
            pad2.cd()
            # Define the ratio plot
            # print("{}".format(hists[0]))
            if hists[0]:
                #try:
                h3 = hists[0].Clone("h3_{}_{}_{}".format(tag, channel, var_name))             
                h3.SetLineColor(kBlack)
                h3.SetMarkerColor(kBlack)
                h3.SetMinimum(ratio_min)  # Define Y ..
                h3.SetMaximum(ratio_max) # .. range         
                #        h3.Sumw2()
                h3.SetStats(0)      # No statistics on lower plot
                if len(hists) > 1:
                    h3.Divide(hists[1])
                h3.SetMarkerStyle(21)
                h3.GetYaxis().SetTitle("Data/Bkg");
                h3.GetYaxis().SetTitleSize(.13) #0.042)
                h3.GetYaxis().SetLabelSize(.12) #0.037)
                h3.GetYaxis().SetNdivisions(8)
                h3.GetYaxis().SetTitleOffset(.38)
                #
                h3.GetXaxis().SetTitle(var.label.replace('.', ' '))
                h3.GetXaxis().SetTitleSize(.15) #0.042)
                h3.GetXaxis().SetLabelSize(.12) #0.037)
                h3.GetXaxis().SetTitleOffset(1.)
                #
                #createdH3=True
                h3.Draw("ep")
                line = TLine(h3.GetBinLowEdge(1), 1, h3.GetBinLowEdge(h3.GetNbinsX())+h3.GetBinWidth(h3.GetNbinsX()), 1.)
                line.SetLineWidth(2)
                line.SetLineColor(kRed)
                line.Draw()
                list_arr = []
                for k in range(h3.GetNbinsX()):
                    content = h3.GetBinContent(k+1)
                    step    = 0.1
                    if hists[1].GetBinContent(k+1)<1e-6 or hists[0].GetBinContent(k+1)<1e-6: 
                        continue
                    if content < ratio_min or content> ratio_max:
                        x1 = h3.GetBinCenter(k+1)
                        x2 = x1
                        if content < ratio_min:
                            y1 = ratio_min + 2*step
                            y2 = ratio_min + 1*step
                        else:
                            y2 = ratio_max - 1*step
                            y1 = ratio_max - 2*step
                        ar = TArrow(x1, y1, x2, y2, 0.01, "|>")
                        ar.SetLineWidth(1)
                        ar.SetLineColor(kRed)
                        ar.SetFillColor(kRed)
                        ar.Draw()
                        list_arr.append(ar)

                if add_sys:
                    h4 = h_sys0.Clone("h4_{}_{}_{}_sys".format(tag, channel, var_name))     
                    h4.Add(h_fake, -1.)
                    h4.Divide(h_fake)
                    #now do the same for the second sys:
                    h5 = h_sys1.Clone("h5_{}_{}_{}_sys".format(tag, channel, var_name))     
                    h5.Add(h_fake, -1.)
                    h5.Divide(h_fake)
                h6 = hists[1].Clone("h6_{}_{}_{}".format(tag, channel, var_name))     
                for k in range(h6.GetNbinsX()):
                    sys0_err  = 0.
                    sys1_err  = 0.
                    if add_sys:
                        sys0_err  = h4.GetBinContent(k+1)
                        if np.isnan(sys0_err)==True:
                            sys0_err = 0.
                        sys1_err = h5.GetBinContent(k+1)
                        if np.isnan(sys1_err)==True:
                            sys1_err = 0.
                    stat_err = 0.
                    if h_fake.GetBinContent(k+1)>0.:
                        stat_err = h_fake.GetBinError(k+1)/h_fake.GetBinContent(k+1)
                    error    = np.sqrt(sys0_err**2+sys1_err**2+stat_err**2)
                    h6.SetBinContent(k+1, 1.)
                    h6.SetBinError(k+1, error)
                    # print("[doPlotsROOT::plot_ROOT_stack] var = {} k = {}, content = {}, sys = {:2.2f}, stat_err = {:2.2f}, error = {:2.2f}, h_sys = {:2.2f}".format(
                    #     var_name, k,  h4.GetBinContent(k+1), sys_err, stat_err, error, h_sys.GetBinContent(k+1)))
                h6.SetFillStyle(3001)
                h6.SetLineColor(kRed)
                h6.SetFillColor(kOrange)
                h6.Draw("E2 same")
                for a in list_arr:
                    a.Draw()
                line.Draw()
                h3.Draw("ep same")

               

        c.Print("{}/plot_{}_{}_{}_{}.png".format(output_dir, tag, channel, var_name, name_tag))
        c.Write()
        
        if var.ratio == True:
            if var.rebin == True:
                for h in hists:
                    h.RebinX(var.rebinFactor)
                    binSize = h.GetXaxis().GetBinWidth(1)
                    h.GetYaxis().SetTitle("Entries/({:0.2f} {})".format(binSize, var.unit));

                    if var.logy:
                        h.SetMaximum((maxy/miny)**yScale[1] * miny)
                    else:
                        if miny < 0:
                            h.SetMaximum(yScale[0] * maxy*var.rebinFactor)
                        else:
                            h.SetMaximum(yScale[1] * maxy*var.rebinFactor)
                if hists[0]:
                    h3 = hists[0].Clone("h3_{}_rebinned".format(var_name))
                    h3.SetLineColor(kBlack)
                    h3.SetMarkerColor(kBlack)
                    h3.SetMinimum(ratio_min)  # Define Y ..
                    h3.SetMaximum(ratio_max) # .. range         
                    #        h3.Sumw2()
                    h3.SetStats(0)      # No statistics on lower plot
                    if len(hists) > 1:
                        h3.Divide(hists[1])
                    h3.SetMarkerStyle(21)
                    h3.GetYaxis().SetTitle("Data/Bkg");
                    h3.GetYaxis().SetTitleSize(.13) #0.042)
                    h3.GetYaxis().SetLabelSize(.12) #0.037)
                    h3.GetYaxis().SetNdivisions(8)
                    h3.GetYaxis().SetTitleOffset(.38)
                    #
                    h3.GetXaxis().SetTitle(var.label.replace('.', ' '))
                    h3.GetXaxis().SetTitleSize(.15) #0.042)
                    h3.GetXaxis().SetLabelSize(.12) #0.037)
                    h3.GetXaxis().SetTitleOffset(1.)
                    pad2.cd()
                    h3.Draw("ep")
                #
                pad1.Update()
                pad2.Update()
                c.Print("{}/plot_{}_{}_{}_rebinned_{}.png".format(output_dir, tag, channel, var_name, name_tag))
        else:
            cn.Print(output_dir+"/plot_{}_{}_{}_{}.png".format( tag, channel, var_name, name_tag))
            if var.rebin == True:
                for h in hists:
                    h.RebinX(var.rebinFactor)
                    binSize = h.GetXaxis().GetBinWidth(1)
                    h.GetYaxis().SetTitle("Entries/({:0.2f} {})".format(binSize, var.unit));
                    if var.logy:
                        h.SetMaximum((maxy/miny)**yScale[1] * miny)
                    else:
                        if miny < 0:
                            h.SetMaximum(yScale[0] * maxy*var.rebinFactor)
                        else:
                            h.SetMaximum(yScale[1] * maxy*var.rebinFactor)
                pad1.Update()
                cn.Print(output_dir+"/plot_{}_{}_{}_rebinned_{}.png".format(tag, channel, var_name, name_Tag))

        #del c
    
    rootFile.Close()
    print("file: {} saved!".format(fileName));

