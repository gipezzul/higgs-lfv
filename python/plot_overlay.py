#!/usr/bin/env python

import os, sys
import argparse
from importlib import import_module


import ROOT
from ROOT import *

# flattening options
workingpoints = {
    "UltraLoose": 99,
    "VeryLoose": 97,
    "Loose": 95,
    "VeryMedium": 90,
    "Medium": 85,
    "Tight": 75
}


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser(prog='./plot_overlay.py')
    
    parser.add_argument("--input-file", "-i", dest='input_files', default=[], action='append', type=str, help="Input file(s).")
    parser.add_argument("--input-name", "-n", dest='input_names', default=[], action='append', type=str, help="Input name(s). e.g. 'v02', 'MC16a'")   
    parser.add_argument("--dsid", "-ds", dest='dsid', type=str, help="Input DSID")

    parser.add_argument("--output-dir", "-o", default="plots", type=str, help="Output directory.")
    
    parser.add_argument("--vars", default="plot_vars_overlay", type=str, help="Variables to plot (defined in an importable python module).")
    
    parser.add_argument("--force-recreation", "-f", action="store_true", help="Force recreation of cached histograms.")
    
    try:
        args = parser.parse_args(argv)
        
        checkArgs(args)
        
    except:
        parser.print_help()
        raise
    
    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    for file in args.input_files:
        if not os.path.exists(file) or not os.path.isfile(file):
            raise Exception("Cannot access input file '{}'".format(file))
    
    if not len(args.input_files) == len(args.input_names):
        raise Exception("{} input files were given, but there are {} input names".format(len(args.input_files), len(args.input_names)))
    
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)
    elif not os.path.isdir(args.output_dir):
        raise Exception("Cannot access directory '{}'".format(args.output_dir))
    
    if not os.path.exists(args.vars+".py") or not os.path.isfile(args.vars+".py"):
        raise Exception("Cannot access module '{}'".format(args.vars+".py"))
    
    return

#-------------------------------------------------------------------------------
def plot_overlay(args):
    
    gROOT.SetBatch()
    gROOT.ProcessLine("{gErrorIgnoreLevel = kWarning;}")
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    colours = [kRed, kBlue, kGreen, kCyan+1]
    markers = [kOpenCircle, kOpenTriangleUp, kOpenTriangleDown, kOpenSquare]
    
    vars = import_module(args.vars).vars
    
    cached = TFile(os.path.join(args.output_dir, "overlay_plots.root"), "UPDATE")
    
    for var_name,var in vars.iteritems():
        print "Plotting '{}'...".format(var_name)
        
        # for (wpname, wpeff) in workingpoints.items():
        hists = []
        c = TCanvas("c", "c", 800, 700)
        c.SetLeftMargin(0.15)
        c.SetBottomMargin(0.10)
        c.SetRightMargin(0.05)
        c.SetTopMargin(0.05)
        c.SetLogy(var.logy)
        # c.SetLogx(var.logx)
        
        for i,file in enumerate(args.input_files):
            print "  plotting from '{}'".format(file)
            f = TFile(file)
            tree = f.Get("NOMINAL")
            h_name  = "{}_{}".format(var_name, args.input_names[i])
            h = TH1F(h_name, h_name, var.bins, var.min, var.max)

            #             h.SetName(h_name)

            tree.Project("{}".format(h_name), "{}".format(var.var), "{}".format(var.weight))
        
            print("histo integral = {}".format(h.GetIntegral()))
            
            tar = "mc16e, DSID: {}".format(int(args.dsid))
            sel = var.weight
            extra_labels = [tar, sel]
            
            #now create the rejection histogram

            h.GetXaxis().SetTitle(var.label.replace('.', ' '))
            h.GetXaxis().SetTitleSize(0.042)
            h.GetXaxis().SetLabelSize(0.037)
            h.GetXaxis().SetTitleOffset(1.2)
            
            binSize = h.GetXaxis().GetBinWidth(1)

            h.GetYaxis().SetTitle("Entries/{:0.2f}".format(binSize));
            h.GetYaxis().SetTitleSize(0.042)
            h.GetYaxis().SetLabelSize(0.037)
            
            h.SetDirectory(0)
            hists.append(h)
            
            cached.cd()
            h.Write()
            #            f.cd()
            
            #            f.Close()
        
        miny = float('inf')
        maxy = -float('inf')
        for h in hists:
            miny = min(miny, h.GetBinContent(h.GetMinimumBin()))
            maxy = max(maxy, h.GetBinContent(h.GetMaximumBin()))
            miny = max(miny, 1e-6)
        
        for i,h in enumerate(hists):
            h.SetLineColor(colours[i])
            h.SetLineWidth(2)
            h.SetMarkerStyle(markers[i])
            h.SetMarkerColor(colours[i])
            h.SetMinimum(miny)
            if var.logy:
                h.SetMaximum((maxy/miny)**1.5 * miny)
            else:
                h.SetMaximum(1.5 * maxy)
            
            h.Draw("E0 same")
        
        atlas_label = paveText("#font[72]{ATLAS} #font[42]{Simulation Internal}")
        atlas_label.Draw()

        latex = ROOT.TLatex()
        latex.SetNDC()
        latex.SetTextFont(43)
        latex.SetTextSize(24)
        latex.SetTextAlign(31)
        th = 0.04
        tx = 0.90
        #ty = 1.0-0.15-0.055
        ty = 1.0-0.05-0.055
        for l in extra_labels:
            latex.DrawLatex(tx,ty,l)
            ty-=th
        
        #legend = TLegend(0.7, 0.83, 0.935, 0.935)
        legend = TLegend(0.2, 0.96, 0.9, 1)
        legend.SetNColumns(2)
        legend.SetBorderSize(0)
        legend.SetFillColor(0)
        legend.SetFillStyle(0)
        legend.SetTextFont(43)
        legend.SetTextSize(20)

        for i,h in enumerate(hists):
            legend.AddEntry(h, args.input_names[i], "lp")
            legend.Draw()
        
        c.SaveAs(os.path.join(args.output_dir, "c_Overlay_{}_{}.png".format(args.dsid, var_name)))
        
        del c
    
    cached.Close()
    
    print "Done!"

#-------------------------------------------------------------------------------
def paveText(text, left=0.15, right=0.5, bottom=0.9, top=0.94, size=0.045, align=13):
    pt = TPaveText(left, bottom, right, top, "NDC")
    pt.SetBorderSize(0)
    pt.SetFillStyle(0)
    pt.SetLineStyle(0)
    
    text = pt.AddText(text)
    text.SetTextSize(size)
    text.SetTextAlign(align)
    
    return pt

#-------------------------------------------------------------------------------
if __name__ == '__main__':
    args = parseArgs(sys.argv[1:])
    plot_overlay(args)
