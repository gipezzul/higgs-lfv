
def plot_pt(data):
    
    bins = [0 + 2*x for x in range(201) ]
    data_x = [1 + 2*x for x in range(200) ]

    data_ = []

    mc_pt  = []
    mc_weights = []
    mc_colors = []
    mc_labels = []

    for s in stack_order:
        if s =='Ztautau' : 
            data_,_ = np.histogram(data[s].jet_1_pt.values, bins=bins)
        mc_labels.append(s)
        mc_pt.append(data[s].jet_1_pt.values)
        mc_colors.append(samples[s]['color'])
        mc_weights.append(data[s].totalWeight_Ele.values)

    top = np.amax(data_)*7
    
    plt.clf()

    plt.hist(mc_pt,bins=bins,stacked=False,color=mc_colors, label=mc_labels)

    plt.xlabel(r'$\rm p_{T}$ [GeV]',fontname='sans-serif',horizontalalignment='right',x=1.0,fontsize=11)

    plt.ylabel(r'Events',fontname='sans-serif',horizontalalignment='right',y=1.0,fontsize=11)
#    plt.yscale('log')
    plt.ylim(bottom=1,top=top)

    ax = plt.gca()
    plt.text(0.05,0.97,r'$\mathbf{{ATLAS}}$ Simulation Internal',ha="left",va="top",family='sans-serif',transform=ax.transAxes,fontsize=13)
    plt.text(0.05,0.92,'Higgs LFV',ha="left",va="top",family='sans-serif',transform=ax.transAxes,style='italic',fontsize=8)
#    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV},\;\int L\,dt=XX\,\mathrm{fb}^{-1}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)
    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)

    plt.legend()

    #plt.show()
    plt.savefig("plot_pt.pdf")

    return

def plot_dijet_m(data):
    
    bins = [300 + 2*x for x in range(401) ]

    data_ = []

    mc_dijet_m  = []
    mc_weights = []
    mc_colors = []
    mc_labels = []

    for s in stack_order:
        if s =='Ztautau' : 
            data_,_ = np.histogram(data[s].dijet_m.values, bins=bins)
        mc_labels.append(s)
        mc_dijet_m.append(data[s].dijet_m.values)
        mc_colors.append(samples[s]['color'])
        mc_weights.append(data[s].totalWeight_Ele.values)

    top = 1000 #np.amax(data_)*20
    
    plt.clf()

    plt.hist(mc_dijet_m,bins=bins,stacked=False,color=mc_colors, label=mc_labels)

    plt.xlabel(r'$\rm m_{jj}$ [GeV]',fontname='sans-serif',horizontalalignment='right',x=1.0,fontsize=11)

    plt.ylabel(r'Events',fontname='sans-serif',horizontalalignment='right',y=1.0,fontsize=11)
#    plt.yscale('log')
    plt.ylim(bottom=1,top=top)

    ax = plt.gca()
    plt.text(0.05,0.97,r'$\mathbf{{ATLAS}}$ Simulation Internal',ha="left",va="top",family='sans-serif',transform=ax.transAxes,fontsize=13)
    plt.text(0.05,0.92,'Higgs LFV',ha="left",va="top",family='sans-serif',transform=ax.transAxes,style='italic',fontsize=8)
#    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV},\;\int L\,dt=XX\,\mathrm{fb}^{-1}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)
    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)

    plt.legend()

    #plt.show()
    plt.savefig("plot_dijet_m.pdf")

    return

def plot_jet_pt(data):
    
    bins = [0 + 2*x for x in range(201) ]
    data_x = [1 + 2*x for x in range(200) ]

    data_ = []

    mc_pt  = []
    mc_weights = []
    mc_colors = []
    mc_labels = []

    for s in stack_order:
        if s =='Ztautau' : 
            data_,_ = np.histogram(data[s].jet_0_pt.values, bins=bins)
        mc_labels.append(s)
        mc_pt.append(data[s].jet_0_pt.values)
        mc_colors.append(samples[s]['color'])
        mc_weights.append(data[s].totalWeight_Ele.values)

    top = np.amax(data_)*20
    
    plt.clf()

    plt.hist(mc_pt,bins=bins,stacked=False,color=mc_colors, label=mc_labels)

    plt.xlabel(r'$\rm p_{T}^{jet}$ [GeV]',fontname='sans-serif',horizontalalignment='right',x=1.0,fontsize=11)

    plt.ylabel(r'Events',fontname='sans-serif',horizontalalignment='right',y=1.0,fontsize=11)
#    plt.yscale('log')
    plt.ylim(bottom=1,top=top)

    ax = plt.gca()
    plt.text(0.05,0.97,r'$\mathbf{{ATLAS}}$ Simulation Internal',ha="left",va="top",family='sans-serif',transform=ax.transAxes,fontsize=13)
    plt.text(0.05,0.92,'Higgs LFV',ha="left",va="top",family='sans-serif',transform=ax.transAxes,style='italic',fontsize=8)
#    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV},\;\int L\,dt=XX\,\mathrm{fb}^{-1}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)
    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)

    plt.legend()

    #plt.show()
    plt.savefig("plot_jet_pt.pdf")

    return

def plot_njets(data):
    
    bins_njets   = [0 + x for x in range(11) ]
    data_x_njets = [1 + x for x in range(10) ]

    data_ = []

    mc_njets = []
    mc_weights = []
    mc_colors = []
    mc_labels = []
    
    for s in stack_order:
        if s =='Ztautau' : 
            data_,_ = np.histogram(data[s].n_jets_30.values, bins=bins_njets)
        mc_labels.append(s)
        mc_njets.append(data[s].n_jets_30.values)
        mc_colors.append(samples[s]['color'])
        mc_weights.append(data[s].totalWeight_Ele.values)
        
    plt.clf()
    plt.hist(mc_njets,bins=bins_njets,stacked=False,color=mc_colors, label=mc_labels)

    plt.xlabel(r'$\rm N_{jets}(p_{T}>30 \ GeV)$',fontname='sans-serif',horizontalalignment='right',x=1.0,fontsize=11)

    plt.ylabel(r'Events',fontname='sans-serif',horizontalalignment='right',y=1.0,fontsize=11)

    #    data_,_ = np.histogram(data['lephad'].n_jets_30.values, bins=bins_njets)
    top = np.amax(data_)*20
    #    plt.ylim(bottom=1,top=top)

    ax = plt.gca()
    plt.text(0.05,0.97,r'$\mathbf{{ATLAS}}$ Simulation Internal',ha="left",va="top",family='sans-serif',transform=ax.transAxes,fontsize=13)
    plt.text(0.05,0.92,'Higgs LFV',ha="left",va="top",family='sans-serif',transform=ax.transAxes,style='italic',fontsize=8)
#    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV},\;\int L\,dt=XX\,\mathrm{fb}^{-1}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)
    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)

    plt.legend()

    #plt.show()
    plt.savefig("plot_njets_30.pdf")
    return

def plot_pt(data):
    
    bins = [0 + 2*x for x in range(201) ]
    data_x = [1 + 2*x for x in range(200) ]

    data_ = []

    mc_pt  = []
    mc_weights = []
    mc_colors = []
    mc_labels = []

    for s in stack_order:
        if s =='Ztautau' : 
            data_,_ = np.histogram(data[s].jet_1_pt.values, bins=bins)
        mc_labels.append(s)
        mc_pt.append(data[s].jet_1_pt.values)
        mc_colors.append(samples[s]['color'])
        mc_weights.append(data[s].totalWeight_Ele.values)

    top = np.amax(data_)*7
    
    plt.clf()

    plt.hist(mc_pt,bins=bins,stacked=False,color=mc_colors, label=mc_labels)

    plt.xlabel(r'$\rm p_{T}$ [GeV]',fontname='sans-serif',horizontalalignment='right',x=1.0,fontsize=11)

    plt.ylabel(r'Events',fontname='sans-serif',horizontalalignment='right',y=1.0,fontsize=11)
#    plt.yscale('log')
    plt.ylim(bottom=1,top=top)

    ax = plt.gca()
    plt.text(0.05,0.97,r'$\mathbf{{ATLAS}}$ Simulation Internal',ha="left",va="top",family='sans-serif',transform=ax.transAxes,fontsize=13)
    plt.text(0.05,0.92,'Higgs LFV',ha="left",va="top",family='sans-serif',transform=ax.transAxes,style='italic',fontsize=8)
#    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV},\;\int L\,dt=XX\,\mathrm{fb}^{-1}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)
    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)

    plt.legend()

    #plt.show()
    plt.savefig("plot_pt.pdf")

    return

def plot_dijet_m(data):
    
    bins = [300 + 2*x for x in range(401) ]

    data_ = []

    mc_dijet_m  = []
    mc_weights = []
    mc_colors = []
    mc_labels = []

    for s in stack_order:
        if s =='Ztautau' : 
            data_,_ = np.histogram(data[s].dijet_m.values, bins=bins)
        mc_labels.append(s)
        mc_dijet_m.append(data[s].dijet_m.values)
        mc_colors.append(samples[s]['color'])
        mc_weights.append(data[s].totalWeight_Ele.values)

    top = 1000 #np.amax(data_)*20
    
    plt.clf()

    plt.hist(mc_dijet_m,bins=bins,stacked=False,color=mc_colors, label=mc_labels)

    plt.xlabel(r'$\rm m_{jj}$ [GeV]',fontname='sans-serif',horizontalalignment='right',x=1.0,fontsize=11)

    plt.ylabel(r'Events',fontname='sans-serif',horizontalalignment='right',y=1.0,fontsize=11)
#    plt.yscale('log')
    plt.ylim(bottom=1,top=top)

    ax = plt.gca()
    plt.text(0.05,0.97,r'$\mathbf{{ATLAS}}$ Simulation Internal',ha="left",va="top",family='sans-serif',transform=ax.transAxes,fontsize=13)
    plt.text(0.05,0.92,'Higgs LFV',ha="left",va="top",family='sans-serif',transform=ax.transAxes,style='italic',fontsize=8)
#    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV},\;\int L\,dt=XX\,\mathrm{fb}^{-1}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)
    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)

    plt.legend()

    #plt.show()
    plt.savefig("plot_dijet_m.pdf")

    return

def plot_jet_pt(data):
    
    bins = [0 + 2*x for x in range(201) ]
    data_x = [1 + 2*x for x in range(200) ]

    data_ = []

    mc_pt  = []
    mc_weights = []
    mc_colors = []
    mc_labels = []

    for s in stack_order:
        if s =='Ztautau' : 
            data_,_ = np.histogram(data[s].jet_0_pt.values, bins=bins)
        mc_labels.append(s)
        mc_pt.append(data[s].jet_0_pt.values)
        mc_colors.append(samples[s]['color'])
        mc_weights.append(data[s].totalWeight_Ele.values)

    top = np.amax(data_)*20
    
    plt.clf()

    plt.hist(mc_pt,bins=bins,stacked=False,color=mc_colors, label=mc_labels)

    plt.xlabel(r'$\rm p_{T}^{jet}$ [GeV]',fontname='sans-serif',horizontalalignment='right',x=1.0,fontsize=11)

    plt.ylabel(r'Events',fontname='sans-serif',horizontalalignment='right',y=1.0,fontsize=11)
#    plt.yscale('log')
    plt.ylim(bottom=1,top=top)

    ax = plt.gca()
    plt.text(0.05,0.97,r'$\mathbf{{ATLAS}}$ Simulation Internal',ha="left",va="top",family='sans-serif',transform=ax.transAxes,fontsize=13)
    plt.text(0.05,0.92,'Higgs LFV',ha="left",va="top",family='sans-serif',transform=ax.transAxes,style='italic',fontsize=8)
#    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV},\;\int L\,dt=XX\,\mathrm{fb}^{-1}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)
    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)

    plt.legend()

    #plt.show()
    plt.savefig("plot_jet_pt.pdf")

    return

def plot_njets(data):
    
    bins_njets   = [0 + x for x in range(11) ]
    data_x_njets = [1 + x for x in range(10) ]

    data_ = []

    mc_njets = []
    mc_weights = []
    mc_colors = []
    mc_labels = []
    
    for s in stack_order:
        if s =='Ztautau' : 
            data_,_ = np.histogram(data[s].n_jets_30.values, bins=bins_njets)
        mc_labels.append(s)
        mc_njets.append(data[s].n_jets_30.values)
        mc_colors.append(samples[s]['color'])
        mc_weights.append(data[s].totalWeight_Ele.values)
        
    plt.clf()
    plt.hist(mc_njets,bins=bins_njets,stacked=False,color=mc_colors, label=mc_labels)

    plt.xlabel(r'$\rm N_{jets}(p_{T}>30 \ GeV)$',fontname='sans-serif',horizontalalignment='right',x=1.0,fontsize=11)

    plt.ylabel(r'Events',fontname='sans-serif',horizontalalignment='right',y=1.0,fontsize=11)

    #    data_,_ = np.histogram(data['lephad'].n_jets_30.values, bins=bins_njets)
    top = np.amax(data_)*20
    #    plt.ylim(bottom=1,top=top)

    ax = plt.gca()
    plt.text(0.05,0.97,r'$\mathbf{{ATLAS}}$ Simulation Internal',ha="left",va="top",family='sans-serif',transform=ax.transAxes,fontsize=13)
    plt.text(0.05,0.92,'Higgs LFV',ha="left",va="top",family='sans-serif',transform=ax.transAxes,style='italic',fontsize=8)
#    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV},\;\int L\,dt=XX\,\mathrm{fb}^{-1}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)
    plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)

    plt.legend()

    #plt.show()
    plt.savefig("plot_njets_30.pdf")
    return
