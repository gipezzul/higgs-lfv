import uproot 
import sys
import os 

my_dir=os.environ['PWD']
my_python_dir=my_dir+'/python'
sys.path.append(my_python_dir)

def eval_metadata(dir_name, dsid):
    tmp_files = os.listdir(dir_name)
    files     = []
    for f in tmp_files:
        if '.root' in f:
            files.append(dir_name+'/'+f)
    
    # print("Running over MC -- calculating sum_of_weights for each DSID...")
    bin_110 = ['345211','345212','345217','346324','346325','346326',
               '346329','345948','346190','346191','346192','346193',
               '346343','346344','346345'] # Inclusive VH, Leptonic VH, VBFH, ttH
    bin_152 = ['345120', '345121', '345122', '345123', '345324','345060'] # ggH
    if dsid in bin_110:
        metadata_tag = "h_metadata_theory_weights"
        bin_tag      = 110
    elif dsid in bin_152:
        metadata_tag = "h_metadata_theory_weights"
        bin_tag      = 152
    else:
        metadata_tag = "h_metadata"
        bin_tag      = 8
        
    sum_of_weights = 0.
    for f in files:
        hMetadata       = uproot.open(f)[metadata_tag]
        sum_of_weights += hMetadata[bin_tag]
    return sum_of_weights
