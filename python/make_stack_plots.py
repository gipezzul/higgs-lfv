from __future__ import division # Force python to turn integer division into a float result! Otherwise your fake rates will be all 0...
import os
import sys
import argparse
import numpy as np
import pandas as pd
from tqdm import tqdm 
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import argparse

plt.rcParams['figure.figsize'] = (10, 7)
plt.rcParams['font.size'] = 14
pd.options.mode.chained_assignment = None  # default='warn'. This eliminates warnings when assigning new variables to dataframes during the cutflow.
working_node ="/home/gpezz/project"

my_dir=os.environ['PWD']
my_python_dir=my_dir+'/python'
sys.path.append(my_python_dir)

from doPlots import *
from plot_vars import vars

def parseArgs(argv):
    parser = argparse.ArgumentParser(prog='./make_stack_plots.py')
    
    parser.add_argument("--mc-data", "-mc",  default="mc16a", type=str, help="MC or Data directory.")
    parser.add_argument("--channel", "-ch",  default="etau", type=str, help="Channel.")
    parser.add_argument("--sel-region", "-sr",  default="nonVBF", type=str, help="Selection region.")   
    parser.add_argument("--sel-region-2", "-sr2",  default="preselection", type=str, help="Selection region.")   
    parser.add_argument("--vars", default="plot_vars", type=str, help="Variables to plot (defined in an importable python module).")
    parser.add_argument("--tag",  "-t", default="V05", help="Tag.")
    parser.add_argument("--data-driven", "-dd",default=False, type=bool, help="Evaluate Zee using the data driven approach.")
    parser.add_argument("--FF-version",default="all", help="Version of FF and R used .")
    parser.add_argument("--eFF-version",default="2D", help="Version of eFF used .")
    parser.add_argument("--save-fakes", default=False, type=bool, help="Save dataframes of the fake data.")
    parser.add_argument("--same-sign", default=False, type=bool, help="Same sign lep tau.")
    parser.add_argument("--only-xp", default=False, type=bool, help="Select only xP.")
    parser.add_argument("--nprongs", default=1, type=int, help="Prongness.")
    parser.add_argument("--mvisCut", "-mt", default=0, type=int, help="apply the mVis cut (only for nonVBF).")
    parser.add_argument("--min-mass", "-minM", default=0, type=float, help="set the mVis min cut.")
    parser.add_argument("--force-recreation", "-f", action="store_true", help="Force recreation of cached histograms.")
    parser.add_argument("--add-sys", "-sys", action="store_true", help="include systematics.")
    parser.add_argument("--debug", default=False, type=bool, help="Turn on debug messages.")

    args = parser.parse_args(argv)
    return args

FF_ZeeCR = {}
FF_ZeeCR_antiID = {}
FF_Zee_antiID = {}

ZeeCR_wp = 'medium!tight'
# ZeeCR_wp = 'tight'

for mc in ['mc16a','mc16d','mc16e']:
    # FF_ZeeCR[mc] = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/FF_V05_all_ZeeCR.root') 
    # FF_ZeeCR_antiID[mc] = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/FF_V05_all_ZeeCR_antieleID.root')
    #testing wider Zee inv mass
    # FF_ZeeCR[mc]        = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/FF_V05_all_ZeeCR_WiderMvisCut.root') 
    # FF_ZeeCR_antiID[mc] = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/FF_V05_all_ZeeCR_antieleID_WiderMvisCut.root')
    if ZeeCR_wp == 'medium!tight':
        FF_ZeeCR[mc]        = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/FF_V05_all_ZeeCR_NewMvisCut.root') 
        FF_ZeeCR_antiID[mc] = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/FF_V05_all_ZeeCR_antieleID_NewMvisCut.root')
    elif ZeeCR_wp == 'tight':
        FF_ZeeCR[mc]        = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/FF_V05_all_ZeeCR_NewMvisCut_TightTauID.root') 
        FF_ZeeCR_antiID[mc] = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/FF_V05_all_ZeeCR_antieleID_NewMvisCut_TightTauID.root')

    FF_Zee_antiID[mc] = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/FF_tight_V05_all_antieleID.root')
    # FF_ZeeCR       [mc] = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/FF_V05_{}_ZeeCR.root'.format(mc))
    # FF_ZeeCR_antiID[mc] = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/FF_V05_{}_ZeeCR_antieleID.root'.format(mc))

FF_tight = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/FF_tight_V05_all.root')

FF_hist = {
    'etau' : {
        '1P' : {
            'QCD' : {
                'mc16a' : {
                    'VBF'    : FF_tight.Get('etau_QCD_VBF_1Prong'),
                    'nonVBF' : FF_tight.Get('etau_QCD_Preselection_1Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16a'].Get('etau_QCD_ZeeCR_1Prong'),
                    'ZeeCR_antiID'  : FF_ZeeCR_antiID['mc16a'].Get('etau_QCD_ZeeCR_1Prong'),
                    'Zee_VBF_antiID'     : FF_Zee_antiID['mc16a'].Get('etau_QCD_VBF_1Prong'),
                    'Zee_nonVBF_antiID'  : FF_Zee_antiID['mc16a'].Get('etau_QCD_Preselection_1Prong')
                },
                'mc16d' : {
                    'VBF'    : FF_tight.Get('etau_QCD_VBF_1Prong'),
                    'nonVBF' : FF_tight.Get('etau_QCD_Preselection_1Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16d'].Get('etau_QCD_ZeeCR_1Prong'),
                    'ZeeCR_antiID'  : FF_ZeeCR_antiID['mc16d'].Get('etau_QCD_ZeeCR_1Prong'),
                    'Zee_VBF_antiID'     : FF_Zee_antiID['mc16d'].Get('etau_QCD_VBF_1Prong'),
                    'Zee_nonVBF_antiID'  : FF_Zee_antiID['mc16d'].Get('etau_QCD_Preselection_1Prong')
                },
                'mc16e' : {
                    'VBF'    : FF_tight.Get('etau_QCD_VBF_1Prong'),
                    'nonVBF' : FF_tight.Get('etau_QCD_Preselection_1Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16e'].Get('etau_QCD_ZeeCR_1Prong'),
                    'ZeeCR_antiID'       : FF_ZeeCR_antiID['mc16d'].Get('etau_QCD_ZeeCR_1Prong'),
                    'Zee_VBF_antiID'     : FF_Zee_antiID['mc16d'].Get('etau_QCD_VBF_1Prong'),
                    'Zee_nonVBF_antiID'  : FF_Zee_antiID['mc16d'].Get('etau_QCD_Preselection_1Prong')
                }
            },
            'W' : {
                'mc16a' : {
                    'VBF'    : FF_tight.Get('etau_W_VBF_1Prong'),
                    'nonVBF' : FF_tight.Get('etau_W_Preselection_1Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16a'].Get('etau_W_ZeeCR_1Prong'),
                    'ZeeCR_antiID'  : FF_ZeeCR_antiID['mc16a'].Get('etau_W_ZeeCR_1Prong'),
                    'Zee_VBF_antiID'     : FF_Zee_antiID['mc16a'].Get('etau_W_VBF_1Prong'),
                    'Zee_nonVBF_antiID'  : FF_Zee_antiID['mc16a'].Get('etau_W_Preselection_1Prong')
                },
                'mc16d' : {
                    'VBF'    : FF_tight.Get('etau_W_VBF_1Prong'),
                    'nonVBF' : FF_tight.Get('etau_W_Preselection_1Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16d'].Get('etau_W_ZeeCR_1Prong'),
                    'ZeeCR_antiID'  : FF_ZeeCR_antiID['mc16d'].Get('etau_W_ZeeCR_1Prong'),
                    'Zee_VBF_antiID'     : FF_Zee_antiID['mc16d'].Get('etau_W_VBF_1Prong'),
                    'Zee_nonVBF_antiID'  : FF_Zee_antiID['mc16d'].Get('etau_W_Preselection_1Prong')
                },
                'mc16e' : {
                    'VBF'    : FF_tight.Get('etau_W_VBF_1Prong'),
                    'nonVBF' : FF_tight.Get('etau_W_Preselection_1Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16e'].Get('etau_W_ZeeCR_1Prong'),
                    'ZeeCR_antiID'  : FF_ZeeCR_antiID['mc16e'].Get('etau_W_ZeeCR_1Prong'),
                    'Zee_VBF_antiID'     : FF_Zee_antiID['mc16e'].Get('etau_W_VBF_1Prong'),
                    'Zee_nonVBF_antiID'  : FF_Zee_antiID['mc16e'].Get('etau_W_Preselection_1Prong')
                }
            }
        },
        '3P' : {
            'QCD' : {
                'mc16a' : {
                    'VBF'    : FF_tight.Get('etau_QCD_VBF_3Prong'),
                    'nonVBF' : FF_tight.Get('etau_QCD_Preselection_3Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16a'].Get('etau_QCD_ZeeCR_3Prong'),
                    'ZeeCR_antiID'  : FF_ZeeCR_antiID['mc16a'].Get('etau_QCD_ZeeCR_3Prong'),
                    'Zee_VBF_antiID'     : FF_Zee_antiID['mc16a'].Get('etau_QCD_VBF_3Prong'),
                    'Zee_nonVBF_antiID'  : FF_Zee_antiID['mc16a'].Get('etau_QCD_Preselection_3Prong')
                },
                'mc16d' : {
                    'VBF'    : FF_tight.Get('etau_QCD_VBF_3Prong'),
                    'nonVBF' : FF_tight.Get('etau_QCD_Preselection_3Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16d'].Get('etau_QCD_ZeeCR_3Prong'),
                    'ZeeCR_antiID'  : FF_ZeeCR_antiID['mc16d'].Get('etau_QCD_ZeeCR_3Prong'),
                    'Zee_VBF_antiID'     : FF_Zee_antiID['mc16d'].Get('etau_QCD_VBF_3Prong'),
                    'Zee_nonVBF_antiID'  : FF_Zee_antiID['mc16d'].Get('etau_QCD_Preselection_3Prong')
                },
                'mc16e' : {
                    'VBF'    : FF_tight.Get('etau_QCD_VBF_3Prong'),
                    'nonVBF' : FF_tight.Get('etau_QCD_Preselection_3Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16e'].Get('etau_QCD_ZeeCR_3Prong'),
                    'ZeeCR_antiID'  : FF_ZeeCR_antiID['mc16e'].Get('etau_QCD_ZeeCR_3Prong'),
                    'Zee_VBF_antiID'     : FF_Zee_antiID['mc16e'].Get('etau_QCD_VBF_3Prong'),
                    'Zee_nonVBF_antiID'  : FF_Zee_antiID['mc16e'].Get('etau_QCD_Preselection_3Prong')
                }
            },
            'W' : {
                'mc16a' : {
                    'VBF'    : FF_tight.Get('etau_W_VBF_3Prong'),
                    'nonVBF' : FF_tight.Get('etau_W_Preselection_3Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16a'].Get('etau_W_ZeeCR_3Prong'),
                    'ZeeCR_antiID'  : FF_ZeeCR_antiID['mc16a'].Get('etau_W_ZeeCR_3Prong'),
                    'Zee_VBF_antiID'     : FF_Zee_antiID['mc16a'].Get('etau_W_VBF_3Prong'),
                    'Zee_nonVBF_antiID'  : FF_Zee_antiID['mc16a'].Get('etau_W_Preselection_3Prong')
                },
                'mc16d' : {
                    'VBF'    : FF_tight.Get('etau_W_VBF_3Prong'),
                    'nonVBF' : FF_tight.Get('etau_W_Preselection_3Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16d'].Get('etau_W_ZeeCR_3Prong'),
                    'ZeeCR_antiID'  : FF_ZeeCR_antiID['mc16d'].Get('etau_W_ZeeCR_3Prong'),
                    'Zee_VBF_antiID'     : FF_Zee_antiID['mc16d'].Get('etau_W_VBF_3Prong'),
                    'Zee_nonVBF_antiID'  : FF_Zee_antiID['mc16d'].Get('etau_W_Preselection_3Prong')
                },
                'mc16e' : {
                    'VBF'    : FF_tight.Get('etau_W_VBF_3Prong'),
                    'nonVBF' : FF_tight.Get('etau_W_Preselection_3Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16e'].Get('etau_W_ZeeCR_3Prong'),
                    'ZeeCR_antiID'  : FF_ZeeCR_antiID['mc16e'].Get('etau_QCD_ZeeCR_3Prong'),
                    'Zee_VBF_antiID'     : FF_Zee_antiID['mc16e'].Get('etau_W_VBF_3Prong'),
                    'Zee_nonVBF_antiID'  : FF_Zee_antiID['mc16e'].Get('etau_W_Preselection_3Prong')
                }
            }
        }
    }, #finished etau
    'mutau' : {
        '1P' : {
            'QCD' : {
                'mc16a' : {
                    'VBF'    : FF_tight.Get('mutau_QCD_VBF_1Prong'),
                    'nonVBF' : FF_tight.Get('mutau_QCD_Preselection_1Prong'),
                    'ZeeCR'  : 0,
                    'ZeeCR_antiID'  : 0,
                    'Zee_VBF_antiID'     : 0,
                    'Zee_nonVBF_antiID'  : 0
                },
                'mc16d' : {
                    'VBF'    : FF_tight.Get('mutau_QCD_VBF_1Prong'),
                    'nonVBF' : FF_tight.Get('mutau_QCD_Preselection_1Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16d'].Get('mutau_QCD_ZeeCR_1Prong'),
                    'ZeeCR_antiID'  : 0,
                    'Zee_VBF_antiID'     : 0,
                    'Zee_nonVBF_antiID'  : 0
                },
                'mc16e' : {
                    'VBF'    : FF_tight.Get('mutau_QCD_VBF_1Prong'),
                    'nonVBF' : FF_tight.Get('mutau_QCD_Preselection_1Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16e'].Get('mutau_QCD_ZeeCR_1Prong'),
                    'ZeeCR_antiID'  : 0,
                    'Zee_VBF_antiID'     : 0,
                    'Zee_nonVBF_antiID'  : 0
                }
            },
            'W' : {
                'mc16a' : {
                    'VBF'    : FF_tight.Get('mutau_W_VBF_1Prong'),
                    'nonVBF' : FF_tight.Get('mutau_W_Preselection_1Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16a'].Get('mutau_W_ZeeCR_1Prong'),
                    'ZeeCR_antiID'  : 0,
                    'Zee_VBF_antiID'     : 0,
                    'Zee_nonVBF_antiID'  : 0
                },
                'mc16d' : {
                    'VBF'    : FF_tight.Get('mutau_W_VBF_1Prong'),
                    'nonVBF' : FF_tight.Get('mutau_W_Preselection_1Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16d'].Get('mutau_W_ZeeCR_1Prong'),
                    'ZeeCR_antiID'  : 0,
                    'Zee_VBF_antiID'     : 0,
                    'Zee_nonVBF_antiID'  : 0
                },
                'mc16e' : {
                    'VBF'    : FF_tight.Get('mutau_W_VBF_1Prong'),
                    'nonVBF' : FF_tight.Get('mutau_W_Preselection_1Prong'),
                    'ZeeCR'  : FF_ZeeCR['mc16e'].Get('mutau_W_ZeeCR_1Prong'),
                    'ZeeCR_antiID'  : 0,
                    'Zee_VBF_antiID'     : 0,
                    'Zee_nonVBF_antiID'  : 0
                }
            }
        },
        '3P' : {
            'QCD' : {
                'mc16a' : {
                    'VBF'    : FF_tight.Get('mutau_QCD_VBF_3Prong'),
                    'nonVBF' : FF_tight.Get('mutau_QCD_Preselection_3Prong'),
                    'ZeeCR'  : 0,
                    'ZeeCR_antiID'  : 0,
                    'Zee_VBF_antiID'     : 0,
                    'Zee_nonVBF_antiID'  : 0
                },
                'mc16d' : {
                    'VBF'    : FF_tight.Get('mutau_QCD_VBF_3Prong'),
                    'nonVBF' : FF_tight.Get('mutau_QCD_Preselection_3Prong'),
                    'ZeeCR'  : 0,
                    'Zee_VBF_antiID'     : 0,
                    'Zee_nonVBF_antiID'  : 0
                },
                'mc16e' : {
                    'VBF'    : FF_tight.Get('mutau_QCD_VBF_3Prong'),
                    'nonVBF' : FF_tight.Get('mutau_QCD_Preselection_3Prong'),
                    'ZeeCR'  : 0,
                    'ZeeCR_antiID'  : 0,
                    'Zee_VBF_antiID'     : 0,
                    'Zee_nonVBF_antiID'  : 0
                }
            },
            'W' : {
                'mc16a' : {
                    'VBF'    : FF_tight.Get('mutau_W_VBF_3Prong'),
                    'nonVBF' : FF_tight.Get('mutau_W_Preselection_3Prong'),
                    'ZeeCR'  : 0,
                    'ZeeCR_antiID'  :0 ,
                    'Zee_VBF_antiID'     : 0,
                    'Zee_nonVBF_antiID'  : 0
                },
                'mc16d' : {
                    'VBF'    : FF_tight.Get('mutau_W_VBF_3Prong'),
                    'nonVBF' : FF_tight.Get('mutau_W_Preselection_3Prong'),
                    'ZeeCR'  : 0,
                    'ZeeCR_antiID'  : 0,
                    'Zee_VBF_antiID'     : 0,
                    'Zee_nonVBF_antiID'  : 0
                },
                'mc16e' : {
                    'VBF'    : FF_tight.Get('mutau_W_VBF_3Prong'),
                    'nonVBF' : FF_tight.Get('mutau_W_Preselection_3Prong'),
                    'ZeeCR'  : 0,
                    'ZeeCR_antiID'  : 0,
                    'Zee_VBF_antiID'     : 0,
                    'Zee_nonVBF_antiID'  : 0
                }
            }
        }
    }
}

R_ZeeCR = {}
R_tight = {}
eFF_hist = {}
eFF_files = {}
R_ZeeCR_antiID = {}
R_tight_antieleID = {}

for mc in ['mc16a','mc16d','mc16e']:
    # R_ZeeCR  [mc]  = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/R_V05_{}_ZeeCR.root'.format(mc))
    # R_ZeeCR_antiID   [mc]  = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/R_V05_{}_ZeeCR_antieleID.root'.format(mc))
    #test wider Zee inv mass selection
    if ZeeCR_wp == 'medium!tight':
        R_ZeeCR          [mc] = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/R_V05_all_ZeeCR_NewMvisCut.root')
        R_ZeeCR_antiID   [mc] = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/R_V05_all_ZeeCR_antieleID_NewMvisCut.root')
    elif ZeeCR_wp == 'tight':
        R_ZeeCR          [mc] = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/R_V05_all_ZeeCR_NewMvisCut_TightTauID.root')
        R_ZeeCR_antiID   [mc] = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/R_V05_all_ZeeCR_antieleID_NewMvisCut_TightTauID.root')

    #
    #eFF_files[mc]  = TFile.Open("/home/gpezz/project/plots_V05_mc16a/etau/stack_hists/eFF_all/plots_eFF_etau_all.root")
    #test wider mass window
    # eFF_files[mc]  = TFile.Open("/home/gpezz/project/plots_V05_v1ZeeCRTight_mc16a/etau/stack_hists/eFF_all/plots_eFF_etau_all.root")
    R_tight  [mc]  = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/R_tight_V05_{}.root'.format(mc))
    R_tight_antieleID[mc]  = TFile.Open('/home/gpezz/project/HLFV_FakeFiles/R_tight_V05_{}_antieleID.root'.format(mc))

eFF_files = {
    "medium!tight" : TFile.Open("/home/gpezz/project/plots_V05_v2_mc16a/etau/stack_hists/eFF_all/plots_eFF_etau_all.root"),
    "tight"        : TFile.Open("/home/gpezz/project/plots_V05_v2ZeeCRTight_mc16a/etau/stack_hists/eFF_all/plots_eFF_etau_all.root")
}

eFF_info = {
    "medium!tight" : {
        "eFF_hist"              : eFF_files["medium!tight"].Get("h_eFF_ID_etau"),
        "eFF_hist_2D"           : eFF_files["medium!tight"].Get("h2D_eFF_ID_etau"),
        "eFF_hist_2D_3EtaBins"  : eFF_files["medium!tight"].Get("h2D_eFF_ID_etau_3b"),
    },
    "tight"        : {
        "eFF_hist"              : eFF_files["tight"].Get("h_eFF_ID_etau"),
        "eFF_hist_2D"           : eFF_files["tight"].Get("h2D_eFF_ID_etau"),
        "eFF_hist_2D_3EtaBins"  : eFF_files["tight"].Get("h2D_eFF_ID_etau_3b"),
    }
}
#test_file = TFile.Open("/home/gpezz/project/plots_V05_mc16a/etau/stack_hists/eFF_all/plots_eFF_etau_all_test.root")
#testing wider mass cut
test_file = TFile.Open("/home/gpezz/project/plots_V05_v2_mc16a/etau/stack_hists/eFF_all/plots_eFF_etau_all.root")
eFF_hist_2D_3EtaBins_test  = test_file.Get("h2D_eFF_ID_etau_3b")

R_hist = {
    'etau': {
        'mc16a' : {
            'VBF'    : R_tight['mc16a'].Get('etau_QCD_VBF_SR'),
            'nonVBF' : R_tight['mc16a'].Get('etau_QCD_Preselection_SR'),
            'ZeeCR'  : R_ZeeCR['mc16a'].Get('etau_QCD_ZeeCR_1Prong'),
            'ZeeCR_antiID'  : R_ZeeCR_antiID['mc16a'].Get('etau_QCD_ZeeCR_1Prong'),
            'Zee_VBF_antiID_a'     : R_tight_antieleID['mc16a'].Get('etau_QCD_pta_VBF'),
            'Zee_nonVBF_antiID_a'  : R_tight_antieleID['mc16a'].Get('etau_QCD_pta_nonVBF'),
            'Zee_VBF_antiID_b'     : R_tight_antieleID['mc16a'].Get('etau_QCD_ptb_VBF'),
            'Zee_nonVBF_antiID_b'  : R_tight_antieleID['mc16a'].Get('etau_QCD_ptb_nonVBF')
        },
        'mc16d' : {
            'VBF'    : R_tight['mc16d'].Get('etau_QCD_VBF_SR'),
            'nonVBF' : R_tight['mc16d'].Get('etau_QCD_Preselection_SR'),
            'ZeeCR'  : R_ZeeCR['mc16d'].Get('etau_QCD_ZeeCR_1Prong'),
            'ZeeCR_antiID'  : R_ZeeCR_antiID['mc16d'].Get('etau_QCD_ZeeCR_1Prong'),
            'Zee_VBF_antiID_a'     : R_tight_antieleID['mc16d'].Get('etau_QCD_pta_VBF'),
            'Zee_nonVBF_antiID_a'  : R_tight_antieleID['mc16d'].Get('etau_QCD_pta_nonVBF'),
            'Zee_VBF_antiID_b'     : R_tight_antieleID['mc16d'].Get('etau_QCD_ptb_VBF'),
            'Zee_nonVBF_antiID_b'  : R_tight_antieleID['mc16d'].Get('etau_QCD_ptb_nonVBF')
        },
        'mc16e' : {
            'VBF'    : R_tight['mc16e'].Get('etau_QCD_VBF_SR'),
            'nonVBF' : R_tight['mc16e'].Get('etau_QCD_Preselection_SR'),
            'ZeeCR'  : R_ZeeCR['mc16e'].Get('etau_QCD_ZeeCR_1Prong'),
            'ZeeCR_antiID'  : R_ZeeCR_antiID['mc16e'].Get('etau_QCD_ZeeCR_1Prong'),
            'Zee_VBF_antiID_a'     : R_tight_antieleID['mc16e'].Get('etau_QCD_pta_VBF'),
            'Zee_nonVBF_antiID_a'  : R_tight_antieleID['mc16e'].Get('etau_QCD_pta_nonVBF'),
            'Zee_VBF_antiID_b'     : R_tight_antieleID['mc16e'].Get('etau_QCD_ptb_VBF'),
            'Zee_nonVBF_antiID_b'  : R_tight_antieleID['mc16e'].Get('etau_QCD_ptb_nonVBF')
        }
    },
    'mutau': {
        'mc16a' : {
            'VBF'    : R_tight['mc16a'].Get('mutau_QCD_VBF_SR'),
            'nonVBF' : R_tight['mc16a'].Get('mutau_QCD_Preselection_SR'),
            'ZeeCR'  : 0,
            'ZeeCR_antiID' : 0
        },
        'mc16d' : {
            'VBF'    : R_tight['mc16d'].Get('mutau_QCD_VBF_SR'),
            'nonVBF' : R_tight['mc16d'].Get('mutau_QCD_Preselection_SR'),
            'ZeeCR'  : 0,
            'ZeeCR_antiID' : 0
        },
        'mc16e' : {
            'VBF'    : R_tight['mc16e'].Get('mutau_QCD_VBF_SR'),
            'nonVBF' : R_tight['mc16e'].Get('mutau_QCD_Preselection_SR'),
            'ZeeCR'  : 0,
            'ZeeCR_antiID' : 0
        }
    }
}

def deltaR(eta_0, phi_0, eta_1, phi_1):
    dR = np.sqrt(((eta_0 - eta_1)**2)+(((phi_0 - phi_1 + np.pi)%(2*np.pi)-np.pi)**2))
    return dR

def deltaRJetTau(jet_pt, eta_0, phi_0, eta_1, phi_1):
    dR = 10.
    if jet_pt>0.:
        dR = np.sqrt(((eta_0 - eta_1)**2)+(((phi_0 - phi_1 + np.pi)%(2*np.pi)-np.pi)**2))
    return dR


def getMCLabel(dataset):
    if 'data15' in dataset  or 'data16' in dataset:
        return 'mc16a'
    elif 'data17' in dataset:
        return 'mc16d'
    elif 'data18' in dataset:
        return 'mc16e'
    
    mc_types = ['mc16a', 'mc16d','mc16e']
    for mc in mc_types:
        if mc in dataset:
            return mc

    return 'undefined'

def add_eFF(mc, ch, wg, tau_0_pt, nprongs, isData, tag="medium!tight", addErr=False):
    pt_bin = eFF_info[tag]["eFF_hist"].FindBin(tau_0_pt)
    if nprongs == 3:
        return 0. #wg
    eFF = eFF_info[tag]["eFF_hist"].GetBinContent(pt_bin)
    if addErr:
        eFF = eFF + eFF_info[tag]["eFF_hist"].GetBinError(pt_bin)
    if isData == True:
        return wg*eFF
    else:
        return -1.*wg*eFF

# eFF_pt_thresholds = {40., 55.}
eFF_pt_thresholds = [35., 55., 60.]


def add_eFF_2D(mc, ch, wg, tau_0_pt, tau_0_eta, nprongs, isData, tag="medium!tight", addErr=False):
    if nprongs == 3:
        return 0. #wg
    if tau_0_pt>=eFF_pt_thresholds[0]:
        pt_bin  = eFF_info[tag]["eFF_hist_2D"].GetXaxis().FindBin(tau_0_pt)
        eta_bin = eFF_info[tag]["eFF_hist_2D"].GetYaxis().FindBin(np.abs(tau_0_eta))
        eFF = eFF_info[tag]["eFF_hist_2D"].GetBinContent(pt_bin, eta_bin)
        if addErr:
            eFF = eFF + eFF_info[tag]["eFF_hist_2D"].GetBinError(pt_bin, eta_bin)
    else:
        pt_bin  = eFF_hist_2D_3EtaBins_test.GetXaxis().FindBin(tau_0_pt)
        eta_bin = eFF_hist_2D_3EtaBins_test.GetYaxis().FindBin(np.abs(tau_0_eta))
        eFF = eFF_hist_2D_3EtaBins_test.GetBinContent(pt_bin, eta_bin)
        if addErr:
            eFF = eFF + eFF_hist_2D_3EtaBins_test.GetBinError(pt_bin, eta_bin)

    if isData == True:
        return wg*eFF
    else:
        return -1.*wg*eFF

def add_eFF_2D_3EtaBins(mc, ch, wg, tau_0_pt, tau_0_eta, nprongs, isData, tag="medium!tight", addErr=False):
    if nprongs == 3:
        return 0. #wg
    if tau_0_pt<eFF_pt_thresholds[0] or tau_0_pt>=eFF_pt_thresholds[2]:
        # pt_bin  = eFF_hist_2D_3EtaBins_test.GetXaxis().FindBin(tau_0_pt)
        # eta_bin = eFF_hist_2D_3EtaBins_test.GetYaxis().FindBin(np.abs(tau_0_eta))
        # eFF = eFF_hist_2D_3EtaBins_test.GetBinContent(pt_bin, eta_bin)

        pt_bin = eFF_info[tag]["eFF_hist"].FindBin(tau_0_pt)
        eFF = eFF_info[tag]["eFF_hist"].GetBinContent(pt_bin)
        if addErr:
            # eFF = eFF + eFF_hist_2D_3EtaBins_test.GetBinError(pt_bin, eta_bin)
            eFF = eFF + eFF_info[tag]["eFF_hist"].GetBinError(pt_bin)
    elif tau_0_pt>=eFF_pt_thresholds[0] and tau_0_pt<eFF_pt_thresholds[1]:
        pt_bin  = eFF_info[tag]["eFF_hist_2D_3EtaBins"].GetXaxis().FindBin(tau_0_pt)
        eta_bin = eFF_info[tag]["eFF_hist_2D_3EtaBins"].GetYaxis().FindBin(np.abs(tau_0_eta))
        eFF     = eFF_info[tag]["eFF_hist_2D_3EtaBins"].GetBinContent(pt_bin, eta_bin)
        if addErr:
            eFF = eFF + eFF_info[tag]["eFF_hist_2D_3EtaBins"].GetBinError(pt_bin, eta_bin)
    elif tau_0_pt>=eFF_pt_thresholds[1] and tau_0_pt<eFF_pt_thresholds[2]:
        pt_bin  = eFF_info[tag]["eFF_hist_2D"].GetXaxis().FindBin(tau_0_pt)
        eta_bin = eFF_info[tag]["eFF_hist_2D"].GetYaxis().FindBin(np.abs(tau_0_eta))
        eFF     = eFF_info[tag]["eFF_hist_2D"].GetBinContent(pt_bin, eta_bin)
        if addErr:
            eFF = eFF + eFF_info[tag]["eFF_hist_2D"].GetBinError(pt_bin, eta_bin)
    if isData == True:
        return wg*eFF
    else:
        return -1.*wg*eFF

def add_FF(mc, ch, sel, wg, tau_0_pt, ditau_mt_lep1_met, nprongs, mettau_dphi):
    # FF = (1-R_QCD)*FF_W + R_QCD*FF_QCD
    if 'Zee_' not in sel:
        # test with wider Zee inv mass cut
        if 'ZeeCR' in sel:
            pt_bin_RQCD = R_hist[ch][mc][sel].FindBin(tau_0_pt)
            # bin_limit = 5
            bin_limit = 6
            if 'antiID' in sel:
                # bin_limit = 3
                bin_limit = 4
            if pt_bin_RQCD>bin_limit:
                R_QCD = 0
            else:
                R_QCD = R_hist[ch][mc][sel].GetBinContent(pt_bin_RQCD)
        else:
            mt_bin_RQCD = R_hist[ch][mc][sel].FindBin(ditau_mt_lep1_met)
            R_QCD       = R_hist[ch][mc][sel].GetBinContent(mt_bin_RQCD)            
    else:
        dphi_bin_RQCD = R_hist[ch][mc][sel+"_a"].FindBin(mettau_dphi)
        if tau_0_pt < 30.:#GeV
            R_QCD = R_hist[ch][mc][sel+"_a"].GetBinContent(dphi_bin_RQCD)
        else:
            R_QCD = R_hist[ch][mc][sel+"_b"].GetBinContent(dphi_bin_RQCD)
            
    if R_QCD < 0:
        R_QCD = 0.
    if nprongs == 1:
        prong_tag = '1P'
    elif nprongs == 3:
        prong_tag = '3P'
    else:
        prong_tag = 'undefined'
    pt_bin_FFQCD = FF_hist[ch][prong_tag]['QCD'][mc][sel].FindBin(tau_0_pt)
    FF_QCD       = FF_hist[ch][prong_tag]['QCD'][mc][sel].GetBinContent(pt_bin_FFQCD)

    #test for wider Zee inv mass 
    if 'ZeeCR' in sel:
        # bin_FF_limit = 4
        bin_FF_limit = 5
        if pt_bin_FFQCD>bin_FF_limit:
            FF_QCD = 0
    #
    if FF_QCD<0:
        FF_QCD = 0
    pt_bin_FFW   = FF_hist[ch][prong_tag]['W'][mc][sel].FindBin(tau_0_pt)
    FF_W         = FF_hist[ch][prong_tag]['W'][mc][sel].GetBinContent(pt_bin_FFW)
    if FF_W<0:
        FF_W = 0
    FF = ((1.-R_QCD)*FF_W + R_QCD*FF_QCD)
    if FF < 0.:
        print("[::add_FF] FF = {:2.3f} wg = {:2.3f}".format(FF, wg))
        return 0.
    return FF*wg
    
# def plotFF(mc, ch, sel, nprongs):

# def apply_selection(sel, df):
#     if sel == SR:
        

def apply_VBFR_sel(Df):
    if len(Df.index) != 0:
        Df = Df[(Df['tau_0_pt']  > 45.) &
                (Df['n_jets_30'] >= 2 ) &
                (Df['jet_0_pt']  > 40.) &
                (Df['dijet_m']  > 400.) &
                (Df['dijet_absDEta'] > 3.) &
                (Df['n_bjets_DL1r_FixedCutBEff_85']  == 0) ]
    return Df
        

def apply_nonVBF_SR1_sel(Df):
    if len(Df.index) != 0:
        Df = Df[(Df['tau_0_pt']  > 45.) &
                (Df['ditau_mt_lep1_met'] > 40. ) &
                (Df['ditau_mt_lep0_met'] < 30.) &
                (Df['n_bjets_DL1r_FixedCutBEff_85']  == 0) ]
    return Df

def apply_nonVBF_SR2_sel(Df):
    if len(Df.index) != 0:
        Df = Df[(Df['tau_0_pt']  > 45.) &
                (Df['ditau_mt_lep1_met'] < 40. ) &
                (Df['ditau_mt_lep0_met'] < 60.) &
                (Df['ditau_vis_mass'] > 100.) &
                (Df['n_bjets_DL1r_FixedCutBEff_85']  == 0) ]
    return Df

def apply_nonVBF_SR3_sel(Df):
    if len(Df.index) != 0:
        Df = Df[(Df['tau_0_pt']  < 45.) &
                (Df['tau_0_pt']  > 25.) &
                (Df['tau_1_pt']  > 45.) &
                (Df['ditau_mt_lep1_met'] > 40. ) &
                (Df['ditau_mt_lep0_met'] < 30.) &
                (Df['n_bjets_DL1r_FixedCutBEff_85']  == 0) ]
    return Df


def make_SR1_plots(data, mc_signal, mc, mc_list, nt, sr, mc_inputs, mc_labels, lumi_tag, title, mc_folder):
    #make copies
    dfc_data      = data.copy()
    dfc_mc_signal = mc_signal.copy()
    dfc_mc        = mc.copy()
    dfcs_mc = []
    if args.debug == True:
        print("[apply_SR1 before] DEBUG dfc_data = {}, dfc_mc_signal = {}, dfc_mc = {}".format(len(dfc_data.index),len(dfc_mc_signal.index),len(dfc_mc.index) ))
    dfc_data      = apply_nonVBF_SR1_sel(dfc_data)
    dfc_mc_signal = apply_nonVBF_SR1_sel(dfc_mc_signal)
    dfc_mc        = apply_nonVBF_SR1_sel(dfc_mc)
    for df in mc_list:
        if args.debug == True:
            print("[apply_SR1 before] DEBUG dfs_mc = {}".format(len(df.index) ))
        dd = df.copy()
        dd = apply_nonVBF_SR1_sel(dd)
        if args.debug == True:
            print("[apply_SR1 result] DEBUG dfs_mc = {}".format(len(dd.index) ))
        dfcs_mc.append(dd)
    name_tag   = nt+"_SR1"
    sel_region = sr+"_SR1"
    if args.debug == True:
        print("[apply_SR1 results] DEBUG dfc_data = {}, dfc_mc_signal = {}, dfc_mc = {}".format(len(dfc_data.index),len(dfc_mc_signal.index),len(dfc_mc.index) ))
    plot_ROOT_stack(dfc_data, dfc_mc_signal, dfc_mc, dfcs_mc, 
                    mc_inputs, mc_labels, 
                    lumi_tag, args.channel, title, mc_folder, sel_region, name_tag)
    
def make_SR2_plots(data, mc_signal, mc, mc_list, nt, sr, mc_inputs, mc_labels, lumi_tag, title, mc_folder):
    #make copies
        dfc_data      = data.copy()
        dfc_mc_signal = mc_signal.copy()
        dfc_mc        = mc.copy()
        dfcs_mc = []
        if args.debug == True:
            print("[apply_SR2 before] DEBUG dfc_data = {}, dfc_mc_signal = {}, dfc_mc = {}".format(len(dfc_data.index),len(dfc_mc_signal.index),len(dfc_mc.index) ))
        dfc_data      = apply_nonVBF_SR2_sel(dfc_data)
        dfc_mc_signal = apply_nonVBF_SR2_sel(dfc_mc_signal)
        dfc_mc        = apply_nonVBF_SR2_sel(dfc_mc)
        for df in mc_list:
            if args.debug == True:
                print("[apply_SR1 before] DEBUG dfs_mc = {}".format(len(df.index) ))
            dd = df.copy()
            dd = apply_nonVBF_SR2_sel(dd)
            if args.debug == True:
                print("[apply_SR1 result] DEBUG dfs_mc = {}".format(len(dd.index) ))
            dfcs_mc.append(dd)
        if args.debug == True:
            print("[apply_SR2 results] DEBUG dfc_data = {}, dfc_mc_signal = {}, dfc_mc = {}".format(len(dfc_data.index),len(dfc_mc_signal.index),len(dfc_mc.index) ))
        name_tag   = nt+"_SR2"
        sel_region = sr+"_SR2"
        plot_ROOT_stack(dfc_data, dfc_mc_signal, dfc_mc, dfcs_mc, 
                        mc_inputs, mc_labels, 
                        lumi_tag, args.channel, title, mc_folder, sel_region, name_tag)

def make_SR3_plots(data, mc_signal, mc, mc_list, nt, sr, mc_inputs, mc_labels, lumi_tag, title, mc_folder):
    #make copies
        dfc_data      = data.copy()
        dfc_mc_signal = mc_signal.copy()
        dfc_mc        = mc.copy()
        dfcs_mc = []
        if args.debug == True:
            print("[apply_SR3 before] DEBUG dfc_data = {}, dfc_mc_signal = {}, dfc_mc = {}".format(len(dfc_data.index),len(dfc_mc_signal.index),len(dfc_mc.index) ))
        dfc_data      = apply_nonVBF_SR3_sel(dfc_data)
        dfc_mc_signal = apply_nonVBF_SR3_sel(dfc_mc_signal)
        dfc_mc        = apply_nonVBF_SR3_sel(dfc_mc)
        for df in mc_list:
            dd = df.copy()
            dd = apply_nonVBF_SR3_sel(dd)
            dfcs_mc.append(dd)
        if args.debug == True:
            print("[apply_SR3 results] DEBUG dfc_data = {}, dfc_mc_signal = {}, dfc_mc = {}".format(len(dfc_data.index),len(dfc_mc_signal.index),len(dfc_mc.index) ))
        name_tag   = nt+"_SR3"
        sel_region = sr+"_SR3"
        plot_ROOT_stack(dfc_data, dfc_mc_signal, dfc_mc, dfcs_mc, 
                        mc_inputs, mc_labels, 
                        lumi_tag, args.channel, title, mc_folder, sel_region, name_tag)

def init_sys_wg(df, wg_ch):
    n_sys = 2
    if len(df.index)>0:
        for i in range(n_sys):
            wg = wg_ch + "_sys%i"%(i)
            df[wg] = df[wg_ch]
        
def eval_zee_sys(df, wg_ch, args, df_isData):
    #statistical error on eFF
    df[wg_ch+"_sys0"] = df.apply(lambda x: add_eFF_2D_3EtaBins(args.mc_data, args.channel, x[wg_ch+"_sys0"],
                                                               x['tau_0_pt'], x['tau_0_eta'], 
                                                               x['tau_0_n_charged_tracks'], 
                                                               df_isData, "medium!tight", addErr=True), axis=1) 
    #
    #eFF from the tight selection
    df[wg_ch+"_sys1"] = df.apply(lambda x: add_eFF_2D_3EtaBins(args.mc_data, args.channel, x[wg_ch+"_sys1"],
                                                               x['tau_0_pt'], x['tau_0_eta'], 
                                                               x['tau_0_n_charged_tracks'], 
                                                               df_isData, "tight"), axis=1) 
            
def make_stack_plots(args):
    var_cols = []
    vars_to_exclude = ['rebin','pdgId','dpt','taujet_dR']
    for v in vars:
        if any(x in v for x in vars_to_exclude):
            continue
        var_cols.append(v)
    var_cols.append('totalWeight_Ele')
    print("[make_stack_plots] cols = {}".format(var_cols))
    mc_var_cols = var_cols + ['tau_0_matched_pdgId']

    print("[make_stack_plots] mvisCut = {}".format(args.mvisCut))
    print("[make_stack_plots] DD = {}".format(args.data_driven))
    print("[make_stack_plots] min mvisCut = {}".format(args.min_mass))
    data_folder = []
    data_inputs = []
    if args.mc_data == 'mc16e':
        data_folder.append(working_node+"/plots_"+args.tag+"_data18/")
        lumi_tag='data18'
        data_inputs.append('data18')
    elif args.mc_data == 'mc16d':
        data_folder.append(working_node+"/plots_"+args.tag+"_data17/")
        lumi_tag='data17'
        data_inputs.append('data17')
    elif args.mc_data == 'mc16a':
        data_folder.append(working_node+"/plots_"+args.tag+"_data15/")
        data_folder.append(working_node+"/plots_"+args.tag+"_data16/")
        lumi_tag='data1516'
        data_inputs.append('data15')
        data_inputs.append('data16')
    elif args.mc_data == 'all':
        data_folder.append(working_node+"/plots_"+args.tag+"_data15/")
        data_folder.append(working_node+"/plots_"+args.tag+"_data16/")
        data_folder.append(working_node+"/plots_"+args.tag+"_data17/")
        data_folder.append(working_node+"/plots_"+args.tag+"_data18/")
        lumi_tag='data15161718'
        data_inputs.append('data15')
        data_inputs.append('data16')
        data_inputs.append('data17')
        data_inputs.append('data18')

    print("[make_stack_plots::make_stack_plots] data folder to inspect: {}".format(data_folder))

    datafiles = []
    dfs_data = []
    print("[make_stack_plots::make_stack_plots] data files for {}".format(args.sel_region))
    for dd in data_folder: #tqdm(data_folder, total=len(data_folder), desc="data files from {} {}".format(args.channel, args.sel_region), file=sys.stdout):
        d_file = dd+args.channel+"/Data/csv/data_{}_{}".format(args.channel, args.sel_region+"_filtered.csv")
        if os.path.exists(d_file):
            datafiles.append(d_file)
            df = pd.read_csv(d_file, usecols =var_cols)
            if args.same_sign:
                df = df[(df['os_leptau'] == False)]
            else:
                df = df[(df['os_leptau'] == True)]
            dfs_data.append(df)
        else:
            print("{} not found...".format(d_file))
    df_data = pd.concat(dfs_data, axis=0, sort=False)
    df_data = df_data[(df_data['ditau_vis_mass']>args.min_mass)]

    if args.sel_region == 'nonVBF' and args.mvisCut:
        df_data = df_data[(df_data['pass_ditauVisMassCut'] == True)]

    #now get the background MC dataframes
    sr = args.sel_region
    mc_folder = []
    if args.mc_data != 'all':
        mc_folder.append(working_node+"/plots_"+args.tag+"_{}/".format(args.mc_data)+args.channel)
    else:
        for mc in ['mc16a','mc16d','mc16e']:
            mc_folder.append(working_node+"/plots_"+args.tag+"_{}/".format(mc)+args.channel)
    mc_inputs = []
    mc_labels = []
    dfs_mc    = []
    if args.channel == 'etau':
        wg_ch = 'totalWeight_Ele'
    elif args.channel == 'mutau':
        wg_ch = 'totalWeight_Mu'
    wg_ch_sys0 = wg_ch+"_sys0"
    wg_ch_sys1 = wg_ch+"_sys1"

    if sr == 'VBF' or sr == 'nonVBF':
        mc_inputs = ['DiBoson', 'HWW', 'SMH', 'Top', 'W_Jets', 'Zll_Jets', 'Ztautau']
        mc_labels = ['DiBoson', 'H#rightarrowWW', 'SM Higgs', 'Top', 'W+jets', 'Z#rightarrow ll', 'Z#rightarrow #tau#tau']
    elif sr == 'ZeeCR' or sr == 'ZeeCR_antiID':
        # mc_inputs = ['DiBoson', 'HWW', 'SMH', 'Top', 'W_Jets', 'Ztautau']
        # mc_labels = ['DiBoson', 'H#rightarrow WW', 'SM Higgs', 'Top', 'W+jets', 'Z#rightarrow #tau#tau']
        mc_inputs = ['DiBoson', 'HWW', 'SMH', 'Top', 'W_Jets', 'Zee', 'Ztautau']
        mc_labels = ['DiBoson', 'H#rightarrowWW', 'SM Higgs', 'Top', 'W+jets', 'Z#rightarrow ee', 'Z#rightarrow #tau#tau']
 
    print("[make_stack_plots::make_stack_plots] MC folder to inspect: {}, {}".format(mc_folder, mc_inputs))
    print("[make_stack_plots::make_stack_plots] MC files for {}".format(args.sel_region))
    for mc in mc_inputs:#tqdm(mc_inputs, total=len(mc_inputs), desc="MC files from {} {}".format(args.channel, args.sel_region), file=sys.stdout):
        if args.data_driven == True and (mc == 'Zll_Jets' or mc =='Zee'):
            print("[] skipping MC from {}...".format(mc))
            continue
        else:
            dfs_mc_ch = []
            for mc_dir in mc_folder:
                mc_file = mc_dir+"/{}/csv/data_{}_{}".format(mc, args.channel, args.sel_region)+"_filtered.csv"      
                if args.debug == True:
                    print("[make_stack_plots::make_stack_plots] reading {}".format(mc_file))
                if os.path.exists(mc_file):
                    df = pd.read_csv(mc_file, usecols =mc_var_cols)
                    #if sr != 'ZeeCR':
                    if args.same_sign:
                        df = df[(df['os_leptau'] == False)]
                    else:
                        df = df[(df['os_leptau'] == True)]
                    if args.sel_region == 'nonVBF' and args.mvisCut:
                        df = df[(df['pass_ditauVisMassCut'] == True)]
                    dfs_mc_ch.append(df)
                else:
                    print("[make_stack_plots::make_stack_plots] file {} NOT FOUND".format(mc_file))
                    dfs_mc_ch.append(pd.DataFrame())
            dfs_mc_merge = pd.concat(dfs_mc_ch, axis=0, sort=False) 

            if args.sel_region == 'nonVBF' and mc == 'Ztautau':
                sf_Ztt_nonVBF       = 0.92
                dfs_mc_merge[wg_ch] = sf_Ztt_nonVBF*dfs_mc_merge[wg_ch]
            init_sys_wg(dfs_mc_merge, wg_ch)
            #dfs_mc_merge[wg_ch_sys] = dfs_mc_merge[wg_ch]
            if len(dfs_mc_merge.index)>0:
                dfs_mc_merge = dfs_mc_merge[(dfs_mc_merge['ditau_vis_mass']>args.min_mass)]
                print("[] {} total = {:10.2f} {:10.2f} {:10.3f}".format(mc, sum(dfs_mc_merge[wg_ch]), sum(dfs_mc_merge[wg_ch_sys0]), sum(dfs_mc_merge[wg_ch_sys1])))
            
            dfs_mc.append(dfs_mc_merge)
    #
    print("MC dataframse loaded = {:2.0f}".format(len(dfs_mc)))
    if args.data_driven == True:
        inputs_zee_dd = data_inputs + ['DiBoson', 'HWW', 'SMH', 'Top', 'W_Jets', 'Ztautau']
        dfs_zee_dd = []
        print("[make_stack_plots::make_stack_plots]*******************************")
        print("[make_stack_plots::make_stack_plots]**    DD evaluation starts!  **")
        print("[make_stack_plots::make_stack_plots]*******************************")
        for zee_dd in inputs_zee_dd: #tqdm(inputs_zee_dd, total=len(inputs_zee_dd), desc="ZEE antiID files from {} {}".format(args.channel, args.sel_region), file=sys.stdout):
            df_isData=False
            zee_dd_file = []
            antiID_label = 'Zee_antiID'
            if sr == 'ZeeCR':
                antiID_label = 'antiID'
            if zee_dd in data_inputs:
                zee_dd_file.append(working_node+"/plots_"+args.tag+"_"+zee_dd+"/{}/Data/csv/data_{}_{}_{}".format(
                    args.channel, args.channel, args.sel_region, antiID_label)+"_filtered.csv")
                df_isData = True
            else:
                for mc_dir in mc_folder:
                    zee_dd_file.append(mc_dir+"/{}/csv/data_{}_{}_{}".format(
                        zee_dd, args.channel, args.sel_region, antiID_label)+"_filtered.csv")

            for zee_dd_f in zee_dd_file:
                if args.debug == True:
                    print("[make_stack_plots::make_stack_plots] reading file {}".format(zee_dd_f))
                if os.path.exists(zee_dd_f):
                    ddf = pd.read_csv(zee_dd_f, usecols =var_cols)
                    ddf = ddf[(ddf['tau_0_n_charged_tracks'] == 1)]
                    if df_isData == False:
                        ddf[wg_ch] = -1.*ddf[wg_ch]
                        
                    dfs_zee_dd.append(ddf)
                    if args.debug == True:
                        print("Zee from {}: {:10.2f} ({:10.2f})".format(zee_dd, sum(ddf[wg_ch]), len(ddf.index)))
                else:
                    dfs_zee_dd.append(pd.DataFrame())
                    print("file {} NOT FOUND!".format(zee_dd_f))
        #add the contirbution from the jet facking taus
        print("[make_stack_plots::make_stack_plots]***************************************************")
        print("[make_stack_plots::make_stack_plots] removing the contribution form jet faking taus...")
        print("[make_stack_plots::make_stack_plots]***************************************************")
        inputs_zee_dd.append("Zee")
        for zee_dd in inputs_zee_dd: #data_inputs: #tqdm(data_inputs, total=len(data_inputs), desc="ZEE from jet facking taus", file=sys.stdout):
            zee_dd_file = []
            fakes_label = 'fakes_antiID_Zee_antiID'
            if sr == 'ZeeCR':
                fakes_label = 'antiID_fakes_antiID'                
                # fakes_label = 'fakes_antiID'                
            df_isData = True
            if zee_dd in data_inputs:
                zee_dd_file.append(working_node+"/plots_"+args.tag+"_"+zee_dd+"/{}/Data/csv/data_{}_{}_{}".format(
                    args.channel, args.channel, args.sel_region, fakes_label)+"_filtered.csv")      
                df_isData = False
            else:
                for mc_dir in mc_folder:
                    zee_dd_file.append(mc_dir+"/{}/csv/data_{}_{}_{}".format(
                        zee_dd, args.channel, args.sel_region, fakes_label)+"_filtered.csv")
            #
            for zee_dd_f in zee_dd_file:
                if args.debug == True:
                    print("[make_stack_plots::make_stack_plots] reading file {}".format(zee_dd_f))
                if os.path.exists(zee_dd_f):
                    ddf = pd.read_csv(zee_dd_f, usecols =var_cols)
                    ddf = ddf[(ddf['ditau_vis_mass']>args.min_mass)]
                    ddf = ddf[(ddf['tau_0_n_charged_tracks'] == 1)]

                    # ddf        = ddf[(ddf['tau_0_matched_pdgId']!=11)]
                    mc_label = getMCLabel(zee_dd_f)
                    zee_antiID_label = "Zee_"+args.sel_region+"_antiID"
                    if args.sel_region == "ZeeCR":
                        zee_antiID_label = args.sel_region+"_antiID"                    
                    if args.debug == True:
                        print("[{}, {}] applying FF_3EtaBins... before:{:6.2f}".format(zee_dd, mc_label, sum(ddf[wg_ch])))
                    ddf[wg_ch] = ddf.apply(lambda x: add_FF(mc_label, args.channel, zee_antiID_label, 
                                                            x[wg_ch], 
                                                            x['tau_0_pt'], 
                                                            x['ditau_mt_lep1_met'],
                                                            x['tau_0_n_charged_tracks'],
                                                            x['dphi_mettau']),
                                           axis=1)
                    if df_isData == False:
                        ddf[wg_ch] = -1.*ddf[wg_ch]
                    if args.debug == True:
                        print("[] applying FF_3EtaBins... after:{:6.2f}".format(sum(ddf[wg_ch])))

                    if args.same_sign:
                        ddf = ddf[(ddf['os_leptau'] == False)]
                    else:
                        ddf = ddf[(ddf['os_leptau'] == True)]
                        # ddf = ddf[(ddf['tau_0_ele_bdt_score_trans_retuned'] > 0.005)]
                    if args.debug == True:
                        print("fakes Zee from data {} to subtract {:10.2f} ({:10.2f})".format(zee_dd_f, sum(ddf[wg_ch]), len(ddf.index)))
                    #if sr != 'ZeeCR':#this is a test
                    dfs_zee_dd.append(ddf)
                else:
                    print("[dd Zee from jet facking taus] file {} NOT FOUND!!".format(zee_dd_f))
        #
        #concatenate everything
        df_zee_dd = pd.concat(dfs_zee_dd, axis=0, sort=False)
        df_zee_dd = df_zee_dd[(df_zee_dd['ditau_vis_mass']>args.min_mass)]

        if args.sel_region == 'nonVBF' and args.mvisCut:
            df_zee_dd = df_zee_dd[(df_zee_dd['pass_ditauVisMassCut'] == True)]
        if args.same_sign:
            df_zee_dd = df_zee_dd[(df_zee_dd['os_leptau'] == False)]
        else:
            if 'os_leptau' in df_zee_dd:
                df_zee_dd = df_zee_dd[(df_zee_dd['os_leptau'] == True)]
        #--------------------------------------------------------------------------------
        #--------------------------------------------------------------------------------        
        print("Zee data-driven estimate BEFORE eFF: {:10.3f}".format(sum(df_zee_dd[wg_ch])))
        df_isData = True
        if args.eFF_version == '2D':
            init_sys_wg(df_zee_dd, wg_ch)
            # df_zee_dd[wg_ch_sys] = df_zee_dd[wg_ch]
            df_zee_dd[wg_ch]     = df_zee_dd.apply(lambda x: add_eFF_2D(args.mc_data, args.channel, x[wg_ch],
                                                                    x['tau_0_pt'], x['tau_0_eta'],
                                                                    x['tau_0_n_charged_tracks'], df_isData), axis=1) 
            df_zee_dd[wg_ch_sys0] = df_zee_dd.apply(lambda x: add_eFF_2D(args.mc_data, args.channel, x[wg_ch_sys0],
                                                                        x['tau_0_pt'], x['tau_0_eta'],
                                                                        x['tau_0_n_charged_tracks'], df_isData, addErr=True), axis=1) 
        elif args.eFF_version == '3EtaBins':
            if args.debug == True:
                print("[] applying eFF_3EtaBins... before:{:6.2f}".format(sum(df_zee_dd[wg_ch])))
            init_sys_wg(df_zee_dd, wg_ch)
            # df_zee_dd[wg_ch_sys] = df_zee_dd[wg_ch]
            df_zee_dd[wg_ch]     = df_zee_dd.apply(lambda x: add_eFF_2D_3EtaBins(args.mc_data, args.channel, x[wg_ch],
                                                                                 x['tau_0_pt'], x['tau_0_eta'], 
                                                                                 x['tau_0_n_charged_tracks'], 
                                                                                 df_isData), axis=1) 
            eval_zee_sys(df_zee_dd, wg_ch, args, df_isData)
            # df_zee_dd[wg_ch_sys] = df_zee_dd.apply(lambda x: add_eFF_2D_3EtaBins(args.mc_data, args.channel, x[wg_ch_sys],
            #                                                                      x['tau_0_pt'], x['tau_0_eta'], 
            #                                                                      x['tau_0_n_charged_tracks'], 
            #                                                                      df_isData, addErr=True), axis=1) 
            if args.debug == True:
                print("[] applying eFF_3EtaBins... after :{:6.2f}".format(sum(df_zee_dd[wg_ch])))
        else:
            init_sys_wg(df_zee_dd, wg_ch)
            # df_zee_dd[wg_ch_sys] = df_zee_dd[wg_ch]
            df_zee_dd[wg_ch]     = df_zee_dd.apply(lambda x: add_eFF(args.mc_data, args.channel, x[wg_ch],
                                                                     x['tau_0_pt'], x['tau_0_n_charged_tracks'], df_isData), axis=1)
            #eval_zee_sys(df_zee_dd, wg_ch, args, df_isData)
            df_zee_dd[wg_ch_sys0] = df_zee_dd.apply(lambda x: add_eFF(args.mc_data, args.channel, x[wg_ch_sys0],
                                                                     x['tau_0_pt'], x['tau_0_n_charged_tracks'], df_isData, addErr=True), axis=1)
        print("Zee data-driven estimate After eFF: {:10.3f}".format(sum(df_zee_dd[wg_ch])))
        print("Zee data-driven estimate including eFF systematic: {:10.3f}".format(sum(df_zee_dd[wg_ch_sys0])))
        #now we need to add the 3P component, which is estimated from the MC w/o using the eFF
        if 'ZeeCR' not in args.sel_region :
            print("[make_stack_plots::make_stack_plots] evaluating the 3P e events...")
            dfs_zee_dd_3p = []
            for mc in ['Zee']: #mc_inputs:#tqdm(mc_inputs, total=len(mc_inputs), desc="MC files from {} {}".format(args.channel, args.sel_region), file=sys.stdout):
                for mc_dir in mc_folder:
                    mc_file = mc_dir+"/{}/csv/data_{}_{}".format(mc, args.channel, args.sel_region)+"_filtered.csv"      
                    if args.debug == True:
                        print("[make_stack_plots::make_stack_plots] reading {}".format(mc_file))
                    if os.path.exists(mc_file):
                        df = pd.read_csv(mc_file, usecols =mc_var_cols)
                        df = df[(df['ditau_vis_mass']>args.min_mass)]
                        df = df[(df['tau_0_n_charged_tracks'] == 3) &
                                (np.abs(df['tau_0_matched_pdgId']) == 11)]
                        if args.same_sign:
                            df = df[(df['os_leptau'] == False)]
                        else:
                            df = df[(df['os_leptau'] == True)]
                        if args.sel_region == 'nonVBF' and args.mvisCut:
                            df = df[(df['pass_ditauVisMassCut'] == True)]

                        # df[wg_ch_sys] = df[wg_ch]
                        init_sys_wg(df, wg_ch)
                        dfs_zee_dd_3p.append(df)
                    else:
                        print("[make_stack_plots::make_stack_plots] file {} NOT FOUND".format(mc_file))
                        dfs_zee_dd_3p.append(pd.DataFrame())
            dfs_zee_dd_3p.append(df_zee_dd)
            df_zee_dd = pd.concat(dfs_zee_dd_3p, axis=0, sort=False)
            df_zee_dd = df_zee_dd[(df_zee_dd['ditau_vis_mass']>args.min_mass)]

            print("Zee data-driven estimate adding 3P : {:10.3f} {:10.3f} {:10.3f}".format(sum(df_zee_dd[wg_ch]), sum(df_zee_dd[wg_ch_sys0]), sum(df_zee_dd[wg_ch_sys1])))

        #--------------------------------------------------------------------------------
        #--------------------------------------------------------------------------------
        if args.save_fakes:
            zee_dd_df_fileName = mc_folder[0]+"/Zee_dd"
            if args.mc_data == 'all':
                zee_dd_df_fileName = zee_dd_df_fileName+"_all"
            if not os.path.exists(zee_dd_df_fileName):
                os.makedirs("{}".format(zee_dd_df_fileName))
            zee_dd_df_fileName = zee_dd_df_fileName+"/csv"
            if not os.path.exists(zee_dd_df_fileName):
                os.makedirs("{}".format(zee_dd_df_fileName))
            ntag = args.sel_region
            # if args.mvisCut == False:
            #     ntag = ntag + "_noMVisCut"
            zee_dd_df_fileName = zee_dd_df_fileName+"/data_{}_{}_zee_dd_{}_filtered.csv".format(
                args.channel, ntag, args.FF_version)
            df_zee_dd.to_csv(zee_dd_df_fileName)
        dfs_mc.insert(5, df_zee_dd)
        
    df_mc = pd.concat(dfs_mc, axis=0, sort=False)
    df_mc = df_mc[(df_mc['ditau_vis_mass']>args.min_mass)]
    print("MC events : {:10.3f} {:10.3f} {:10.3f}".format(sum(df_mc[wg_ch]), sum(df_mc[wg_ch_sys0]), sum(df_mc[wg_ch_sys1])))
    #now get the signal MC dataframes
    mc_signal_fname = []
    if args.mc_data != 'all':
        mc_signal_fname.append(working_node+"/plots_"+args.tag+"_{}/{}/LFV/csv/data_{}_{}".format(args.mc_data, args.channel, args.channel, args.sel_region)+"_filtered.csv")
    else:
        for mc in ['mc16a','mc16d','mc16e']:
            mc_signal_fname.append(working_node+"/plots_"+args.tag+"_{}/{}/LFV/csv/data_{}_{}".format(
                mc, args.channel, args.channel, args.sel_region)+"_filtered.csv")

    dfs_mc_signal = []
    for mc_signal_f in mc_signal_fname:
        if os.path.exists(mc_signal_f):
            df_mcsignal = pd.read_csv(mc_signal_f, usecols =mc_var_cols)        
            mc_signal_sf = 10.
            df_mcsignal[wg_ch] = df_mcsignal[wg_ch]*mc_signal_sf
        else:
            print("file {} NOT FOUND!".format(mc_signal_f))
            df_mcsignal = pd.DataFrame()
        dfs_mc_signal.append(df_mcsignal)
    df_mc_signal = pd.concat(dfs_mc_signal, axis=0, sort=False)
    df_mc_signal = df_mc_signal[(df_mc_signal['ditau_vis_mass']>args.min_mass)]

    print("mc_signal = {}".format(len(df_mc_signal.index)))
    #if sr != 'ZeeCR':
    if args.same_sign:
        df_mc_signal = df_mc_signal[(df_mc_signal['os_leptau'] == False)]
    else:
        df_mc_signal = df_mc_signal[(df_mc_signal['os_leptau'] == True)]
        
    if args.sel_region == 'nonVBF' and args.mvisCut:
        df_mc_signal = df_mc_signal[(df_mc_signal['pass_ditauVisMassCut'] == True)]

    sel_region = args.sel_region
    #now create the dataframe for the fakes from data
    dfs_data_fakes   = []
    print("[make_stack_plots::make_stack_plots] reading data for evaluating the fakes")
    for dd in data_folder: #tqdm(data_folder, total=len(data_folder), desc="fake data files from {} {}".format(args.channel, args.sel_region), file=sys.stdout):
        d_file = dd+args.channel+"/Data/csv/data_{}_{}_fakes_antiID_filtered.csv".format(args.channel, args.sel_region)
        if args.debug == True:
            print("[make_stack_plots::make_stack_plots] reading file {}".format(d_file))
        if os.path.exists(d_file):
            df_d = pd.read_csv(d_file, usecols =var_cols)
            df_d = df_d[(df_d['ditau_vis_mass']>args.min_mass)]
            label = dd[len(dd)-7:len(dd)-1]
            if args.debug == True:
                print("data label = {}".format(label))
            mc_label = getMCLabel(label)
            df_d[wg_ch] = df_d.apply(lambda x: add_FF(mc_label, args.channel, sel_region,
                                                      x[wg_ch], 
                                                      x['tau_0_pt'], 
                                                      x['ditau_mt_lep1_met'],
                                                      x['tau_0_n_charged_tracks'],
                                                      x['dphi_mettau']),
                                     axis=1)
            if args.debug == True:
                print("fakes from {} {:10.2f} ({:10.2f})".format(label, sum(df_d[wg_ch]), len(df_d.index)))
            dfs_data_fakes.append(df_d)
        else:
            print("{} not found...".format(d_file))
    df_data_fakes = pd.concat(dfs_data_fakes, axis=0, sort=False)
    df_data_fakes = df_data_fakes[(df_data_fakes['ditau_vis_mass']>args.min_mass)]

    #include the FF in the weight variable
    # df_data_fakes[wg_ch] = df_data_fakes.apply(lambda x: add_FF(args.mc_data, args.channel, sel_region,
    #                                                             x[wg_ch], 
    #                                                             x['tau_0_pt'], 
    #                                                             x['ditau_mt_lep1_met'],
    #                                                             x['tau_0_n_charged_tracks'],
    #                                                             x['dphi_mettau']),
    #                                            axis=1)
    # if sr != 'ZeeCR':
    if args.same_sign:
        df_data_fakes = df_data_fakes[(df_data_fakes['os_leptau'] == False)]
    else:
        df_data_fakes = df_data_fakes[(df_data_fakes['os_leptau'] == True)]
    if args.sel_region == 'nonVBF' and args.mvisCut:
        df_data_fakes = df_data_fakes[(df_data_fakes['pass_ditauVisMassCut'] == True)]

    print("[] fakes from data files = {:10.2f} ({:10.2f})".format(sum(df_data_fakes[wg_ch]), len(df_data_fakes.index)))

    #now create the dataframe for the fakes from MC
    dfs_mc_fakes   = []
    mc_inputs2 = mc_inputs #['DiBoson', 'HWW', 'SMH', 'Top', 'W_Jets', 'Ztautau']
    print("[make_stack_plots::make_stack_plots] now evaluate the fakes from the MC...")
    for mc in mc_inputs2: #tqdm(mc_inputs2, total=len(mc_inputs2), desc="Fakes from MC files from {} {}".format(args.channel, args.sel_region), file=sys.stdout):
        for mc_dir in mc_folder:
            mc_file = mc_dir+"/{}/csv/data_{}_{}_fakes_antiID".format(mc, args.channel, args.sel_region)+"_filtered.csv"    
            if args.debug == True:
                print("[make_stack_plots::make_stack_plots] reading file {}".format(mc_file))      
            if os.path.exists(mc_file):
                df_mc_f = pd.read_csv(mc_file, usecols =var_cols)
                #include the FF in the weight variable
                mc_label = mc_dir[len(mc_dir)-10:len(mc_dir)-5]
                if args.debug == True:
                    print("data label = {}".format(mc_label))
                df_mc_f[wg_ch] = df_mc_f.apply(lambda x: add_FF(mc_label, args.channel, sel_region,
                                                            x[wg_ch], x['tau_0_pt'], 
                                                            x['ditau_mt_lep1_met'],
                                                            x['tau_0_n_charged_tracks'],
                                                            x['dphi_mettau']),
                                           axis=1)
                dfs_mc_fakes.append(df_mc_f)
                if args.debug == True:
                    print("fakes from {}, {} = {:10.20f}".format(mc, mc_label, sum(df_mc_f[wg_ch])))
            else:
                dfs_mc_fakes.append(pd.DataFrame())
                print("[make_stack_plots::make_stack_plots] file {} NOT FOUND!".format(mc_file))

    df_mc_fakes = pd.concat(dfs_mc_fakes, axis=0, sort=False)
    df_mc_fakes = df_mc_fakes[(df_mc_fakes['ditau_vis_mass']>args.min_mass)]
    
    df_mc_fakes[wg_ch] = -1.*df_mc_fakes[wg_ch]
    if args.sel_region == 'nonVBF' and args.mvisCut:
        df_mc_fakes = df_mc_fakes[(df_mc_fakes['pass_ditauVisMassCut'] == True)]

    df_fakes = pd.concat([df_data_fakes, df_mc_fakes] , axis=0, sort=False)
    # df_fakes[wg_ch_sys] = df_fakes[wg_ch]
    init_sys_wg(df_fakes, wg_ch)
    df_mc    = pd.concat([df_mc, df_fakes] , axis=0, sort=False)

    df_fakes = df_fakes[(df_fakes['ditau_vis_mass']>args.min_mass)]
    df_mc    = df_mc[(df_mc['ditau_vis_mass']>args.min_mass)]

    # if sr != 'ZeeCR':
    if args.same_sign:
        df_mc_fakes = df_mc_fakes[(df_mc_fakes['os_leptau'] == False)]
        df_fakes = df_fakes[(df_fakes['os_leptau'] == False)]
        df_mc = df_mc[(df_mc['os_leptau'] == False)]
    else:
        df_mc_fakes = df_mc_fakes[(df_mc_fakes['os_leptau'] == True)]
        df_fakes = df_fakes[(df_fakes['os_leptau'] == True)]
        df_mc = df_mc[(df_mc['os_leptau'] == True)]

    print("[] fakes from MC files = {:10.2f}".format(sum(df_mc_fakes[wg_ch])))
    print("[] fakes total = {:10.2f}".format(sum(df_fakes[wg_ch])))


    dfs_mc.append(df_fakes)
    if args.save_fakes:
        fakes_df_fileName = mc_folder[0]+"/Fakes"
        if args.mc_data == 'all':
            fakes_df_fileName = fakes_df_fileName + "_all"
        if not os.path.exists(fakes_df_fileName):
            os.makedirs("{}".format(fakes_df_fileName))
        fakes_df_fileName = fakes_df_fileName+"/csv"
        if not os.path.exists(fakes_df_fileName):
            os.makedirs("{}".format(fakes_df_fileName))
        ntag = args.sel_region
        # if args.mvisCut == False:
        #     ntag = ntag + "_noMVisCut"
        fakes_df_fileName = fakes_df_fileName+"/data_{}_{}_fakes_antiID_{}_filtered.csv".format(
            args.channel, ntag, args.FF_version)
        df_fakes.to_csv(fakes_df_fileName)
        print('saved fakes dataframe: {}'.format(fakes_df_fileName))

    print('dfs_mc_columns = {} , dfs_fakes_columns = {}'.format(len(df_mc), len(df_mc_fakes)))
    mc_inputs.append('Fakes')
    mc_labels.append('Fakes')

    if args.channel == 'etau':
        title = "H#rightarrow e#tau_{had}"
    elif args.channel == 'mutau':
        title = "H#rightarrow#mu#tau_{had}"
    # name_tag='stack'
    # name_tag='stack_Zee'
    nt = sel_region+'_stack_FF'+args.FF_version

    if args.mvisCut == False:
        nt  = nt+"_noMVisCut"
        sr  = sr+"_noMVisCut"
    if args.min_mass >0:
        nt  = nt + "_minMVis_{:2d}".format(int(args.min_mass))
        sr  = sr + "_minMVis_{:2d}".format(int(args.min_mass))
    if args.same_sign:
        nt  = nt+"_ss"
        sr  = sr+"_ss"
    if args.data_driven:
        nt  = nt+"_dd"
        sr  = sr+"_dd"
    if args.mc_data == 'all':
        nt  = nt+"_allData"
        sr  = sr+"_allData"
    else:
        nt  = nt+"_"+args.mc_data
        sr  = sr+"_"+args.mc_data
    if args.data_driven == True:
        if args.eFF_version == '2D':
            nt  = nt+"_eFF2D"
            sr  = sr+"_eFF2D"
        elif args.eFF_version == '3EtaBins':
            nt  = nt+"_eFF3EtaBins"
            sr  = sr+"_eFF3EtaBins"
        else:
            nt  = nt+"_eFF1D"
            sr  = sr+"_eFF1D"
    if args.add_sys:
        nt  = nt+"_wSys"
        sr  = sr+"_wSys"
        
    print("[make_stack_plots] mvisCut = {}, nt = {}, sr = {}".format(args.mvisCut, nt, sr))

    sr2 = args.sel_region_2
    
    #DEBUG
    tmp_mc =[]
    # n_jets_30 = 0
    # jet_max_pt = 10.
    # min_eVeto_BDT=0.01
    for df in dfs_mc:
        if len(df.index) >0:
            # df = df[(df['n_jets_30'] == n_jets_30)]
            # df = df[(df['jet_0_pt'] < jet_max_pt)]
            df['ditau_dpt'] = df.apply(lambda x: x['tau_0_pt'] - x['tau_1_pt'], axis=1, raw=True)
            # df['dijet_dR']  = df.apply(lambda x: deltaR(x['jet_0_eta'],x['jet_0_phi'], 
            #                                             x['jet_1_eta'],x['jet_1_phi']), axis=1, raw=True)
            df['taujet_dR']  = df.apply(lambda x: deltaRJetTau(x['jet_0_pt'], x['jet_0_eta'],x['jet_0_phi'], 
                                                               x['tau_0_eta'],x['tau_0_phi']), axis=1, raw=True)
            if args.only_xp:
                df = df[(df['tau_0_n_charged_tracks'] == args.nprongs)]
            #df = df[(df['ditau_dpt'] <0)]
            # df = df[(df['met_pt'] < 25.)]
            # df = df[(df['tau_0_eta'] > -0.1245) & (df['tau_0_eta'] < 0.1245)]
            # df = df[(df['tau_0_ele_bdt_score_trans_retuned'] > min_eVeto_BDT)]
            
        tmp_mc.append(df)
    dfs_mc = tmp_mc

    df_data     ['ditau_dpt'] = df_data.apply(lambda x: x['tau_0_pt'] - x['tau_1_pt'], axis=1, raw=True)
    # df_data     ['dijet_dR']  = df_data.apply(lambda x: deltaR(x['jet_0_eta'],x['jet_0_phi'], 
    #                                                            x['jet_1_eta'],x['jet_1_phi']), axis=1, raw=True)
    df_data    ['taujet_dR']  = df_data.apply(lambda x: deltaRJetTau(x['jet_0_pt'], x['jet_0_eta'],x['jet_0_phi'], 
                                                                     x['tau_0_eta'],x['tau_0_phi']), axis=1, raw=True)
    df_mc       ['ditau_dpt'] = df_mc.apply(lambda x: x['tau_0_pt'] - x['tau_1_pt'], axis=1, raw=True)
    # df_mc       ['dijet_dR']  = df_mc.apply(lambda x: deltaR(x['jet_0_eta'],x['jet_0_phi'], 
    #                                                          x['jet_1_eta'],x['jet_1_phi']), axis=1, raw=True)
    df_mc       ['taujet_dR'] = df_mc.apply(lambda x: deltaRJetTau(x['jet_0_pt'], x['jet_0_eta'],x['jet_0_phi'], 
                                                                   x['tau_0_eta'],x['tau_0_phi']), axis=1, raw=True)
    df_mc_signal['ditau_dpt'] = df_mc_signal.apply(lambda x: x['tau_0_pt'] - x['tau_1_pt'], axis=1, raw=True)
    # df_mc_signal['dijet_dR']  = df_mc_signal.apply(lambda x: deltaR(x['jet_0_eta'],x['jet_0_phi'], 
    #                                                                 x['jet_1_eta'],x['jet_1_phi']), axis=1, raw=True)
    df_mc_signal['taujet_dR'] = df_mc_signal.apply(lambda x: deltaRJetTau(x['jet_0_pt'], x['jet_0_eta'],x['jet_0_phi'], 
                                                                          x['tau_0_eta'],x['tau_0_phi']), axis=1, raw=True)
    if args.only_xp:
        df_data      = df_data[(df_data['tau_0_n_charged_tracks'] == args.nprongs)]
        df_mc        = df_mc[(df_mc['tau_0_n_charged_tracks'] == args.nprongs)]
        df_mc_signal = df_mc_signal[(df_mc_signal['tau_0_n_charged_tracks'] == args.nprongs)]
        sr = sr + "_only{:d}P".format(args.nprongs)
        nt = nt + "_only{:d}P".format(args.nprongs)
    #debugging ZeeCR
    # df_data      = df_data[(df_data['n_jets_30']           == n_jets_30)]
    # df_mc        = df_mc[(df_mc['n_jets_30']               == n_jets_30)]
    # df_mc_signal = df_mc_signal[(df_mc_signal['n_jets_30'] == n_jets_30)]
    # #
    # df_data      = df_data[(df_data['jet_0_pt']           < jet_max_pt)]
    # df_mc        = df_mc[(df_mc['jet_0_pt']               < jet_max_pt)]
    # df_mc_signal = df_mc_signal[(df_mc_signal['jet_0_pt'] < jet_max_pt)]
    # #
    # df_data      = df_data[(df_data['ditau_dpt']           < 0)]
    # df_mc        = df_mc[(df_mc['ditau_dpt']               < 0)]
    # df_mc_signal = df_mc_signal[(df_mc_signal['ditau_dpt'] < 0)]
    #
    # df_data      = df_data[(df_data['tau_0_eta'] > -0.1245) & (df_data['tau_0_eta'] < 0.1245)]
    # df_mc        = df_mc[(df_mc['tau_0_eta'] > -0.1245) & (df_mc['tau_0_eta'] < 0.1245)]
    # df_mc_signal = df_mc_signal[(df_mc_signal['tau_0_eta'] > -0.1245) & (df_mc_signal['tau_0_eta'] < 0.1245)]
    # df_data      = df_data[(df_data['tau_0_ele_bdt_score_trans_retuned']           > min_eVeto_BDT)]
    # df_mc        = df_mc[(df_mc['tau_0_ele_bdt_score_trans_retuned']               > min_eVeto_BDT)]
    # df_mc_signal = df_mc_signal[(df_mc_signal['tau_0_ele_bdt_score_trans_retuned'] > min_eVeto_BDT)]
    #
    # sr = sr + "_debug3"
    # nt = nt + "_debug3"

    if ZeeCR_wp == 'tight':
        #swap the weights
        #df_data[wg_ch]      = df_data[wg_ch_sys1]
        #df_mc_signal[wg_ch] = df_mc_signal[wg_ch_sys1]
        df_mc[wg_ch]        = df_mc[wg_ch_sys1]
        for ddf in dfs_mc:
            if len(ddf.index)>0:
                ddf[wg_ch] = ddf[wg_ch_sys1]

    if sr2 =='preselection':
        sel_region = sr+'_preselection'
        name_tag   = nt
        plot_ROOT_stack(df_data, df_mc_signal, df_mc, dfs_mc, 
                        mc_inputs, mc_labels, 
                        lumi_tag, args.channel, title, mc_folder[0], sel_region, name_tag, add_sys=args.add_sys)

    if sr == 'VBF' and sr2 == 'VBFR': #we have only 1 SR
        df_data      = apply_VBFR_sel(df_data)
        df_mc_signal = apply_VBFR_sel(df_mc_signal)
        df_mc        = apply_VBFR_sel(df_mc)
        dfcs_mc = []
        for df in dfs_mc:
            dd = apply_VBFR_sel(df)
            dfcs_mc.append(dd)
        name_tag   = nt+"_VBFR"
        sel_region = sr+"_VBFR"
        plot_ROOT_stack(df_data, df_mc_signal, df_mc, dfcs_mc, 
                        mc_inputs, mc_labels, 
                        lumi_tag, args.channel, title, mc_folder[0], sel_region, name_tag)
    
    elif 'nonVBF' in sr:
        if sr2 == 'SR1':
            make_SR1_plots(df_data, df_mc_signal, df_mc, dfs_mc, nt, sr, mc_inputs, mc_labels, lumi_tag, title, mc_folder[0])
        elif sr2 == 'SR2':
            make_SR2_plots(df_data, df_mc_signal, df_mc, dfs_mc, nt, sr, mc_inputs, mc_labels, lumi_tag, title, mc_folder[0])
        elif sr2 == 'SR3':
            make_SR3_plots(df_data, df_mc_signal, df_mc, dfs_mc, nt, sr, mc_inputs, mc_labels, lumi_tag, title, mc_folder[0])
        
        
################################################################################
if __name__=="__main__":
    args = parseArgs(sys.argv[1:])

    make_stack_plots(args)
