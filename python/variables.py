variables = ["event_number",
             "event_is_bad_batman",
             "vbf_central_jet_b_tagged_DL1r_FixedCutBEff_85",
             "n_jets_30",
             "n_bjets_DL1r_FixedCutBEff_85",
             #                         "n_taus_rnn_medium",
             # "truth_passedVBFFilter",
             #                         "n_taus_medium",
             "tau_1",#electron/muon flag for tau_1 variables
             # "NOMINAL_pileup_random_run_number", #needed for selecting run period
             #electron trigger flags
             "tau_1_electron_trig_HLT_e24_lhmedium_L1EM20VH", 
             "tau_1_electron_trig_HLT_e60_lhmedium", "tau_1_electron_trig_HLT_e60_lhmedium_nod0",
             "tau_1_electron_trig_HLT_e120_lhloose", 
             "tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose",
             "tau_1_electron_trig_HLT_e140_lhloose_nod0",
             # muon trigger flags
             "tau_1_muon_trig_HLT_mu20_iloose_L1MU15", "tau_1_muon_trig_HLT_mu50",
             "tau_1_muon_trig_HLT_mu26_ivarmedium",
             "tau_1_id_medium", "tau_1", 
             # "tau_1_iso_FCHighPtCaloOnly",
             # "tau_1_iso_FCTightTrackOnly_FixedRad",
             "tau_1_iso_FCTight_FixedRad",
             "tau_1_iso_Gradient",
             "tau_1_q",
             # "tau_1_NOMINAL_MuEffSF_IsoFCTightTrackOnly_FixedRad",
             #
             # "tau_0_allTrk_n","tau_0_allTrk_pt",
             "tau_0_q",
             # "tau_0_ele_bdt_score_trans", 
             "tau_0_ele_bdt_score_trans_retuned",
             "tau_0_ele_olr_pass",
             #"tau_0_jet_bdt_tight", 
             #"tau_0_jet_bdt_medium", 
             "tau_0_jet_rnn_tight", "tau_0_jet_rnn_medium","tau_0_jet_rnn_veryloose",
             "tau_0_jet_rnn_loose","tau_0_jet_rnn_score_trans",
             # "tau_0_leadTrk_pt", "tau_0_leadTrk_eta",
             # "tau_0_allTrk_pt",
             
             #             "tau_0_matched_isTau","tau_0_matched_pdgId","tau_0_matched_q",
             "tau_0_n_charged_tracks",
             #
             "ditau_met_sum_cos_dphi","ditau_deta", "ditau_dphi","dphi_mettau",
             "ditau_met_lep1_cos_dphi",
             #             "ditau_matched_vis_mass",
             #                         "ditau_mmc_mlm_m",
             "lephad_lfv_mmc_mlm_m", "lephad_lfv_mmc_mlm_fit_status",
             "ditau_qxq",
             "coll_approx_lfv_x","coll_approx_lfv_m",
             #"ditau_coll_approx_x0",
             "ditau_mt_lep1_met","is_dijet_centrality",
             'ditau_mt_lep0_met',
             "met_more_met_phi_ele",
             #
             "n_taus","n_electrons","n_muons",
             "n_pvx",
             #                         "cross_section","kfactor","filter_efficiency",
             #scale factors
             # "tau_0_NOMINAL_TauEffSF_reco",
             # "tau_0_NOMINAL_TauEffSF_JetBDTmedium","tau_0_NOMINAL_TauEffSF_JetBDTtight",
             # "tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad",
             #"tau_0_NOMINAL_TauEffSF_MediumEleBDTPlusVeto_electron",
             #"tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron",
             # "tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron",
             # lepton variables
             "tau_1_id_medium"]
             # "tau_1_NOMINAL_EleEffSF_MULTI_L_2015_e17_lhloose_2016_2018_e17_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient",   # "tau_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolGradient",
             # "tau_1_NOMINAL_MuEffSF_Reco_QualMedium", 
             # #                         "tau_1_NOMINAL_MuEffSF_IsoGradient", # NOT PRESENT
             # "tau_1_NOMINAL_EleEffSF_offline_RecoTrk","tau_1_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v13",
             # "tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient",
             # "tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCTight",
             # "tau_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v13_Gradient",
             # "tau_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone","tau_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone",
             # "jet_NOMINAL_global_effSF_MV2c10","jet_NOMINAL_global_ineffSF_MV2c10",
             # "jet_NOMINAL_central_jets_global_effSF_JVT","jet_NOMINAL_central_jets_global_ineffSF_JVT"]
             #weights
             #"weight_mc",
             #"NOMINAL_pileup_combined_weight"]
