class Var(object):
    #    def __init__(self, var, bins, min, max, label="", cut="", weight="n_electrons==1", logy=False):
    # def __init__(self, var, bins, min, max, label="", cut="", weight="n_electrons==1&&tau_0_ele_olr_pass==1", logy=False)
    def __init__(self, var, bins, min, max, label="", cut="", weight="n_electrons==1&&tau_0_ele_bdt_score_trans > 0.15", logy=False):
    # def __init__(self, var, bins, min, max, label="", cut="", weight="n_electrons==1&&n_jets_30>=1", logy=False):
        self.var = var
        self.label = label if label else var
        
        self.bins = bins
        self.min = min
        self.max = max
        
        self.cut = cut
        self.weight = weight
        
        self.logy = logy

#-------------------------------------------------------------------------------
vars = {}

vars["tau_0_pt"] = Var("tau_0_p4.Pt()",
                       100, 0, 200,
                       label="p_{T}^{#tau} [GeV]",
                       logy=False
                   )

vars["n_jets_30"] = Var("n_jets_30",
                        100, 0, 10,
                        label="N_{\rm jets} (E>30 GeV)",
                        logy=False
                    )

vars["tau_1_pt"] = Var("tau_1_p4.Pt()",
                       100, 0, 200,
                       label="p_{T}^{lep} [GeV]",
                       logy=False
                   )

vars["met_pt"] = Var("met_p4.Pt()",
                     10, 0, 100,
                     label="E_{T}^{miss} [GeV]",
                     logy=False
                   )


vars["ditau_vis_mass"] = Var("ditau_vis_mass",
                             25, 50, 300,
                             label="m_{vis} [GeV]",
                             logy=False
                         )


vars["ditau_mt_lep0_met"] = Var("ditau_mt_lep0_met",
                                25, 0, 250,
                                label="m_{T}(#tau.MET) [GeV]",
                                logy=False
                            )
vars["ditau_mt_lep1_met"] = Var("ditau_mt_lep1_met",
                                25, 0, 250,
                                label="m_{T}(l.MET) [GeV]",
                                logy=False
                            )

# vars["coll_approx_lfv_x"] = Var("coll_approx_lfv_x",
#                                 25, 50, 300,
#                                 label="m_{coll}^{x} [GeV]",
#                                 logy=False
#                             )

# vars["coll_approx_lfv_m"] = Var("coll_approx_lfv_m",
#                                 25, 50, 300,
#                                 label="m_{coll}^{m} [GeV]",
#                                 logy=False
#                             )


# vars["lephad_lfv_mmc_mlm_m"] = Var("lephad_lfv_mmc_mlm_m",
#                                    100, 0, 200,
#                                    label="MMC m_{#tau l}^{mlm} [GeV]",
#                                    logy=False
#                                )
vars["tau_0_ele_olr_pass"] = Var("tau_0_ele_olr_pass",
                                 2, 0,2,
                                 label="tau_0_ele_olr_pass",
                                 logy=False)

# vars["tau_0_ele_bdt_score_trans"] = Var("tau_0_ele_bdt_score_trans",
#                                         50, 0, 1,
#                                         label="old eVeto BDT score",
#                                         logy=True
#                                     )
# vars["tau_0_ele_bdt_score_trans_retuned"] = Var("tau_0_ele_bdt_score_trans_retuned",
#                                                 50, 0, 1,
#                                                 label="new eVeto BDT score",
#                                                 logy=True
#                                             )

vars["tau_0_allTrk_pt"]= Var("tau_0_allTrk_pt",
                             50, 0, 250,
                             label="p_{T}^{#tau,allTrk} [GeV]",
                             logy=False)

vars["ditau_met_lep1_cos_dphi"]= Var("ditau_met_lep1_cos_dphi",
                                     25, 0, 3.45,
                                     label="#Delta#phi_{(l.MET)}",
                                     logy=False)
