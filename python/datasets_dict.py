import numpy as np
from ROOT import *
from infofile import *


z_mass=91.1876
eta_edges_rebin = np.array([-2.616, -2.068, -1.52, -1.37 , -0.822, -0.274,  0.274,  0.822,  1.37, 1.52, 2.068, 2.616])
eta_edges       = np.array([-2.516, -2.267, -2.018, -1.769, -1.52,
                            -1.37      , -1.12090909, -0.87181818, -0.62272727, -0.37363636,
                            -0.12454545,  0.12454545,  0.37363636,  0.62272727,  0.87181818,
                            1.12090909,  1.37, 1.52,
                            1.769, 2.018, 2.267, 2.516])

# 2020-08-03: TFile with the histograms of the eVeto SFs
# eVetoSF_File = TFile.Open("/eos/user/g/gipezzul/EvetoSF_decaymode_updated.root")
eVetoSF_File = TFile.Open("/home/gpezz/project/EvetoSF_decaymode_updated.root")

histEvetoSFs = {
    #decay mode == 0
    0 :  eVetoSF_File.Get('sf_eleBDTMedium_r1p0n'),
    #decay mode == 1
    1 :  eVetoSF_File.Get('sf_eleBDTMedium_r1p1n'),
    #decay mode == 2
    2 :  eVetoSF_File.Get('sf_eleBDTMedium_r1pXn')
}


#--------------------------------------------------------------------------------
# some general parameters
#--------------------------------------------------------------------------------
lumi_vec = [36207.66, 43587.3, 58450.1]

LFV_VH_etau  = [345213, 345214, 345218]
LFV_VH_mutau = [345215, 345216, 345219]

info = infofile("")

colours = [kRed, kBlue, kGreen, kCyan+1]
markers = [kOpenCircle, kOpenTriangleUp, kOpenTriangleDown, kOpenSquare]

#lxplus
#working_node ="/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4"
# yale grace
working_node ="/home/gpezz/project"

tuple_path  = "" 
mc_tuples   = ['mc16a','mc16d', 'mc16e']
data_tuples = ['data15', 'data16','data17','data18']

# # for lxplus use the following
# tuple_path = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/LFV_Htaulep_R21/V04/mc/lephad/mc16a/nom/"
#tuple_path = "/eos/user/g/gipezzul/test_v04/"

stack_order   = ['LFV_VH','LFV_ggH','LFV_VBFH','SMH','HWW','W_Jets','DiBoson','Top','Zmumu','Zee','Ztautau']

# stack_order   = ['Ztautau']
#stack_order   = ['Zee']
#stack_order   = ['VBFH']
run_margin_2015 = [284484]
run_margin_2016 = [297730, 311481]
run_margin_2017 = [324320, 341649]
run_margin_2018 = [341650]

n_cuts = 18
n_cuts_CR = 8
cut_flow_etau_VBF       = [0]*n_cuts
cut_flow_etau_VBF_nowg  = [0]*n_cuts
cut_flow_mutau_VBF      = [0]*n_cuts
cut_flow_mutau_VBF_nowg = [0]*n_cuts
cut_flow_etau_CR        = [0]*n_cuts_CR
cut_flow_mutau_CR       = [0]*n_cuts_CR
cut_flow_etau_CR_nowg   = [0]*n_cuts_CR
cut_flow_mutau_CR_nowg  = [0]*n_cuts_CR

cut_flow_etau_VBF_labels = ['Normalization','Trigger sel','!isTauFakeJet','channel','isHadronTau','n_pvx>0','n_bjets==0','lepton qual','tau qual','OS','tauID','eVeto','Zll VBF-filt','ditau_met_SCosDphi','ditau_deta','nonVBF','mvis_cut(nonVBF)','VBF']
cut_flow_CR_labels  = ['eVeto','Zll VBF-filt',
                       'ditau_met_SCosDphi','ditau_deta',
                       'relaxed tauID', 
                       '|mviss-91| < 5 GeV', 
                       'mT(lep, met) < 40 GeV','mT(tau, met) < 60 GeV']

samples = {
    'Data': {
        'data_etau_VBF'      : [],
        'data_etau_nonVBF'   : [],
        'data_mutau_VBF'     : [],
        'data_mutau_nonVBF'  : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],
        
        'cut_flow_etau_VBF'      : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg' : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'LFV_ggH': {
        'list'  : [],
        'color' : "#fa7921",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],
  
        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },
    
    'LFV_VBFH': {
        'list' : [],
        'color' : "#e55934",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],
  
        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },
    
    'LFV_VH' : {
        'list' : [],
        'color' : "#086788",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],
  
        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },
    
    'Ztautau' : {
        'list' : [],
        'color' : "#9bc53d",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],
  
        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },
    
    'Zmumu' : {
        'list' : [],
        'color' : "#fde74c",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],
  
        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'Zee' : {
        'list' : [],
        'color' : "#E5EE22",
        'data_etau_VBF'       : [],
        'data_etau_nonVBF'    : [],
        'data_mutau_VBF'      : [],
        'data_mutau_nonVBF'   : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
        
    },

    'Top' : {
        'list' : [],
        'color' : "#0EE670",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],
  
        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'DiBoson' : {
        'list' : [],
        'color' : "#33ffec",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],
  
        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'SMH' : {
        'list' : [],
        'color' : "#808080",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],
 
        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'HWW' : {
        'list' : [],
        'color' : "#0000EE",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],
  
        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'W_Jets' : {
        'list' : [],
        'color' : "#EE22E5",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],
  
        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'Zll_Jets' : {
        'list' : [],
        'color' : "#E5EE22",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_CR'        : [],
        'data_mutau_CR'       : [],
        'data_etau_CR_antiID' : [],
        'data_mutau_CR_antiID': [],
  
        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    }
}

