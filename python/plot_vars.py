class Var(object):
    def __init__(self, var, bins, min, max, label="", cut="", weight="weight_ff", logy=False, ratio=True, blind=False, blindMin=110., blindMax=140., rebin=False, rebinFactor=2, unit="", scaleFactor = 1.):
        self.var = var
        self.label = label if label else var
        
        self.bins = bins
        self.min = min
        self.max = max
        
        self.cut = cut
        self.weight = weight
        
        self.logy = logy
        self.ratio = ratio

        self.blind = blind
        self.blindMin = blindMin
        self.blindMax = blindMax
        
        self.rebin       = rebin
        self.rebinFactor = rebinFactor
        
        self.unit = unit

        self.scaleFactor = scaleFactor
#-------------------------------------------------------------------------------
vars = {}


vars["os_leptau"] = Var("os_leptau",
                        3, -0.5, 2.5,
                        label="os leptau",
                        logy=False
                    )

vars["tau_0_matched_pdgId"] = Var("tau_0_matched_pdgId",
                                  101, -50.5, 50.5,
                                  label="pdgID_{#tau}",
                                  logy=False
                              )


vars["tau_0_pt"] = Var("tau_0_pt",
                       20, 0, 100,
                       unit="GeV",
                       label="p_{T}^{#tau} [GeV]",
                       logy=False
                   )
vars["jet_0_pt"] = Var("jet_0_pt",
                       20, 0, 100,
                       unit="GeV",
                       label="p_{T}^{jet,0} [GeV]",
                       logy=False
                   )

vars["dijet_m"] = Var("dijet_m",
                             20, 0, 2000,
                             unit="GeV",
                             label="m_{jj} [GeV]",
                             logy=False
                         )

vars["dijet_absDEta"] = Var("dijet_absDEta",
                            20, 0, 10,
                            label="|#Delta#eta_{jj}|",
                            logy=False
                         )

vars["tau_0_jet_rnn_tight"] = Var("tau_0_jet_rnn_tight",
                                  2, -0.5, 1.5,
                                  label="jetRNNTight_{#tau} ",
                                  logy=False,
                                  # rebin=True
                              )
vars["tau_0_jet_rnn_medium"] = Var("tau_0_jet_rnn_medium",
                                   2, -0.5, 1.5,
                                   label="jetRNNMedium_{#tau} ",
                                   logy=False,
                                   # rebin=True
                               )

vars["tau_0_jet_rnn_loose"] = Var("tau_0_jet_rnn_loose",
                                   2, -0.5, 1.5,
                                   label="jetRNNLoose_{#tau} ",
                                   logy=False,
                                   # rebin=True
                               )

vars["tau_0_eta"] = Var("tau_0_eta",
                        24, -3, 3,
                        label="#eta^{#tau} ",
                        logy=False,
                        # rebin=True
                    )
vars["jet_0_eta"] = Var("jet_0_eta",
                        24, -3, 3,
                        label="#eta^{#jet,0} ",
                        logy=False,
                        # rebin=True
                    )
vars["tau_0_phi"] = Var("tau_0_phi",
                        21, -180, 180,
                        label="#phi^{#tau,0} ",
                        logy=False,
                        scaleFactor=57.295780
                        # rebin=True
                    )
vars["jet_0_phi"] = Var("jet_0_phi",
                        21, -180, 180,
                        label="#phi^{#jet,0} ",
                        logy=False,
                        scaleFactor=57.295780
                        # rebin=True
                    )
vars["n_jets_30"] = Var("n_jets_30",
                        11, -0.5, 10.5,
                        label="N_{\rm jets} (E>30 GeV)",
                        logy=False
                    )

vars['tau_0_n_charged_tracks'] = Var('tau_0_n_charged_tracks',
                                     5, -0.5, 4.5,
                                     label="n-prongs_{#tau}",
                                     logy=False
                                 )


vars["n_bjets_DL1r_FixedCutBEff_85"] = Var("n_bjets_DL1r_FixedCutBEff_85",
                                           11, -0.5, 10.5,
                                           label="N bjets",
                                           logy=False
                                       )

vars["tau_1_pt"] = Var("tau_1_pt",
                       20, 0, 100,
                       unit="GeV",
                       label="p_{T}^{l} [GeV]",
                       logy=False
                   )
vars["tau_1_eta"] = Var("tau_1_eta",
                        24, -3, 3,
                        label="#eta^{l} ",
                        logy=False,
                        # rebin=True
                    )
vars["tau_1_phi"] = Var("tau_1_phi",
                        21, -180, 180,
                        label="#phi^{#tau,1} ",
                        logy=False,
                        scaleFactor=57.295780
                        # rebin=True
                    )
vars["jet_1_phi"] = Var("jet_1_phi",
                        21, -180, 180,
                        label="#phi^{jet,1} ",
                        logy=False,
                        scaleFactor=57.295780
                        # rebin=True
                    )
vars["jet_1_pt"] = Var("jet_1_pt",
                       20, 0, 100,
                       unit="GeV",
                       label="p_{T}^{jet,1} [GeV]",
                       logy=False
                   )
vars["jet_1_eta"] = Var("jet_1_eta",
                        24, -3, 3,
                        label="#eta^{jet,1} ",
                        logy=False,
                        # rebin=True
                    )


vars["met_pt"] = Var("met_pt",
                     20, 0, 100,
                     unit="GeV",
                     label="E_{T}^{miss} [GeV]",
                     logy=False
                   )


vars["ditau_qxq"] = Var("ditau_qxq",
                         3, -1.5, 1.5,
                         label="q_{#tau} #times q_{l}",
                         logy=False
                     )

vars["ditau_deta"] = Var("ditau_deta",
                         21, -6.3, 6.3,
                         label="#Delta#eta = #eta_{#tau} - #eta_{l}",
                         logy=False
                     )

vars["ditau_dR"] = Var("ditau_dR",
                       30, 0, 6.,
                       label="dR(#tau, l)",
                       logy=False,
                   )
 
vars["dijet_dR"] = Var("dijet_dR",
                       30, 0, 6.,
                       label="dR(jet^{0}, jet^{1})",
                       logy=False,
                   )
vars["taujet_dR"] = Var("taujet_dR",
                        30, 0, 6.,
                        label="dR(jet^{0}, #tau^{0})",
                        logy=False,
                    )

vars["ditau_vis_mass"] = Var("ditau_vis_mass",
                             50, 50, 300,
                             unit="GeV",
                             label="m_{vis} [GeV]",
                             logy=False
                         )


vars["ditau_mt_lep0_met"] = Var("ditau_mt_lep0_met",
                                25, 0, 250,
                                unit="GeV",
                                label="m_{T}(#tau, E_{T}^{miss}) [GeV]",
                                logy=False
                            )
vars["ditau_mt_lep1_met"] = Var("ditau_mt_lep1_met",
                                25, 0, 250,
                                unit="GeV",
                                label="m_{T}(l, E_{T}^{miss}) [GeV]",
                                logy=False
                            )

vars["coll_approx_lfv_x"] = Var("coll_approx_lfv_x",
                                25, 50, 300,
                                unit="GeV",
                                label="m_{coll}^{x} [GeV]",
                                logy=False
                            )

vars["coll_approx_lfv_m"] = Var("coll_approx_lfv_m",
                                25, 50, 300,
                                unit="GeV",
                                label="m_{coll}^{m} [GeV]",
                                logy=False
                            )


vars["lephad_lfv_mmc_mlm_m"] = Var("lephad_lfv_mmc_mlm_m",
                                   40, 0, 200,
                                   unit="GeV",
                                   label="MMC m_{#tau l}^{mlm} [GeV]",
                                   logy=False
                               )
vars["tau_0_ele_olr_pass"] = Var("tau_0_ele_olr_pass",
                                 2, 0,2,
                                 label="tau_0_ele_olr_pass",
                                 logy=False)

vars["tau_0_ele_bdt_score_trans_retuned"] = Var("tau_0_ele_bdt_score_trans_retuned",
                                                50, 0, 1,
                                                label="new eVeto BDT score",
                                                logy=True
                                            )
vars["tau_0_jet_rnn_score_trans"] = Var("tau_0_jet_rnn_score_trans",
                                                50, 0, 1,
                                                label="jet RNN score",
                                                logy=True
                                            )


vars["ditau_met_sum_cos_dphi"]= Var("ditau_met_sum_cos_dphi",
                                    50, -.5, 2.5,
                                    label="ditau_met_sum_cos_dphi",
                                    logy=False)

vars["ditau_met_lep1_cos_dphi"]= Var("ditau_met_lep1_cos_dphi",
                                     25, 0, 3.45,
                                     label="#Delta#phi_{(l, E_{T}^{miss})}",
                                     logy=False)
vars["ditau_dpt"]= Var("ditau_dpt",
                       33, -82.5, 82.5,
                       label="#Delta p_{T}=p_{T}^{tau} - p_{T}^{lep}",
                       unit="GeV",
                       logy=False)

vars["dphi_mettau"] = Var("dphi_mettau",
                          25, 0, 5.,
                          label="d#phi(MET,#tau)",
                          logy=False,
                      )
