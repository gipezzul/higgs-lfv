import pandas as pd 

class infofile:
    inputs_dsids = ""

    def __init__(self, var):
        self.inputs_dsids = var

    def set_inputs_dsids(name):
        inputs_dsids = name

    def get_xsec(self, fileName):
        #extract the DSID form the file name
        newstr = ''.join((ch if ch in '0123456789' else ' ') for ch in fileName)
        listOfNumbers = [str(i) for i in newstr.split()]
        dsid = listOfNumbers[3]
        df = pd.read_csv("test/Higgs_LFV_V04_datasets_metadata.txt", sep='\t')
        counter = 0
        for s in df['DSID']:
            if str(s) == str(dsid):
                return df['XSec'][counter]
            counter = counter +1
            
        raise Exception('No cross section found for DSID: {}'.format(dsid))
    
    def get_kfactor(self, fileName):
        #extract the DSID form the file name
        newstr = ''.join((ch if ch in '0123456789' else ' ') for ch in fileName)
        listOfNumbers = [str(i) for i in newstr.split()]
        dsid = listOfNumbers[3]
        df = pd.read_csv("test/Higgs_LFV_V04_datasets_metadata.txt", sep='\t')
        counter = 0
        for s in df['DSID']:
            if str(s) == str(dsid):
                return df['K-Factor'][counter]
            counter = counter +1
                    
        raise Exception('No K-Factor found for DSID: {}'.format(dsid))

    def get_genEff(self, fileName):
        #extract the DSID form the file name
        newstr = ''.join((ch if ch in '0123456789' else ' ') for ch in fileName)
        listOfNumbers = [str(i) for i in newstr.split()]
        dsid = listOfNumbers[3]
        df = pd.read_csv("test/Higgs_LFV_V04_datasets_metadata.txt", sep='\t')
        counter = 0
        for s in df['DSID']:
            if str(s) == str(dsid):
                return df['Eff'][counter]/100.
            counter = counter +1
                        
        raise Exception('No generation efficiency found for DSID: {}'.format(dsid))
    
    def get_channel_from_DSID(self, dsid):
        df = pd.read_csv(self.inputs_dsids, sep='\t',dtype=str, na_filter = False)
        for s in df:
            for ch in df[s]:
                if ch == dsid:
                    return s
                    
        raise Exception('No channel found for DSID: {}'.format(dsid))
    
    def check_channel_from_DSID(self, dsid):
        #    df = pd.read_csv("inputs_V04.txt", sep='\t',dtype=str, na_filter = False)
        #    df = pd.read_csv("inputs_Ztautau_debug.txt", sep='\t',dtype=str, na_filter = False)
        df = pd.read_csv(self.inputs_dsids, sep='\t',dtype=str, na_filter = False)
        for s in df:
            for ch in df[s]:
                if ch == dsid:
                    return True
                    
        return False

    
    def check_metaData_presence(self, dsid):
        df = pd.read_csv("test/Higgs_LFV_V04_datasets_metadata.txt", sep='\t')
        for s in df['DSID']:
            if str(s) == str(dsid):
                return True
                
        return False
