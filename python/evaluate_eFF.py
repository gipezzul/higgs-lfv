from __future__ import division # Force python to turn integer division into a float result! Otherwise your fake rates will be all 0...
import os
import sys
import argparse
import numpy as np
import pandas as pd
from tqdm import tqdm 
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import argparse

plt.rcParams['figure.figsize'] = (10, 7)
plt.rcParams['font.size'] = 14
pd.options.mode.chained_assignment = None  # default='warn'. This eliminates warnings when assigning new variables to dataframes during the cutflow.
working_node ="/home/gpezz/project"

my_dir=os.environ['PWD']
my_python_dir=my_dir+'/python'
sys.path.append(my_python_dir)

from doPlots import *

def parseArgs(argv):
    parser = argparse.ArgumentParser(prog='./eval_yields_V05.py')
    
    parser.add_argument("--mc-data", "-mc",  default="mc16a", type=str, help="MC or Data directory.")
    parser.add_argument("--channel", "-ch",  default="etau", type=str, help="Channel.")
    parser.add_argument("--vars", default="plot_vars", type=str, help="Variables to plot (defined in an importable python module).")
    parser.add_argument("--tag",  "-t", default="V05", help="Tag.")
    parser.add_argument("--FF-version", default="all", help="Version of FF and R used.")
    parser.add_argument("--min-mass", "-minM", default=0, type=float, help="set the mVis min cut.")
    parser.add_argument("--compare", default=False, type=bool, help="Turn on debug messages.")
    parser.add_argument("--debug", default=False, type=bool, help="Turn on debug messages.")
    parser.add_argument("--force-recreation", "-f", action="store_true", help="Force recreation of cached histograms.")

    args = parser.parse_args(argv)

    return args

lum = {}
lum['data15'] = 3.2
lum['data16'] = 32.9
lum['data1516'] = 36.21
lum['data17'] = 43.59
lum['data1617'] = 77.2
lum['data151617'] = 79.8
lum['data18'] = 58.45
lum['data15161718'] = 138.25
lum['mc16a'] = 32.9
lum['mc16d'] = 44.3
lum['mc16e'] = 59.9


def compare_eFF(args):
    gROOT.SetBatch()
    gROOT.ProcessLine("{gErrorIgnoreLevel = kWarning;}")
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    data_folder = []
    if args.mc_data == 'mc16e':
        data_folder.append(working_node+"/plots_"+args.tag+"_data18/")
        lumi_tag='data18'
    elif args.mc_data == 'mc16d':
        data_folder.append(working_node+"/plots_"+args.tag+"_data17/")
        lumi_tag='data17'
    elif args.mc_data == 'mc16a':
        data_folder.append(working_node+"/plots_"+args.tag+"_data15/")
        data_folder.append(working_node+"/plots_"+args.tag+"_data16/")
        lumi_tag='data1516'

    print("[evaluate_eFF::evaluate_eFF] data folder to inspect: {}".format(data_folder))

    dfs_data = []
    dfs_antiID_data = []
    sr_ID    = 'ZeeCR'
    sr_antiID = 'ZeeCR_antiID'
 
    #now get the background MC dataframes
    mc_folder = working_node+"/plots_"+args.tag+"_{}/".format(args.mc_data)+args.channel

    if args.channel == 'etau':
        title = "H#rightarrow e#tau_{had}"
    elif args.channel == 'mutau':
        title = "H#rightarrow#mu#tau_{had}"
    if args.channel == 'etau':
        wg_mc = 'totalWeight_Ele'
    elif args.channel == 'mutau':
        wg_mc = 'totalWeight_Mu'


    output_dir  = mc_folder+"/stack_hists/eFF"
    if not os.path.exists("{}".format(output_dir)):
        os.makedirs("{}".format(output_dir))
    rootFile    = TFile("{}/plots_eFF_comparison_{}_{}.root".format(output_dir, args.channel, args.FF_version), "RECREATE")
    # rootFile.cd()
    # lum = {}
    # lum['data15'] = 3.2
    # lum['data16'] = 32.9
    # lum['data1516'] = 36.21
    # lum['data17'] = 43.59
    # lum['data1617'] = 77.2
    # lum['data151617'] = 79.8
    # lum['data18'] = 58.45
    # lum['data15161718'] = 138.25
    # lum['mc16a'] = 32.9
    # lum['mc16d'] = 44.3
    # lum['mc16e'] = 59.9
    yScale = [1.8, 1.5, 1.15]

    c  = TCanvas("c_eFF_comparison", "c_eFF_comparison"  , 800, 900)
    c.SetLeftMargin(0.15)
    c.SetBottomMargin(0.15)
    c.SetRightMargin(0.05)
    c.SetTopMargin(0.05)

    hists   = [0]*3
    colours = [kRed, kBlue, kGreen, kCyan+1, kOrange, kMagenta, kGreen+1, kRed+2, kBlue+2, kBlack, kMagenta+2]
    markers = [kOpenCircle, kOpenTriangleUp, kOpenTriangleDown, kOpenSquare, 
               kOpenCross, kOpenDiamondCross,  kFullTriangleUp, kFullTriangleDown, kFullSquare, kFullCross]
    labels = ['mc16a', 'mc16d', 'mc16e']
    fIn = [0]*3
    for i in range(len(labels)):
        dd    = working_node+"/plots_"+args.tag+"_{}/".format(labels[i])+args.channel+"/stack_hists/eFF"
        fname = "{}/plots_eFF_{}_{}.root".format(dd, args.channel, args.FF_version)
        fIn[i]   = TFile.Open(fname)
        print("file {} opened...".format(fname))
        hists[i]  = fIn[i].Get("h_eFF_ID_{}".format(args.channel))
        hists[i].SetName("h_eFF_ID_{}_{}".format(args.channel, labels[i]))
        # hists.append(h_ID)

    print("all histograms have been loaded...")

    maxy = 0

    for h in hists: 
        maxy = max(maxy, h.GetMaximum())
        print("{}".format(h.GetName()))

    for i, h in enumerate(hists): 
        h.SetMaximum(yScale[2] * maxy)
        h.SetLineColor(colours[i])
        h.SetLineWidth(2)
        h.SetMarkerStyle(markers[i])
        h.SetMarkerColor(colours[i])
        h.Draw("EO same")
    print("all histograms have been drawn...")

    atlas_label = paveText("#font[72]{ATLAS} #font[42]{Preliminary}")
    atlas_label.Draw()
    lumi_label = paveText("#font[42]{#sqrt{s}=13 TeV}", bottom=0.84, top=0.85, size=0.04)
    lumi_label.Draw()
    # tar = tag #+", "+channel #"data15, ZHhadhad"
    #        sel = var.weight
    extra_labels = [title]
    
    latex = TLatex()
    latex.SetNDC()
    latex.SetTextFont(43)
    latex.SetTextSize(24)
    latex.SetTextAlign(31)
    th = 0.04
    tx = 0.29 #90
    #ty = 1.0-0.15-0.055
    ty = 0.75 #1.0-0.05-0.055
    for l in extra_labels:
        latex.DrawLatex(tx,ty,l)
        ty-=th
        
    #legend = TLegend(0.7, 0.83, 0.935, 0.935)
    legend = TLegend(0.6, 0.75, 0.935, 0.935)
    # legend = TLegend(0.2, 0.96, 0.9, 1)
    #        legend.SetNColumns(2)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.SetTextFont(43)
    legend.SetTextSize(20)
    for i, h in enumerate(hists): 
        legend.AddEntry(h, labels[i]+"#int L dt= %2.1f fb^{-1}"%(lum[labels[i]]), "lep")
    legend.Draw()
    c.Print("{}/plot_eFF_comparison_{}_{}.png".format(output_dir, args.channel, args.FF_version))

    rootFile.cd()
    c.Write()
    for h in hists:
        h.Write()
    rootFile.Close()



def evaluate_eFF(args):
    if args.channel == 'etau':
        title = "H#rightarrow e#tau_{had}"
    elif args.channel == 'mutau':
        title = "H#rightarrow#mu#tau_{had}"
    if args.channel == 'etau':
        wg_mc = 'totalWeight_Ele'
    elif args.channel == 'mutau':
        wg_mc = 'totalWeight_Mu'
    #
    gROOT.SetBatch()
    gROOT.ProcessLine("{gErrorIgnoreLevel = kWarning;}")
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)
    print("[evaluate_eFF] min mvisCut = {}".format(args.min_mass))

    data_folder = []
    if args.mc_data == 'mc16e':
        data_folder.append(working_node+"/plots_"+args.tag+"_data18/")
        lumi_tag='data18'
    elif args.mc_data == 'mc16d':
        data_folder.append(working_node+"/plots_"+args.tag+"_data17/")
        lumi_tag='data17'
    elif args.mc_data == 'mc16a':
        data_folder.append(working_node+"/plots_"+args.tag+"_data15/")
        data_folder.append(working_node+"/plots_"+args.tag+"_data16/")
        lumi_tag='data1516'
    elif args.mc_data == 'all':
        data_folder.append(working_node+"/plots_"+args.tag+"_data15/")
        data_folder.append(working_node+"/plots_"+args.tag+"_data16/")
        data_folder.append(working_node+"/plots_"+args.tag+"_data17/")
        data_folder.append(working_node+"/plots_"+args.tag+"_data18/")
        lumi_tag='data15161718'

    print("[evaluate_eFF::evaluate_eFF] data folder to inspect: {}".format(data_folder))

    dfs_data = []
    dfs_antiID_data = []
    sr_ID     = 'ZeeCR'
    sr_antiID = 'ZeeCR_antiID'
    print("[evaluate_eFF::evaluate_eFF] data files for ID region")
    for dd in data_folder:#tqdm(data_folder, total=len(data_folder), desc="data files from {} {}".format(args.channel, sr_ID), file=sys.stdout):
        d_file = dd+args.channel+"/Data/csv/data_{}_{}".format(args.channel, sr_ID)+"_filtered.csv"
        if os.path.exists(d_file):
            ddf = pd.read_csv(d_file)
            ddf = ddf[ddf['ditau_vis_mass']>args.min_mass]
            dfs_data.append(ddf)
        else:
            print("[evaluate_eFF] file {} NOT FOUND!".format(d_file))
    df_data = pd.concat(dfs_data, axis=0, sort=False)
    df_data = df_data[(df_data['os_leptau'] == True)]

    print("[evaluate_eFF::evaluate_eFF] data files for antiID region")
    for dd in data_folder: # tqdm(data_folder, total=len(data_folder), desc="data files from {} {}".format(args.channel, sr_ID), file=sys.stdout):
        d_file = dd+args.channel+"/Data/csv/data_{}_{}".format(args.channel, sr_antiID)+"_filtered.csv"
        if os.path.exists(d_file):
            ddfs_antiID_data = pd.read_csv(d_file)
            ddfs_antiID_data = ddfs_antiID_data[ddfs_antiID_data['ditau_vis_mass']>args.min_mass]
            dfs_antiID_data.append(ddfs_antiID_data)
        else:
            print("[evaluate_eFF] file {} NOT FOUND!".format(d_file))
    df_antiID_data = pd.concat(dfs_antiID_data, axis=0, sort=False)
    df_antiID_data = df_antiID_data[(df_antiID_data['os_leptau'] == True)]

    #now get the background MC dataframes
    mc_folder = []
    if args.mc_data != 'all':
        mc_folder.append(working_node+"/plots_"+args.tag+"_{}/".format(args.mc_data)+args.channel)
    else:
        for mc in ['mc16a','mc16d','mc16e']:
            mc_folder.append(working_node+"/plots_"+args.tag+"_{}/".format(mc)+args.channel)
    
    dfs_mc        = []
    dfs_antiID_mc = []
    mc_inputs = ['DiBoson', 'HWW', 'SMH', 'Top', 'W_Jets', 'Ztautau']
    mc_labels = ['DiBoson', 'H#rightarrow WW', 'SM Higgs', 'Top', 'W+jets', 'Z#rightarrow #tau#tau']

    print("[evaluate_eFF::evaluate_eFF] MC folder to inspect: {}, {}".format(mc_folder, mc_inputs))

    print("[evaluate_eFF::evaluate_eFF] MC files for ID region")
    for mc in mc_inputs: #tqdm(mc_inputs, total=len(mc_inputs), desc="MC files for ID region from {} {}".format(args.channel, sr_ID), file=sys.stdout):
        for mc_f in mc_folder:
            mc_file = mc_f+"/{}/csv/data_{}_{}".format(mc, args.channel, sr_ID)+"_filtered.csv"      
            if os.path.exists(mc_file):
                ddfs_mc = pd.read_csv(mc_file)
                ddfs_mc = ddfs_mc[ddfs_mc['ditau_vis_mass']>args.min_mass]
                dfs_mc.append(ddfs_mc)
            else:
                print("[evaluate_eFF] file {} NOT FOUND!".format(mc_file))
                dfs_mc.append(pd.DataFrame())
    df_mc = pd.concat(dfs_mc, axis=0, sort=False)
    df_mc = df_mc[(df_mc['os_leptau'] == True)]

    print("[evaluate_eFF::evaluate_eFF] MC files for antiID region")
    for mc in mc_inputs: #tqdm(mc_inputs, total=len(mc_inputs), desc="MC files for antiID region from {} {}".format(args.channel, sr_antiID), file=sys.stdout):
        for mc_f in mc_folder:
            mc_file = mc_f+"/{}/csv/data_{}_{}".format(
                mc, args.channel, sr_antiID, args.FF_version)+"_filtered.csv"      
            if os.path.exists(mc_file):
                ddfs_antiID_mc = pd.read_csv(mc_file)
                ddfs_antiID_mc = ddfs_antiID_mc[ddfs_antiID_mc['ditau_vis_mass']>args.min_mass]
                dfs_antiID_mc.append(ddfs_antiID_mc)
            else:
                print("[evaluate_eFF] file {} NOT FOUND!".format(mc_file))
                dfs_antiID_mc.append(pd.DataFrame())
    df_antiID_mc = pd.concat(dfs_antiID_mc, axis=0, sort=False)
    df_antiID_mc = df_antiID_mc[(df_antiID_mc['os_leptau'] == True)]

    fake_folder = []
    if args.mc_data !='all':
        fake_folder.append(working_node+"/plots_"+args.tag+"_{}/".format(args.mc_data)+args.channel)
    else:
        for mc in ['mc16a','mc16d','mc16e']:
            fake_folder.append(working_node+"/plots_"+args.tag+"_{}/".format(mc)+args.channel)
        
    fake_inputs = ['Fakes']

    print("[evaluate_eFF::evaluate_eFF] Fake data folder to inspect: {}, {}".format(fake_folder, fake_inputs))
    dfs_fake        = []
    dfs_antiID_fake = []

    print("[evaluate_eFF::evaluate_eFF] Fakes files for ID and antiID regions")
    for fake in fake_inputs: #tqdm(fake_inputs, total=len(fake_inputs), desc="FAKE files from {} {}".format(args.channel, sr_ID), file=sys.stdout):
        for fake_dir in fake_folder:
            fake_file = fake_dir+"/{}/csv/data_{}_ZeeCR_fakes_antiID_{}_filtered.csv".format(fake, args.channel, args.FF_version)
            print("[evaluate_eFF::evaluate_eFF] reading Fake data file for ID region: {}".format(fake_file))
            if os.path.exists(fake_file):
                ddfs_fake = pd.read_csv(fake_file)
                ddfs_fake = ddfs_fake[ddfs_fake['ditau_vis_mass']>args.min_mass]
                dfs_fake.append(ddfs_fake)
            else:
                print("file {} NOT FOUND!".format(fake_file))
                dfs_fake.append(pd.DataFrame())
        
            fake_file = fake_dir+"/{}/csv/data_{}_{}_fakes_antiID_{}".format(
                fake, args.channel, sr_antiID, args.FF_version)+"_filtered.csv"     
            print("[evaluate_eFF::evaluate_eFF] reading Fake data file for antiID: {}".format(fake_file)) 
            if os.path.exists(fake_file):
                ddfs_antiID_fake = pd.read_csv(fake_file)
                ddfs_antiID_fake = ddfs_antiID_fake[ddfs_antiID_fake['ditau_vis_mass']>args.min_mass]
                dfs_antiID_fake.append(ddfs_antiID_fake)
            else:
                print("file {} NOT FOUND!".format(fake_file))
                dfs_antiID_fake.append(pd.DataFrame())
    df_fake        = pd.concat(dfs_fake, axis=0, sort=False)
    df_antiID_fake = pd.concat(dfs_antiID_fake, axis=0, sort=False)

    #add the fake to the MC dataframe
    print("[evaluate_eFF::evaluate_eFF] MC ID    : {}".format(sum(df_mc[wg_mc])))
    print("[evaluate_eFF::evaluate_eFF] MC antiID: {}".format(sum(df_antiID_mc[wg_mc])))
    df_mc        = pd.concat([df_mc, df_fake], axis=0, sort=False)
    df_antiID_mc = pd.concat([df_antiID_mc, df_antiID_fake], axis=0, sort=False)
    
    if len(df_fake.index)>0:
        print("[evaluate_eFF::evaluate_eFF] Fake data ID    : {}".format(sum(df_fake[wg_mc])))
    if len(df_antiID_fake.index)>0:
        print("[evaluate_eFF::evaluate_eFF] Fake data antiID: {}".format(sum(df_antiID_fake[wg_mc])))

    print("[evaluate_eFF::evaluate_eFF] data ID    : {}".format(len(df_data)))
    print("[evaluate_eFF::evaluate_eFF] data antiID: {}".format(len(df_antiID_data)))




    # output_dir  = mc_folder[0]+"/stack_hists/eFF"
    output_dir  = mc_folder[0]+"/stack_hists/eFF"
    if args.mc_data == 'all':
        output_dir = output_dir+"_all"
    if not os.path.exists("{}".format(output_dir)):
        os.makedirs("{}".format(output_dir))
    rootFile    = TFile("{}/plots_eFF_{}_{}.root".format(output_dir, args.channel, args.FF_version), "RECREATE")
    rootFile.cd()
    # lum = {}
    # lum['data15'] = 3.2
    # lum['data16'] = 32.9
    # lum['data1516'] = 36.1
    # lum['data17'] = 44.3
    # lum['data1617'] = 77.2
    # lum['data151617'] = 80.4
    # lum['data18'] = 59.9
    # lum['data15161718'] = 140.3
    yScale = [1.8, 1.5, 1.15]

    c  = TCanvas("c_eFF", "c_eFF"  , 800, 900)
    c.SetLeftMargin(0.15)
    c.SetBottomMargin(0.15)
    c.SetRightMargin(0.05)
    c.SetTopMargin(0.05)

    hists_ID          = []
    hists_antiID      = []
    hists2D_ID        = []
    hists2D_antiID    = []
    hists2D_ID_3b     = []
    hists2D_antiID_3b = []
    # edges = np.array([25., 30., 35., 40., 45., 50., 55., 60., 100.])
    edges = np.array([25., 30., 35., 40., 45., 50., 55., 60., 100.])
    edges_3b = np.array([25., 30., 35., 40., 45., 50., 55., 60, 100.]) #np.array([25., 35., 40., 45., 50., 100.])
    # eta_edges = np.array([0,  0.274,  0.822,  1.37, 1.52, 2.068, 2.616])
    eta_edges    = np.array([0, 1.37, 1.52, 2.616])
    eta_edges_3b = np.array ([0,  0.12454545, 0.37363636, 0.62272727,  0.87181818, 1.12090909, 1.37, 1.52, 1.769, 2.018, 2.267, 2.516]) 
                            #([0, 0.249, 0.498, 0.747, 0.996, 1.37, 1.52, 1.769, 2.018, 2.267, 2.516]) 
                            #([0, 0.12454545,  0.37363636,  0.62272727,  0.87181818, 1.12090909,  1.37, 1.52,
                            # 1.769, 2.018, 2.267, 2.516]) #[0, 0.274, 0.822, 1.37, 1.52, 2.068, 2.616]) #np.array([0, 0.274, 0.822, 1.37, 1.52, 2.068, 2.616]) #np.array([0, 0.274, 1.37, 1.52, 2.616]) #
    # eta_edges_3b = np.array([0, 0.62272727, 1.37, 1.52, 2.018, 2.516]) 
    colours = [kRed, kBlue, kGreen, kCyan+1, kOrange, kMagenta, kGreen+1, kRed+2, kBlue+2, kBlack, kMagenta+2]
    markers = [kOpenCircle, kOpenTriangleUp, kOpenTriangleDown, kOpenSquare, 
               kOpenCross, kOpenDiamondCross,  kFullTriangleUp, kFullTriangleDown, kFullSquare, kFullCross]
    nbins      = len(edges) - 1
    nbins_3b   = len(edges_3b) - 1
    nbins_y    = len(eta_edges) - 1
    nbins_y_3b = len(eta_edges_3b) - 1
    var_name   = 'tau_0_pt'
    var_label  = 'p_{T}^{#tau} [GeV]' 
    labels     = ['Data', 'MC']

    if len(df_data.index) > 0:
        df_data       ['tau_0_absEta'] = df_data.apply(lambda x: np.abs(x['tau_0_eta']), axis=1)
        
    if len(df_antiID_data.index) > 0:
        df_antiID_data['tau_0_absEta'] = df_antiID_data.apply(lambda x: np.abs(x['tau_0_eta']), axis=1)
    if len(df_mc.index) > 0:
        df_mc       ['tau_0_absEta'] = df_mc.apply(lambda x: np.abs(x['tau_0_eta']), axis=1)
    if len(df_antiID_mc.index) > 0:
        df_antiID_mc['tau_0_absEta'] = df_antiID_mc.apply(lambda x: np.abs(x['tau_0_eta']), axis=1)

    #for debug
    h_wg_ID     = TH1D("h_wg_ID",";weight", 600, -150., 150.)
    h_wg_antiID = TH1D("h_wg_antiID",";weight", 600, -150., 150.)
    
    #DEBUG
    # df_mc = df_mc[(df_mc['weight_mc']<100.)]
    # df_antiID_mc = df_antiID_mc[(df_antiID_mc['weight_mc']<100.)]


    for i in range(len(labels)):
        h_ID     = TH1D(labels[i]+"_ID_{}".format(var_name),";{}; eFF".format(var_label), nbins, edges)
        h_antiID = TH1D(labels[i]+"_antiID_{}".format(var_name),";{}; eFF".format(var_label), nbins, edges)
        #
        h2D_ID     = TH2D(labels[i]+"2D_ID_{}".format(var_name),";{}".format(var_label)+"; |#eta^{#tau}|", 
                          nbins_3b, edges_3b, nbins_y, eta_edges)
        h2D_antiID = TH2D(labels[i]+"2D_antiID_{}".format(var_name),";{}".format(var_label)+"; |#eta^{#tau}|", 
                          nbins_3b, edges_3b, nbins_y, eta_edges)
        #eta_edges_3bins
        h2D_ID_3b     = TH2D(labels[i]+"2D_ID_{}_3b".format(var_name),";{}".format(var_label)+"; |#eta^{#tau}|", 
                          nbins_3b, edges_3b, nbins_y_3b, eta_edges_3b)
        h2D_antiID_3b = TH2D(labels[i]+"2D_antiID_{}_3b".format(var_name),";{}".format(var_label)+"; |#eta^{#tau}|", 
                          nbins_3b, edges_3b, nbins_y_3b, eta_edges_3b)

        if i == 0:
            fillhist(h_ID, df_data[var_name], 1)
            fillhist(h_antiID, df_antiID_data[var_name], 1)
            #
            fill2Dhist(h2D_ID, df_data[var_name], df_data['tau_0_absEta'], 1)
            fill2Dhist(h2D_antiID, df_antiID_data[var_name], df_antiID_data['tau_0_absEta'], 1)
            #
            fill2Dhist(h2D_ID_3b    , df_data[var_name]       , df_data['tau_0_absEta'], 1)
            fill2Dhist(h2D_antiID_3b, df_antiID_data[var_name], df_antiID_data['tau_0_absEta'], 1)
        else:
            if len(df_mc.index) > 0:
                fillhist(h_wg_ID, df_mc['weight_mc'], 1.)
                fillhistwg(h_ID, df_mc[var_name], df_mc[wg_mc], 1)
                fill2Dhistwg(h2D_ID    , df_mc[var_name], df_mc['tau_0_absEta'], df_mc[wg_mc], 1)
                fill2Dhistwg(h2D_ID_3b , df_mc[var_name], df_mc['tau_0_absEta'], df_mc[wg_mc], 1)
            if len(df_antiID_mc.index) > 0:
                fillhist(h_wg_antiID, df_antiID_mc['weight_mc'], 1.)
                fillhistwg(h_antiID, df_antiID_mc[var_name], df_antiID_mc[wg_mc], 1)
                fill2Dhistwg(h2D_antiID, df_antiID_mc[var_name], df_antiID_mc['tau_0_absEta'],
                             df_antiID_mc[wg_mc], 1)
                fill2Dhistwg(h2D_antiID_3b, df_antiID_mc[var_name], df_antiID_mc['tau_0_absEta'],
                            df_antiID_mc[wg_mc], 1)
        #
        if args.debug:
            print("{} ID: {}".format(labels[i], h_ID))
            for j in range(h_ID.GetNbinsX()):
                print("ID    : {:2.0f} {:2.2f}".format(h_ID.GetBinCenter(j+1), h_ID.GetBinContent(j+1)))
                print("antiID: {:2.0f} {:2.2f}".format(h_antiID.GetBinCenter(j+1), h_antiID.GetBinContent(j+1)))
            print("{} antiID: {}".format(labels[i], h_antiID))
        hists_ID.append(h_ID)
        hists_antiID.append(h_antiID)
        hists2D_ID.append(h2D_ID)
        hists2D_antiID.append(h2D_antiID)
        hists2D_ID_3b.append(h2D_ID_3b)
        hists2D_antiID_3b.append(h2D_antiID_3b)
        h_ID.Write()
        h_antiID.Write()
        h2D_ID.Write()
        h2D_antiID.Write()
        h2D_ID_3b.Write()
        h2D_antiID_3b.Write()
    #DEBUG
    h_wg_ID.Write()
    h_wg_antiID.Write()
    # df_debug = df_mc[(df_mc['weight_mc']>100.)]
    # print("DSID for events with wright_mc > 100: {}".format(df_debug['DSID']))
    #
    h2D_eFF_err    = hists2D_ID[0].Clone("h2D_eFF_err_{}".format(args.channel))
    h2D_eFF_ID     = hists2D_ID[0].Clone("h2D_eFF_ID_{}".format(args.channel))
    h2D_eFF_antiID = hists2D_antiID[0].Clone("h2D_eFF_antiID_{}".format(args.channel))
    # #DEBUG
    # for i in range(hists2D_ID_3b[1].GetNbinsX()):
    #     for j in range(hists2D_ID_3b[1].GetNbinsY()):
    #         content = hists2D_ID_3b[1].GetBinContent(i+1, j+1)
    #         if content<0.:
    #             hists2D_ID_3b[1].SetBinContent(i+1, j+1, 0.)
    #         content = hists2D_antiID_3b[1].GetBinContent(i+1, j+1)
    #         if content<0.:
    #             hists2D_antiID_3b[1].SetBinContent(i+1, j+1, 0.)
    #
    h2D_eFF_ID.Add(hists2D_ID[1], -1.)
    h2D_eFF_antiID.Add(hists2D_antiID[1], -1.)
    h2D_eFF_ID.Divide(h2D_eFF_antiID)
    #
    for i in range(h2D_eFF_ID.GetNbinsX()):
        for j in range(h2D_eFF_ID.GetNbinsY()):
            err_eff = h2D_eFF_ID.GetBinError(i+1, j+1)
            eff     = h2D_eFF_ID.GetBinContent(i+1, j+1)
            if args.debug:
                print("{},{} : {:2.4f} +/- {:2.4f}".format(i, j, eff, err_eff))
            if eff > 1e-10:
                err_eff = err_eff/eff
            else:
                err_eff = 0
            h2D_eFF_err.SetBinContent(i+1, j+1, err_eff)
            h2D_eFF_err.SetBinError(i+1, j+1, 0)
    #3 bins
    h2D_eFF_err_3b    = hists2D_ID_3b[0].Clone("h2D_eFF_err_{}_3b".format(args.channel))
    h2D_eFF_ID_3b     = hists2D_ID_3b[0].Clone("h2D_eFF_ID_{}_3b".format(args.channel))
    h2D_eFF_antiID_3b = hists2D_antiID_3b[0].Clone("h2D_eFF_antiID_{}_3b".format(args.channel))
    h2D_eFF_ID_3b.Add(hists2D_ID_3b[1], -1.)
    h2D_eFF_antiID_3b.Add(hists2D_antiID_3b[1], -1.)
    h2D_eFF_ID_3b.Divide(h2D_eFF_antiID_3b)
    #
    for i in range(h2D_eFF_ID_3b.GetNbinsX()):
        for j in range(h2D_eFF_ID_3b.GetNbinsY()):
            err_eff = h2D_eFF_ID_3b.GetBinError(i+1, j+1)
            eff     = h2D_eFF_ID_3b.GetBinContent(i+1, j+1)
            if eff > 1e-10:
                err_eff = err_eff/eff
            else:
                err_eff = 0
            h2D_eFF_err_3b.SetBinContent(i+1, j+1, err_eff)
            h2D_eFF_err_3b.SetBinError(i+1, j+1, 0)
    #make the division
    if args.debug:
        for i in range(h2D_eFF_ID.GetNbinsX()):
            for j in range(h2D_eFF_ID.GetNbinsY()):
                eff     = h2D_eFF_ID.GetBinContent(i+1, j+1)
                err_eff = h2D_eFF_ID.GetBinError(i+1, j+1)
                print("[{},{}] eff = {:6.6f} +/- {:0.6f}".format(
                    i, j, eff, err_eff))

    
    h_eFF_ID     = hists_ID[0].Clone("h_eFF_ID_{}".format(args.channel))
    h_eFF_antiID = hists_antiID[0].Clone("h_eFF_antiID_{}".format(args.channel))
    h_eFF_ID.Add(hists_ID[1], -1.)
    h_eFF_antiID.Add(hists_antiID[1], -1.)
    if args.debug:
        for j in range(h_ID.GetNbinsX()):
            print("ID    : {:2.0f} {:2.2f}".format(h_eFF_ID.GetBinCenter(j+1), h_eFF_ID.GetBinContent(j+1)))
            print("antiID: {:2.0f} {:2.2f}".format(h_eFF_antiID.GetBinCenter(j+1), h_eFF_antiID.GetBinContent(j+1)))
    h_eFF_ID.Divide(h_eFF_antiID)
    maxy = max(0, h_eFF_ID.GetBinContent(h_eFF_ID.GetMaximumBin()))
    h_eFF_ID.SetMaximum(yScale[1] * maxy)

    h_eFF_ID.SetLineColor(colours[9])
    h_eFF_ID.SetLineWidth(2)
    h_eFF_ID.SetMarkerStyle(markers[9])
    h_eFF_ID.SetMarkerColor(colours[9])
    h_eFF_ID.Draw("EO")
      
    atlas_label = paveText("#font[72]{ATLAS} #font[42]{Preliminary}")
    atlas_label.Draw()
    lumi_label = paveText("#font[42]{#sqrt{s}=13 TeV, #int L dt= %2.1f fb^{-1}}"%(lum[lumi_tag]), bottom=0.84, top=0.85, size=0.04)
    lumi_label.Draw()
    # tar = tag #+", "+channel #"data15, ZHhadhad"
    #        sel = var.weight
    extra_labels = [title]
    
    latex = TLatex()
    latex.SetNDC()
    latex.SetTextFont(43)
    latex.SetTextSize(24)
    latex.SetTextAlign(31)
    th = 0.04
    tx = 0.29 #90
    #ty = 1.0-0.15-0.055
    ty = 0.75 #1.0-0.05-0.055
    for l in extra_labels:
        latex.DrawLatex(tx,ty,l)
        ty-=th
        
    #legend = TLegend(0.7, 0.83, 0.935, 0.935)
    legend = TLegend(0.6, 0.73, 0.975, 0.785)
    # legend = TLegend(0.2, 0.96, 0.9, 1)
    #        legend.SetNColumns(2)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetFillStyle(0)
    legend.SetTextFont(43)
    legend.SetTextSize(20)

    if "Tight" not in args.tag:
        legend.AddEntry(h_eFF_ID, "(medium+!tight) ZeeCR", "lep")
    else:
        legend.AddEntry(h_eFF_ID, "(tight) ZeeCR", "lep")
    legend.Draw()
    c.Print("{}/plot_eFF_{}_{}.png".format(output_dir, args.channel, args.FF_version))
    c.Write()
    h_eFF_ID.Write()
    #
    #debug
    c.SetLogy()
    h_wg_ID.SetLineColor(colours[9])
    h_wg_ID.SetLineWidth(2)
    h_wg_ID.SetMarkerStyle(markers[9])
    h_wg_ID.SetMarkerColor(colours[9])
    h_wg_ID.Draw("EO")
    c.Print("{}/plot_MC_wg_ID_{}_{}.png".format(output_dir, args.channel, args.FF_version))
    h_wg_antiID.SetLineColor(colours[9])
    h_wg_antiID.SetLineWidth(2)
    h_wg_antiID.SetMarkerStyle(markers[9])
    h_wg_antiID.SetMarkerColor(colours[9])
    h_wg_antiID.Draw("EO")
    c.Print("{}/plot_MC_wg_antiID_{}_{}.png".format(output_dir, args.channel, args.FF_version))
    c.SetLogy(0)
    #
    c.SetRightMargin(0.1)
    gStyle.SetPaintTextFormat("2.2f")
    h2D_eFF_ID.GetZaxis().SetRangeUser(0, 0.2)
    h2D_eFF_ID.Draw("colz text")
    c.Print("{}/plot_2D_eFF_{}_{}.png".format(output_dir, args.channel, args.FF_version))
    c.Write()
    h2D_eFF_ID.Write()

    h2D_eFF_err.GetZaxis().SetRangeUser(0, 0.3)
    h2D_eFF_err.Draw("colz text")
    c.Print("{}/plot_2D_eFF_err_{}_{}.png".format(output_dir, args.channel, args.FF_version))
    c.Write()
    h2D_eFF_err.Write()

    #
    h2D_eFF_ID_3b.GetZaxis().SetRangeUser(0, 0.2)
    h2D_eFF_ID_3b.Draw("colz text")
    c.Print("{}/plot_2D_eFF_{}_{}_3b.png".format(output_dir, args.channel, args.FF_version))
    c.Write()
    h2D_eFF_ID_3b.Write()

    h2D_eFF_err_3b.GetZaxis().SetRangeUser(0, 0.3)
    h2D_eFF_err_3b.Draw("colz text")
    c.Print("{}/plot_2D_eFF_err_{}_{}_3b.png".format(output_dir, args.channel, args.FF_version))
    c.Write()
    h2D_eFF_err.Write()

    rootFile.Close()

################################################################################
if __name__=="__main__":
    args = parseArgs(sys.argv[1:])

    if args.compare == True:
        compare_eFF(args)
    else:
        evaluate_eFF(args)
