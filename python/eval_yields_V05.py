import uproot
import uproot_methods.classes.TH1
import pandas as pd
import time
import math
import numpy as np
import matplotlib.pyplot as plt
import glob
from infofile import *
from variables import *

from ROOT import *
from importlib import import_module

import os, sys
import argparse
sys.path.append('$PWD/python')

from concurrent.futures import ThreadPoolExecutor

import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning) 

z_mass=91.1876
eta_edges_rebin = np.array([-2.616, -2.068, -1.52, -1.37 , -0.822, -0.274,  0.274,  0.822,  1.37, 1.52, 2.068, 2.616])
eta_edges       = np.array([-2.516, -2.267, -2.018, -1.769, -1.52,
                            -1.37      , -1.12090909, -0.87181818, -0.62272727, -0.37363636,
                            -0.12454545,  0.12454545,  0.37363636,  0.62272727,  0.87181818,
                            1.12090909,  1.37, 1.52,
                            1.769, 2.018, 2.267, 2.516])
# 2020-08-03: TFile with the histograms of the eVeto SFs
# eVetoSF_File = TFile.Open("/eos/user/g/gipezzul/EvetoSF_decaymode_updated.root")
eVetoSF_File = TFile.Open("/home/gpezz/project/EvetoSF_decaymode_updated.root")

histEvetoSFs = {
    #decay mode == 0
    0 :  eVetoSF_File.Get('sf_eleBDTMedium_r1p0n'),
    #decay mode == 1
    1 :  eVetoSF_File.Get('sf_eleBDTMedium_r1p1n'),
    #decay mode == 2
    2 :  eVetoSF_File.Get('sf_eleBDTMedium_r1pXn')
}

ZeeCR_wp = 'medium'
# ZeeCR_wp = 'tight'

def os_leptau(tau_0_q, tau_1_q):
    if tau_0_q*tau_1_q < 0.:
        return True
    else:
        return False

#used in the Zee CR_antiID selection cut-flow
min_eVetoBDT_cut=0.005

def getEVetoSF(pdg, decay_mode, pt, eta):
    if np.abs(pdg) != 11:
        return 1.;
    
    if decay_mode>=len(histEvetoSFs):
        # print("[eval_yields_V05::getEVetoSF] UNKNOWN DECAY MODE = {}".format(decay_mode))
        return 1.;
    
    hist = histEvetoSFs[decay_mode]
    binx = hist.GetXaxis().FindBin(pt)
    biny = hist.GetYaxis().FindBin(np.abs(eta))
    nBinsX = hist.GetNbinsX()
    nBinsY = hist.GetNbinsY()
    if binx < 1:
        binx = 1
    if binx > nBinsX:
        binx = nBinsX

    if biny < 1:
        biny = 1
    if biny > nBinsY:
        biny = nBinsY


    return hist.GetBinContent(binx, biny)
#--------------------------------------------------------------------------------
# INSTRUCTIONS
#--------------------------------------------------------------------------------
# To run over a specific set od DSID run: python eval_yields_V05.py -i inputs_Ztautau_debug.txt
# modifiy the file "inputs_Ztautau_debug.txt" to list oall the DSID you want
# the macro creates a set of histograms from the list of variables listed in "plot_vars.py"
#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser(prog='./eval_yields_V05.py')
    
    parser.add_argument("--input-DSIDs", "-i", dest='input_DSIDs', default="", type=str, help="Input file(s).")
    parser.add_argument("--output-dir", "-o",  default="plots", type=str, help="Output directory.")
    parser.add_argument("--mc-data", "-mc",  default="mc16a", type=str, help="MC or Data directory.")
    parser.add_argument("--single-DSID", "-s",  default=-1, type=int, help="single DSID you want to use.")
    parser.add_argument("--realData",  default=False, type=bool, help="Processing real data?")
    parser.add_argument("--vars", default="plot_vars", type=str, help="Variables to plot (defined in an importable python module).")
    parser.add_argument("--debug", default=False, type=bool, help="Turn on debug messages.")
    parser.add_argument("--force-recreation", "-f", action="store_true", help="Force recreation of cached histograms.")
    
    try:
        args = parser.parse_args(argv)
        
        checkArgs(args)
        
    except:
        parser.print_help()
        raise
    
    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)
    elif not os.path.isdir(args.output_dir):
        raise Exception("Cannot access directory '{}'".format(args.output_dir))
    
    # if not os.path.exists(args.vars+".py") or not os.path.isfile(args.vars+".py"):
    #     raise Exception("Cannot access module '{}'".format(args.vars+".py"))
    
    return

#--------------------------------------------------------------------------------
# some general parameters
#--------------------------------------------------------------------------------
lumi = 1 #36207.66 # mc16a
lumi_vec = [36207.66, 43587.3, 58450.1]
# mc16d: 44307.4
# mc16e: 58450.1

LFV_VH_etau  = [345213, 345214, 345218]
LFV_VH_mutau = [345215, 345216, 345219]

#testing the contamination infra-channels
#LFV_VH_etau  = [345213, 345214, 345218, 345215, 345216, 345219]
#LFV_VH_mutau = [345213, 345214, 345218, 345215, 345216, 345219]

gROOT.SetBatch()
gROOT.ProcessLine("{gErrorIgnoreLevel = kWarning;}")
gStyle.SetOptStat(0)

info = infofile("")


colours = [kRed, kBlue, kGreen, kCyan+1]
markers = [kOpenCircle, kOpenTriangleUp, kOpenTriangleDown, kOpenSquare]

#lxplus
#working_node ="/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4"
# yale grace
working_node ="/home/gpezz/project"
# working_node ="/ssd/scratch/gpezz"

tuple_path  = "" 
mc_tuples   = ['mc16a','mc16d', 'mc16e']
data_tuples = ['data15', 'data16','data17','data18']

# # for lxplus use the following
# tuple_path = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG4/LFV_Htaulep_R21/V04/mc/lephad/mc16a/nom/"
#tuple_path = "/eos/user/g/gipezzul/test_v04/"

stack_order   = ['LFV_VH','LFV_ggH','LFV_VBFH','SMH','HWW','W_Jets','DiBoson','Top','Zmumu','Zee','Ztautau']

# stack_order   = ['Ztautau']
#stack_order   = ['Zee']
#stack_order   = ['VBFH']
run_margin_2015 = [284484]
run_margin_2016 = [297730, 311481]
run_margin_2017 = [324320, 341649]
run_margin_2018 = [341650]

n_cuts = 18
n_cuts_ZeeCR = 8
cut_flow_etau_VBF       = [0]*n_cuts
cut_flow_etau_VBF_nowg  = [0]*n_cuts
cut_flow_mutau_VBF      = [0]*n_cuts
cut_flow_mutau_VBF_nowg = [0]*n_cuts
cut_flow_etau_ZeeCR        = [0]*n_cuts_ZeeCR
cut_flow_mutau_ZeeCR       = [0]*n_cuts_ZeeCR
cut_flow_etau_ZeeCR_nowg   = [0]*n_cuts_ZeeCR
cut_flow_mutau_ZeeCR_nowg  = [0]*n_cuts_ZeeCR

cut_flow_etau_VBF_labels = ['Normalization','Trigger sel','!isTauFakeJet','channel','isHadronTau',
                            'n_pvx>0','n_bjets==0','lepton qual','tau qual','OS','tauID','eVeto',
                            'Zll VBF-filt','ditau_met_SCosDphi','ditau_deta','nonVBF','mvis_cut(nonVBF)','VBF']
cut_flow_ZeeCR_labels  = ['eVeto','MC, not e->tau',#'Zll VBF-filt',
                       'ditau_met_SCosDphi','ditau_deta',
                       'relaxed tauID', 
                       '|mviss-91| < 5 GeV', 
                       'mT(lep, met) < 40 GeV','mT(tau, met) < 60 GeV']

samples = {
    'Data': {
        'data_etau_VBF'      : [],
        'data_etau_nonVBF'   : [],
        'data_mutau_VBF'     : [],
        'data_mutau_nonVBF'  : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'    : [],
        'data_mutau_VBF_Zee_antiID'    : [],
        'data_etau_nonVBF_Zee_antiID' : [],
        'data_mutau_nonVBF_Zee_antiID' : [],

        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'      : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg' : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },
    
    'LFV': {
        'list'  : [],
        'color' : "#fa7921",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'    : [],
        'data_mutau_VBF_Zee_antiID'   : [],
        'data_etau_nonVBF_Zee_antiID' : [],
        'data_mutau_nonVBF_Zee_antiID': [],

        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },
    
    'LFV_ggH': {
        'list'  : [],
        'color' : "#fa7921",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'    : [],
        'data_mutau_VBF_Zee_antiID'   : [],
        'data_etau_nonVBF_Zee_antiID' : [],
        'data_mutau_nonVBF_Zee_antiID': [],

        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },
    
    'LFV_VBFH': {
        'list' : [],
        'color' : "#e55934",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'    : [],
        'data_mutau_VBF_Zee_antiID'   : [],
        'data_etau_nonVBF_Zee_antiID' : [],
        'data_mutau_nonVBF_Zee_antiID': [],

        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },
    
    'LFV_VH' : {
        'list' : [],
        'color' : "#086788",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'    : [],
        'data_mutau_VBF_Zee_antiID'   : [],
        'data_etau_nonVBF_Zee_antiID' : [],
        'data_mutau_nonVBF_Zee_antiID': [],

        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },
    
    'Ztautau' : {
        'list' : [],
        'color' : "#9bc53d",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'    : [],
        'data_mutau_VBF_Zee_antiID'   : [],
        'data_etau_nonVBF_Zee_antiID' : [],
        'data_mutau_nonVBF_Zee_antiID': [],

        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },
    
    'Zmumu' : {
        'list' : [],
        'color' : "#fde74c",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'    : [],
        'data_mutau_VBF_Zee_antiID'   : [],
        'data_etau_nonVBF_Zee_antiID' : [],
        'data_mutau_nonVBF_Zee_antiID': [],
  
        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'Zee' : {
        'list' : [],
        'color' : "#E5EE22",
        'data_etau_VBF'       : [],
        'data_etau_nonVBF'    : [],
        'data_mutau_VBF'      : [],
        'data_mutau_nonVBF'   : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'     : [],
        'data_mutau_VBF_Zee_antiID'    : [],
        'data_etau_nonVBF_Zee_antiID'  : [],
        'data_mutau_nonVBF_Zee_antiID' : [],

        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
        
    },

    'Top' : {
        'list' : [],
        'color' : "#0EE670",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'     : [],
        'data_mutau_VBF_Zee_antiID'    : [],
        'data_etau_nonVBF_Zee_antiID'  : [],
        'data_mutau_nonVBF_Zee_antiID' : [],
  
        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'DiBoson' : {
        'list' : [],
        'color' : "#33ffec",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'     : [],
        'data_mutau_VBF_Zee_antiID'    : [],
        'data_etau_nonVBF_Zee_antiID'  : [],
        'data_mutau_nonVBF_Zee_antiID' : [],
  
        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'SMH' : {
        'list' : [],
        'color' : "#808080",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'     : [],
        'data_mutau_VBF_Zee_antiID'    : [],
        'data_etau_nonVBF_Zee_antiID'  : [],
        'data_mutau_nonVBF_Zee_antiID' : [],
 
        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'HWW' : {
        'list' : [],
        'color' : "#0000EE",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'     : [],
        'data_mutau_VBF_Zee_antiID'    : [],
        'data_etau_nonVBF_Zee_antiID'  : [],
        'data_mutau_nonVBF_Zee_antiID' : [],
  
        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'W_Jets' : {
        'list' : [],
        'color' : "#EE22E5",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'     : [],
        'data_mutau_VBF_Zee_antiID'    : [],
        'data_etau_nonVBF_Zee_antiID'  : [],
        'data_mutau_nonVBF_Zee_antiID' : [],
  
        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    },

    'Zll_Jets' : {
        'list' : [],
        'color' : "#E5EE22",
        'data_etau_VBF'  : [],
        'data_etau_nonVBF' : [],
        'data_mutau_VBF'  : [],
        'data_mutau_nonVBF' : [],
        'data_etau_preEVeto' : [],
        'data_etau_ZeeCR'        : [],
        'data_mutau_ZeeCR'       : [],
        'data_etau_ZeeCR_antiID' : [],
        'data_mutau_ZeeCR_antiID': [],
        'data_etau_VBF_Zee_antiID'     : [],
        'data_mutau_VBF_Zee_antiID'    : [],
        'data_etau_nonVBF_Zee_antiID'  : [],
        'data_mutau_nonVBF_Zee_antiID' : [],
  
        'data_etau_VBF_fakes_antiID'      : [],
        'data_etau_nonVBF_fakes_antiID'   : [],
        'data_mutau_VBF_fakes_antiID'     : [],
        'data_mutau_nonVBF_fakes_antiID'  : [],
        'data_etau_ZeeCR_fakes_antiID'    : [],
        'data_mutau_ZeeCR_fakes_antiID'   : [],
        'data_etau_ZeeCR_antiID_fakes_antiID' : [],
        'data_mutau_ZeeCR_antiID_fakes_antiID': [],
        'data_etau_nonVBF_fakes_antiID_Zee_antiID' : [],

        'cut_flow_etau_VBF'           : [0]*n_cuts,
        'cut_flow_etau_VBF_nowg'      : [0]*n_cuts,
        'cut_flow_mutau_VBF'     : [0]*n_cuts,
        'cut_flow_mutau_VBF_nowg': [0]*n_cuts
    }
}




def   apply_ZeeCR_cuts(sample,
                       df_etau_CR, df_mutau_CR, 
                       cf_etau_CR, cf_etau_CR_nowg, 
                       cf_mutau_CR, cf_mutau_CR_nowg,
                       channel):
    
    cutId_CR = 0
    #--------------------------------------------------------------------------------
    # select 1P events
    #--------------------------------------------------------------------------------
    if len(df_etau_CR.index) != 0:
            fail = df_etau_CR[ np.vectorize(cut_1P)(df_etau_CR.tau_0_n_charged_tracks)].index
            df_etau_CR.drop(fail, inplace=True)
    if len(df_mutau_CR.index) != 0:
            fail = df_mutau_CR[ np.vectorize(cut_1P)(df_mutau_CR.tau_0_n_charged_tracks)].index
            df_mutau_CR.drop(fail, inplace=True)
    
    #--------------------------------------------------------------------------------
    # apply the eVeto 
    #--------------------------------------------------------------------------------
    if len(df_etau_CR.index) != 0:
        df_etau_CR['ZeeCR_ID'] = df_etau_CR.apply(lambda x: cut_eBDTVeto_antiIDRegion(x['tau_0_n_charged_tracks'], x['tau_0_ele_bdt_score']), axis=1, raw=True)

    if len(df_mutau_CR.index) != 0:
        df_mutau_CR['ZeeCR_ID'] = True
    
    cf_mutau_CR[cutId_CR] += sum(df_mutau_CR.totalWeight_Mu)
    cf_mutau_CR_nowg[cutId_CR] += len(df_mutau_CR.index)
    cutId_CR += 1
    #--------------------------------------------------------------------------------
    # Zll VBF filter samples
    #--------------------------------------------------------------------------------
    if args.realData == False and channel != 'Zee':
        if len(df_etau_CR.index) != 0:
            fail = df_etau_CR[ np.vectorize(cut_pdgID)(df_etau_CR.tau_0_matched_pdgId)].index
            df_etau_CR.drop(fail, inplace=True)
        if len(df_mutau_CR.index) != 0:
            fail = df_mutau_CR[ np.vectorize(cut_pdgID)(df_mutau_CR.tau_0_matched_pdgId)].index
            df_mutau_CR.drop(fail, inplace=True)

    cf_etau_CR[cutId_CR] += sum(df_etau_CR.totalWeight_Ele)
    cf_etau_CR_nowg[cutId_CR] += len(df_etau_CR.index)
    cf_mutau_CR[cutId_CR] += sum(df_mutau_CR.totalWeight_Mu)
    cf_mutau_CR_nowg[cutId_CR] += len(df_mutau_CR.index)
    cutId_CR += 1
    #--------------------------------------------------------------------------------
    # ditau_met_sum_cos_dphi
    #--------------------------------------------------------------------------------
    if len(df_etau_CR.index) != 0:
        fail = df_etau_CR[ np.vectorize(cut_ditau_met_sum_cos_dphi)(df_etau_CR.ditau_met_sum_cos_dphi)].index
        df_etau_CR.drop(fail, inplace=True)
    cf_etau_CR[cutId_CR] += sum(df_etau_CR.totalWeight_Ele)
    cf_etau_CR_nowg[cutId_CR] += len(df_etau_CR.index)
   
    if len(df_mutau_CR.index) != 0:
        fail = df_mutau_CR[ np.vectorize(cut_ditau_met_sum_cos_dphi)(df_mutau_CR.ditau_met_sum_cos_dphi)].index
        df_mutau_CR.drop(fail, inplace=True)
    cf_mutau_CR[cutId_CR] += sum(df_mutau_CR.totalWeight_Mu)
    cf_mutau_CR_nowg[cutId_CR] += len(df_mutau_CR.index)
    cutId_CR += 1

    #--------------------------------------------------------------------------------
    # ditau_deta
    #--------------------------------------------------------------------------------
    if len(df_etau_CR.index) != 0:
        fail = df_etau_CR[ np.vectorize(cut_absDEta_lep_tau)(df_etau_CR.ditau_deta)].index
        df_etau_CR.drop(fail, inplace=True)
    cf_etau_CR[cutId_CR] += sum(df_etau_CR.totalWeight_Ele)
    cf_etau_CR_nowg[cutId_CR] += len(df_etau_CR.index)
   
    if len(df_mutau_CR.index) != 0:
        fail = df_mutau_CR[ np.vectorize(cut_absDEta_lep_tau)(df_mutau_CR.ditau_deta)].index
        df_mutau_CR.drop(fail, inplace=True)
    cf_mutau_CR[cutId_CR] += sum(df_mutau_CR.totalWeight_Mu)
    cf_mutau_CR_nowg[cutId_CR] += len(df_mutau_CR.index)
    cutId_CR += 1
   

    #--------------------------------------------------------------------------------
    # relaxed tauID
    #--------------------------------------------------------------------------------
    # if len(df_etau_CR.index) != 0:
        # fail = df_etau_CR[ np.vectorize(cut_ZeeCR_tauID)(df_etau_CR.tau_0_jet_rnn_medium, df_etau_CR.tau_0_jet_rnn_tight)].index
        # df_etau_CR.drop(fail, inplace=True)
    cf_etau_CR[cutId_CR] += sum(df_etau_CR.totalWeight_Ele)
    cf_etau_CR_nowg[cutId_CR] += len(df_etau_CR.index)
   
    # if len(df_mutau_CR.index) != 0:
        # fail = df_mutau_CR[ np.vectorize(cut_ZeeCR_tauID)(df_mutau_CR.tau_0_jet_rnn_medium, df_mutau_CR.tau_0_jet_rnn_tight)].index
        # df_mutau_CR.drop(fail, inplace=True)
    cf_mutau_CR[cutId_CR] += sum(df_mutau_CR.totalWeight_Mu)
    cf_mutau_CR_nowg[cutId_CR] += len(df_mutau_CR.index)
    cutId_CR += 1
    
    if len(df_etau_CR.index) != 0:
        fail = df_etau_CR[ np.vectorize(cut_ZeeCR_ditau_vis_mass)(df_etau_CR.ditau_vis_mass)].index
        df_etau_CR.drop(fail, inplace=True)
    cf_etau_CR[cutId_CR] += sum(df_etau_CR.totalWeight_Ele)
    cf_etau_CR_nowg[cutId_CR] += len(df_etau_CR.index)
   
    if len(df_mutau_CR.index) != 0:
        fail = df_mutau_CR[ np.vectorize(cut_ZeeCR_ditau_vis_mass)(df_mutau_CR.ditau_vis_mass)].index
        df_mutau_CR.drop(fail, inplace=True)
    cf_mutau_CR[cutId_CR] += sum(df_mutau_CR.totalWeight_Mu)
    cf_mutau_CR_nowg[cutId_CR] += len(df_mutau_CR.index)
    cutId_CR += 1
    
    if len(df_etau_CR.index) != 0:
        fail = df_etau_CR[ np.vectorize(cut_ZeeCR_ditau_mt_lep0_met)(df_etau_CR.ditau_mt_lep0_met)].index
        df_etau_CR.drop(fail, inplace=True)
    cf_etau_CR   [cutId_CR] += sum(df_etau_CR.totalWeight_Ele)
    cf_etau_CR_nowg[cutId_CR] += len(df_etau_CR.index)
   
    if len(df_mutau_CR.index) != 0:
        fail = df_mutau_CR[ np.vectorize(cut_ZeeCR_ditau_mt_lep0_met)(df_mutau_CR.ditau_mt_lep0_met)].index
        df_mutau_CR.drop(fail, inplace=True)
    cf_mutau_CR   [cutId_CR] += sum(df_mutau_CR.totalWeight_Mu)
    cf_mutau_CR_nowg[cutId_CR] += len(df_mutau_CR.index)
    cutId_CR += 1
    
    if len(df_etau_CR.index) != 0:
        fail = df_etau_CR[ np.vectorize(cut_ZeeCR_ditau_mt_lep1_met)(df_etau_CR.ditau_mt_lep1_met)].index
        df_etau_CR.drop(fail, inplace=True)
    cf_etau_CR   [cutId_CR] += sum(df_etau_CR.totalWeight_Ele)
    cf_etau_CR_nowg[cutId_CR] += len(df_etau_CR.index)
   
    if len(df_mutau_CR.index) != 0:
        fail = df_mutau_CR[ np.vectorize(cut_ZeeCR_ditau_mt_lep1_met)(df_mutau_CR.ditau_mt_lep1_met)].index
        df_mutau_CR.drop(fail, inplace=True)
    cf_mutau_CR   [cutId_CR] += sum(df_mutau_CR.totalWeight_Mu)
    cf_mutau_CR_nowg[cutId_CR] += len(df_mutau_CR.index)
    cutId_CR += 1
    
#--------------------------------------------------------------------------------
# apply VBF cuts
#
#--------------------------------------------------------------------------------
def    apply_VBF_cuts(df):

    if len(df.index) != 0:
        fail = df[ np.vectorize(cut_n_jet_30)(df.n_jets_30) ].index
        df.drop(fail, inplace=True)
        
        if len(df.index) != 0:
            fail = df[ np.vectorize(cut_jet_pt)(df.jet_1_pt) ].index
            df.drop(fail, inplace=True)
            
            if len(df.index) != 0:
                fail = df[ np.vectorize(cut_dijetMass)(df.dijet_m) ].index
                df.drop(fail, inplace=True)
                
                # if len(df.index) != 0:
                #     fail = df[ np.vectorize(cut_jets_opposite_hem)(df.jet_0_eta, df.jet_1_eta) ].index
                #     df.drop(fail, inplace=True)
                    
                if len(df.index) != 0:
                    fail = df[ np.vectorize(cut_dijet_absdeta)(df.dijet_absDEta) ].index
                    df.drop(fail, inplace=True)
                    
                    if len(df.index) != 0:
                        fail = df[ np.vectorize(cut_jet_0_pt)(df.jet_0_pt) ].index
                        df.drop(fail, inplace=True)
                    # if len(df.index) != 0:
                        #     fail = df[ np.vectorize(cut_dijet_centrality)(df.is_dijet_centrality) ].index
                        #     df.drop(fail, inplace=True)

#--------------------------------------------------------------------------------
#
#--------------------------------------------------------------------------------
def eval_eta(vec):
    eta_val = -9999.
    try:
        eta_val = vec.eta
    except ZeroDivisionError:
        eta_val = -9999.
    return eta_val

#--------------------------------------------------------------------------------

def isHiggsFile(path, dsid):
    higgs_list = ['LFV_ggH', 'LFV_VBFH', 'LFV_VH', 'SMH','HWW']
    for h in higgs_list:
        for ds in samples[h]['list']:
            if dsid == str(ds):
                print("dsid {} is a Higgs file!".format(dsid))
                return True
    return False
    
#--------------------------------------------------------------------------------
# evaluate the weights
#
#--------------------------------------------------------------------------------
def  eval_weights(ch, mc, data, path, sample, dsid, metadata):
    if ch != 'Data':
        if  isHiggsFile(path, dsid):
            data['theory_weights_nominal'] = mc.array("theory_weights_nominal") # to be used only for Higgs samples
        
            if "ggH" in path:
                data['totalWeight_Mu' ] = np.vectorize(calc_weight)(data.scaleFactor_Jet, 
                                                                    data.scaleFactor_Tau, 
                                                                    data.scaleFactor_Mu , 
                                                                    data.theory_weights_nominal, 
                                                                    data.NOMINAL_pileup_combined_weight, 
                                                                    info.get_xsec(sample), 
                                                                    info.get_kfactor(sample), 
                                                                    info.get_genEff(sample), metadata) 
                data['totalWeight_Ele'] = np.vectorize(calc_weight)(data.scaleFactor_Jet, 
                                                                    data.scaleFactor_Tau, 
                                                                    data.scaleFactor_Ele, 
                                                                    data.theory_weights_nominal, 
                                                                    data.NOMINAL_pileup_combined_weight, 
                                                                    info.get_xsec(sample), 
                                                                    info.get_kfactor(sample), 
                                                                    info.get_genEff(sample), metadata)
            else:
                data['totalWeight_Mu' ] = np.vectorize(calc_weight)(data.scaleFactor_Jet, 
                                                                    data.scaleFactor_Tau,
                                                                    data.scaleFactor_Mu , 
                                                                    data.theory_weights_nominal, 
                                                                    data.NOMINAL_pileup_combined_weight,
                                                                    info.get_xsec(sample),
                                                                    info.get_kfactor(sample), 
                                                                    info.get_genEff(sample), metadata) # this rapresents the VH case
                data['totalWeight_Ele'] = np.vectorize(calc_weight)(data.scaleFactor_Jet,
                                                                    data.scaleFactor_Tau, 
                                                                    data.scaleFactor_Ele, 
                                                                    data.theory_weights_nominal, 
                                                                    data.NOMINAL_pileup_combined_weight,
                                                                    info.get_xsec(sample), 
                                                                    info.get_kfactor(sample), 
                                                                    info.get_genEff(sample), metadata) # this rapresents the VH case
        else:
            dsid   = get_dsid_from_name_int(sample)
            
            data['totalWeight_Mu' ] = np.vectorize(calc_weight)(data.scaleFactor_Jet, 
                                                                data.scaleFactor_Tau, 
                                                                data.scaleFactor_Mu , 
                                                                data.weight_mc,
                                                                data.NOMINAL_pileup_combined_weight, 
                                                                info.get_xsec(sample), 
                                                                info.get_kfactor(sample), 
                                                                info.get_genEff(sample), metadata) 
            data['totalWeight_Ele'] = np.vectorize(calc_weight)(data.scaleFactor_Jet,
                                                                data.scaleFactor_Tau,
                                                                data.scaleFactor_Ele, 
                                                                data.weight_mc,
                                                                data.NOMINAL_pileup_combined_weight, 
                                                                info.get_xsec(sample), 
                                                                info.get_kfactor(sample), 
                                                                info.get_genEff(sample), metadata)
    
    else: #we have real data!
        data['totalWeight_Mu' ] = np.vectorize(calc_weight)(1, 1, 1 , 1, 1, 1, 1, 1, 1)
        data['totalWeight_Ele'] = np.vectorize(calc_weight)(1, 1, 1 , 1, 1, 1, 1, 1, 1)
        
#--------------------------------------------------------------------------------
# add some paramters useful to identify the various SRs, CRs and antiID regions
#
#--------------------------------------------------------------------------------
def add_ID_keys(df):
    df['ZeeCR_ID']        = True
    df['os_leptau']       = False
    df['eVeto']           = False
    df['veryloose_eVeto'] = False
    df['pass_ditauVisMassCut'] = False
#--------------------------------------------------------------------------------
# get some additonal data
#
#--------------------------------------------------------------------------------
def get_reco_dat(data, mc):
    
    # tau
    data['tau_0_pt'     ] = mc.array("tau_0_p4").pt
    data['tau_0_eta'    ] = mc.array("tau_0_p4").eta
    data['tau_0_phi'    ] = mc.array("tau_0_p4").phi
    
    # lepton
    data['tau_1_pt'     ] = mc.array("tau_1_p4").pt
    data['tau_1_eta'    ] = mc.array("tau_1_p4").eta
    data['tau_1_phi'    ] = mc.array("tau_1_p4").phi

    # ditau
    data['ditau_vis_mass'] = np.vectorize(calc_ditau_vis_mass)(data.tau_0_pt, data.tau_1_pt, data.tau_0_eta, data.tau_1_eta, data.tau_0_phi, data.tau_1_phi ) 
    data['ditau_dR'      ] = np.vectorize(calc_dR)(data.tau_0_eta, data.tau_0_phi, data.tau_1_eta, data.tau_1_phi) 
    data['ditau_dpt'     ] = mc.array("tau_1_p4").pt - mc.array("tau_0_p4").pt

    # data['ditau_deta'    ] = np.vectorize(calc_dEta)(data.tau_0_eta, data.tau_1_eta)

    # met
    data['met_pt'       ] = mc.array("met_p4").pt
    
    # jets
    data['jet_0_pt'     ] = mc.array("jet_0_p4").pt
    data['jet_1_pt'     ] = mc.array("jet_1_p4").pt
    data['jet_0_eta'    ] = mc.array("jet_0_p4").eta
    data['jet_1_eta'    ] = mc.array("jet_1_p4").eta
    data['jet_0_phi'    ] = mc.array("jet_0_p4").phi
    data['jet_1_phi'    ] = mc.array("jet_1_p4").phi
    
    #dijet
    data['dijet_m'      ] = mc.array("dijet_p4").mass
    data['dijet_absDEta'] = np.abs(mc.array("jet_0_p4").eta -  mc.array("jet_1_p4").eta)
    data['dijet_dR']      = np.vectorize(calc_dR)(data.jet_0_eta, data.jet_0_phi, data.jet_1_eta, data.jet_1_phi) 

    #ele BDT score
    data['tau_0_ele_bdt_score'] = data['tau_0_ele_bdt_score_trans_retuned']

#--------------------------------------------------------------------------------
# function for getting MC truth realted info: use it only on MC datasets!
#
#--------------------------------------------------------------------------------
def get_MC_data(data, mc):
    data['weight_mc']                        = mc.array('weight_mc')
    data['NOMINAL_pileup_combined_weight']   = mc.array('NOMINAL_pileup_combined_weight')
    data['NOMINAL_pileup_random_run_number'] = mc.array('NOMINAL_pileup_random_run_number') 

    data['tau_0_matched_isTau']              = mc.array('tau_0_matched_isTau')
    data['tau_0_matched_pdgId']              = mc.array('tau_0_matched_pdgId')
    data['tau_1_matched_mother_pdgId']       = mc.array('tau_1_matched_mother_pdgId')    
    data['tau_0_matched_q']                  = mc.array('tau_0_matched_q')
    data['tau_0_decay_mode']                 = mc.array('tau_0_decay_mode')

    data['ditau_matched_vis_mass'] = mc.array('ditau_matched_vis_mass')
    data['truth_passedVBFFilter']  = mc.array('truth_passedVBFFilter')
    # data['tau_1_NOMINAL_MuEffSF_IsoFCTightTrackOnly_FixedRad'] = mc.array('tau_1_NOMINAL_MuEffSF_IsoFCTightTrackOnly_FixedRad')
    data['tau_1_NOMINAL_MuEffSF_IsoFCTight_FixedRad'] = mc.array('tau_1_NOMINAL_MuEffSF_IsoFCTight_FixedRad')
    #
    data['tau_0_NOMINAL_TauEffSF_reco'] = mc.array('tau_0_NOMINAL_TauEffSF_reco')
    # data['tau_0_NOMINAL_TauEffSF_JetBDTmedium'] = mc.array('tau_0_NOMINAL_TauEffSF_JetBDTmedium')
    # data['tau_0_NOMINAL_TauEffSF_JetBDTtight'] = mc.array('tau_0_NOMINAL_TauEffSF_JetBDTtight')
    data['tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad']   = mc.array('tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad')
    data['tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron'] = mc.array('tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron')
    data['tau_0_NOMINAL_TauEffSF_JetRNNtight']           = mc.array('tau_0_NOMINAL_TauEffSF_JetRNNtight')
    #
    #data['tau_1_NOMINAL_EleEffSF_MULTI_L_2015_e17_lhloose_2016_2018_e17_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient'] = mc.array('tau_1_NOMINAL_EleEffSF_MULTI_L_2015_e17_lhloose_2016_2018_e17_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient')   
    data['tau_1_NOMINAL_MuEffSF_Reco_QualMedium'] = mc.array('tau_1_NOMINAL_MuEffSF_Reco_QualMedium') 
    # 
    data['tau_1_NOMINAL_EleEffSF_offline_RecoTrk'] = mc.array('tau_1_NOMINAL_EleEffSF_offline_RecoTrk')
    data['tau_1_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v13'] = mc.array('tau_1_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v13')
    data['tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient'] = mc.array('tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient')
    # data['tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCTight'] = mc.array('tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCTight')
    data['tau_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v13_Gradient'] = mc.array('tau_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v13_Gradient')
    data['tau_1_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_Gradient'] = mc.array('tau_1_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_Gradient')
    #
    # muon SF
    data['tau_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium'] = mc.array('tau_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium')
    data['tau_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium'] = mc.array('tau_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium')
    #
    # jet
    data['jet_NOMINAL_global_effSF_MV2c10'] = mc.array('jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_85') #MV2c10')
    data['jet_NOMINAL_global_ineffSF_MV2c10'] = mc.array('jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_85') #MV2c10')
    data['jet_NOMINAL_central_jets_global_effSF_JVT'] = mc.array('jet_NOMINAL_central_jets_global_effSF_JVT')
    data['jet_NOMINAL_central_jets_global_ineffSF_JVT'] = mc.array('jet_NOMINAL_central_jets_global_ineffSF_JVT')
    data['jet_NOMINAL_forward_jets_global_effSF_JVT']   = mc.array('jet_NOMINAL_forward_jets_global_effSF_JVT')
    data['jet_NOMINAL_forward_jets_global_ineffSF_JVT'] = mc.array('jet_NOMINAL_forward_jets_global_ineffSF_JVT')
    data['jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_85'] = mc.array('jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_85')
    data['jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_85']   = mc.array('jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_85')
    #
    #muon scale factor
    data['scaleFactor_Mu'] = np.vectorize(calc_SF_Mu)(data.NOMINAL_pileup_random_run_number, 
                                                      data.tau_1_NOMINAL_MuEffSF_Reco_QualMedium,
                                                      data.tau_1_NOMINAL_MuEffSF_IsoFCTight_FixedRad, 
                                                      data.tau_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium, 
                                                      data.tau_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium)
    #
    #electron scale factor
    data['scaleFactor_Ele'] = np.vectorize(calc_SF_Ele)(data.NOMINAL_pileup_random_run_number,
                                                        data.tau_1_NOMINAL_EleEffSF_offline_RecoTrk,
                                                        data.tau_1_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v13,
                                                        data.tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient, 
                                                        data.tau_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v13_Gradient,
                                                        data.tau_1_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_Gradient,
                                                        data.tau_1_matched_mother_pdgId,
                                                        data.tau_1_pt)
    #
    #tau scale factor
    #    data['scaleFactor_Tau'] = np.vectorize(calc_SF_Tau)(data.tau_0_NOMINAL_TauEffSF_reco, data.tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad, data.tau_0_NOMINAL_TauEffSF_MediumEleBDTPlusVeto_electron, data.tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron, data.n_electrons, data.n_muons, data.tau_0_n_charged_tracks)
    data['scaleFactor_Tau'] = np.vectorize(calc_SF_Tau)(data.tau_0_NOMINAL_TauEffSF_reco, 
                                                        data.tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad,
                                                        1, #data.tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron,
                                                        1., 
                                                        data.tau_0_NOMINAL_TauEffSF_JetRNNtight,
                                                        data.n_electrons, data.n_muons, data.tau_0_n_charged_tracks,
                                                        data.tau_0_matched_pdgId, data.tau_0_decay_mode, data.tau_0_pt, data.tau_0_eta)
    #
    #jet scale factor
    data['scaleFactor_Jet'] = np.vectorize(calc_SF_Jet)(data.jet_NOMINAL_central_jets_global_effSF_JVT, 
                                                        data.jet_NOMINAL_central_jets_global_ineffSF_JVT,
                                                        data.jet_NOMINAL_forward_jets_global_effSF_JVT,
                                                        data.jet_NOMINAL_forward_jets_global_ineffSF_JVT,
                                                        data.jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_85,
                                                        data.jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_85)



#--------------------------------------------------------------------------------
# 
#
#--------------------------------------------------------------------------------
def get_data_from_files(args):
    
    data       = {}
    data_mutau = {}
    mydatadir  = os.listdir(tuple_path)
    start      = time.time()

    #    info = infofile(args.input_DSIDs)
    print("%s"%info.inputs_dsids)
    #--------------------------------------------------------------------------------
    # check the presence of metadata for all the files
    #--------------------------------------------------------------------------------
    files_counter = 0
    files_failed  = 0
    print("tuple dir = {}".format((tuple_path)))
    if args.realData == False:
        for dir in mydatadir:
            # get the DSID from the name of the directory
            newstr = ''.join((ch if ch in '0123456789' else ' ') for ch in dir)
            listOfNumbers = [str(i) for i in newstr.split()]
            #        print(dir)
            #        print("listOfNumber = {}".format(listOfNumbers))
            if (len(listOfNumbers) < 4 ) or listOfNumbers == []: 
                continue
            dsid   = listOfNumbers[3]
        
            # search for the channel name corresponding to the DSID
            if (info.check_channel_from_DSID(dsid)) == False: 
                continue
        
            print("DSID "+str(dsid))

            if info.check_metaData_presence(dsid):
                files_counter = files_counter + 1
            else:
                files_failed = files_failed + 1
                print("DSID "+str(dsid)+" no metadata found")
    else:
        files_counter = len(mydatadir)

    elapsed = time.time() - start
    print("Datasets found: %5i, files failed: %5i, Time taken: %10.3f"%(files_counter, files_failed, elapsed))

    #--------------------------------------------------------------------------------
    if args.single_DSID>0:
        print("Selected only one DSID: %i"%(args.single_DSID))
    metadata_debug = 0.
    files_counter  = 0

    for dir in mydatadir:

        if args.realData == False:
            # get the DSID from the name of the directory
            newstr = ''.join((ch if ch in '0123456789' else ' ') for ch in dir)
            listOfNumbers = [str(i) for i in newstr.split()]
            if (len(listOfNumbers) < 4 ) or listOfNumbers == []: 
                continue
            dsid   = listOfNumbers[3]
        
            # search for the channel name corresponding to the DSID
            if info.check_channel_from_DSID(dsid) == False: 
                continue

            if args.single_DSID>0:
                if dsid != str(args.single_DSID):
                    continue

            ch = info.get_channel_from_DSID(dsid)
    
            samples[ch]['list'].append(dsid)
            if ch == 'Zee' or ch == 'Zmumu':
                samples['Zll_Jets']['list'].append(dsid)
            if ch == 'LFV_ggH' or ch == 'LFV_VBFH' or ch == 'LFV_VH':
                samples['LFV']['list'].append(dsid)
            
            print("Started processing DSID %s , sample %s"%(dsid, ch))
        else:
            ch = 'Data'
            print("Started processing Dataset %s"%(ch))

        tmp_path     = tuple_path + dir
        files        = os.listdir(tmp_path)
        tmp_counter  = 0       
        metadata_dsid = 1e-10

        #evaluate the metadata vale for normalizing a given DSID dataset
        if args.realData == False:
            for file in files:
                file_path = tmp_path+'/'+file
                metadata_dsid += get_metadata(file_path, dsid)
        
            print("dsid = {}, metadata = {}".format(dsid, metadata_dsid))
        #now concatenate the trees and evalute the cutflow
        for file in files:
            file_path = tmp_path+'/'+file
            print("Started reading file: %s"%file)
            dataFrameNonEmpty, temp_etau_VBF, temp_mutau_VBF, temp_etau_preEVeto, temp_etau_nonVBF, temp_mutau_nonVBF, temp_ZeeCR_etau, temp_ZeeCR_mutau = read_file(file_path,dir, metadata_dsid, ch)
            
            if not dataFrameNonEmpty:
                continue

            #etau channel
            samples[ch]['data_etau_VBF'     ].append(temp_etau_VBF)
            samples[ch]['data_etau_nonVBF'  ].append(temp_etau_nonVBF)
            samples[ch]['data_etau_preEVeto'].append(temp_etau_preEVeto)
            # if ch == 'Zee':
            samples[ch]['data_mutau_ZeeCR' ].append(temp_ZeeCR_mutau)
            samples[ch]['data_etau_ZeeCR'  ].append(temp_ZeeCR_etau)
            # samples[ch]['data_mutau_ZeeCR_antiID'].append(temp_ZeeCR_antiID_mutau)
            # samples[ch]['data_etau_ZeeCR_antiID' ].append(temp_ZeeCR_antiID_etau)
            # print("[::get_data_from_files] temp_etau_VBF.columns = {} : {}".format(len(temp_etau_VBF.columns), temp_etau_VBF.columns))

            #mutau
            samples[ch]['data_mutau_VBF'   ].append(temp_mutau_VBF)
            samples[ch]['data_mutau_nonVBF'].append(temp_mutau_nonVBF)

            if ch == 'Zee' or ch == 'Zmumu':
                samples['Zll_Jets']['data_etau_VBF'     ].append(temp_etau_VBF)
                samples['Zll_Jets']['data_etau_nonVBF'  ].append(temp_etau_nonVBF)
                samples['Zll_Jets']['data_etau_preEVeto'].append(temp_etau_preEVeto)
                samples['Zll_Jets']['data_mutau_ZeeCR'  ].append(temp_ZeeCR_mutau)
                samples['Zll_Jets']['data_etau_ZeeCR'   ].append(temp_ZeeCR_etau)
                # samples['Zll_Jets']['data_mutau_ZeeCR_antiID'].append(temp_ZeeCR_antiID_mutau)
                # samples['Zll_Jets']['data_etau_ZeeCR_antiID' ].append(temp_ZeeCR_antiID_etau)
                samples['Zll_Jets']['data_mutau_VBF'    ].append(temp_mutau_VBF)
                samples['Zll_Jets']['data_mutau_nonVBF' ].append(temp_mutau_nonVBF)

            if ch == 'LFV_ggH' or ch == 'LFV_VBFH' or ch == 'LFV_VH':
                samples['LFV']['data_etau_VBF'     ].append(temp_etau_VBF)
                samples['LFV']['data_etau_nonVBF'  ].append(temp_etau_nonVBF)
                samples['LFV']['data_etau_preEVeto'].append(temp_etau_preEVeto)
                samples['LFV']['data_mutau_ZeeCR'  ].append(temp_ZeeCR_mutau)
                samples['LFV']['data_etau_ZeeCR'   ].append(temp_ZeeCR_etau)
                # samples['LFV']['data_mutau_ZeeCR_antiID'].append(temp_ZeeCR_antiID_mutau)
                # samples['LFV']['data_etau_ZeeCR_antiID' ].append(temp_ZeeCR_antiID_etau)
                samples['LFV']['data_mutau_VBF'    ].append(temp_mutau_VBF)
                samples['LFV']['data_mutau_nonVBF' ].append(temp_mutau_nonVBF)

            #count...
            tmp_counter   = tmp_counter+1
            files_counter = files_counter+1

        print("files processed: %i"%files_counter)

        #now update the cut-flow numbers
        samples[ch]['cut_flow_etau_VBF'      ] = [cut_flow_etau_VBF[i]       + samples[ch]['cut_flow_etau_VBF'][i]       for i in range(len(cut_flow_etau_VBF))]
        samples[ch]['cut_flow_etau_VBF_nowg' ] = [cut_flow_etau_VBF_nowg[i]  + samples[ch]['cut_flow_etau_VBF_nowg'][i]  for i in range(len(cut_flow_etau_VBF_nowg))]
        samples[ch]['cut_flow_mutau_VBF'     ] = [cut_flow_mutau_VBF[i]      + samples[ch]['cut_flow_mutau_VBF'][i]      for i in range(len(cut_flow_mutau_VBF))]
        samples[ch]['cut_flow_mutau_VBF_nowg'] = [cut_flow_mutau_VBF_nowg[i] + samples[ch]['cut_flow_mutau_VBF_nowg'][i] for i in range(len(cut_flow_mutau_VBF_nowg))]

        if ch == 'Zee' or ch == 'Zmumu':
            samples['Zll_Jets']['cut_flow_etau_VBF'      ] = [cut_flow_etau_VBF[i]       + samples['Zll_Jets']['cut_flow_etau_VBF'][i]       for i in range(len(cut_flow_etau_VBF))]
            samples['Zll_Jets']['cut_flow_etau_VBF_nowg' ] = [cut_flow_etau_VBF_nowg[i]  + samples['Zll_Jets']['cut_flow_etau_VBF_nowg'][i]  for i in range(len(cut_flow_etau_VBF_nowg))]
            samples['Zll_Jets']['cut_flow_mutau_VBF'     ] = [cut_flow_mutau_VBF[i]      + samples['Zll_Jets']['cut_flow_mutau_VBF'][i]      for i in range(len(cut_flow_mutau_VBF))]
            samples['Zll_Jets']['cut_flow_mutau_VBF_nowg'] = [cut_flow_mutau_VBF_nowg[i] + samples['Zll_Jets']['cut_flow_mutau_VBF_nowg'][i] for i in range(len(cut_flow_mutau_VBF_nowg))]

        if ch == 'LFV_ggH' or ch == 'LFV_VBFH' or ch == 'LFV_VH':
            samples['LFV']['cut_flow_etau_VBF'      ] = [cut_flow_etau_VBF[i]       + samples['LFV']['cut_flow_etau_VBF'][i]       for i in range(len(cut_flow_etau_VBF))]
            samples['LFV']['cut_flow_etau_VBF_nowg' ] = [cut_flow_etau_VBF_nowg[i]  + samples['LFV']['cut_flow_etau_VBF_nowg'][i]  for i in range(len(cut_flow_etau_VBF_nowg))]
            samples['LFV']['cut_flow_mutau_VBF'     ] = [cut_flow_mutau_VBF[i]      + samples['LFV']['cut_flow_mutau_VBF'][i]      for i in range(len(cut_flow_mutau_VBF))]
            samples['LFV']['cut_flow_mutau_VBF_nowg'] = [cut_flow_mutau_VBF_nowg[i] + samples['LFV']['cut_flow_mutau_VBF_nowg'][i] for i in range(len(cut_flow_mutau_VBF_nowg))]

        if (args.debug == True): print("cut flows updated...")

        #now reset the arrays used as counter
        for i in range(n_cuts):
            cut_flow_etau_VBF      [i] = 0
            cut_flow_etau_VBF_nowg [i] = 0
            cut_flow_mutau_VBF     [i] = 0
            cut_flow_mutau_VBF_nowg[i] = 0


    for key in samples:
        if (args.debug == True): print("concat 1")
        if len(samples[key]['data_etau_VBF'     ]) > 0: 
            tmp_df = pd.concat(samples[key]['data_etau_VBF'     ], axis=0, sort=False, ignore_index=True) 
            samples[key]['data_etau_VBF'     ]         = tmp_df[(tmp_df['tau_0_jet_rnn_tight'] == True) &
                                                                (tmp_df['eVeto'] == True) &
                                                                (tmp_df['os_leptau'] == True)]
            tmp_df2 = tmp_df.copy()
            if ch != 'Data':
                if len(tmp_df2.index)>0:
                    tmp_df2['totalWeight_Ele'] = tmp_df2.apply(lambda x: reeval_wg_wo_jetRNN_SF(x['totalWeight_Ele'], 
                                                                                                x['tau_0_NOMINAL_TauEffSF_JetRNNtight']), 
                                                               axis=1, raw=True) 
            samples[key]['data_etau_VBF_fakes_antiID'] = tmp_df2[(tmp_df['tau_0_jet_rnn_tight'] == False)&
                                                                (tmp_df['tau_0_jet_rnn_veryloose'] == True)&
                                                                (tmp_df['eVeto'] == True) &
                                                                (tmp_df['os_leptau'] == True)]
            if ch != 'Data':
                #remove the eVeto SF
                if len(tmp_df.index)>0:
                    tmp_df['totalWeight_Ele'] = tmp_df.apply(lambda x: reeval_wg_wo_eVeto_SF(x['totalWeight_Ele'], x['tau_0_n_charged_tracks'],  x['tau_0_matched_pdgId'],  x['tau_0_decay_mode'],  x['tau_0_pt'],  x['tau_0_eta']), axis=1, raw=True)  
            samples[key]['data_etau_VBF_Zee_antiID'  ] = tmp_df[(tmp_df['tau_0_jet_rnn_tight'] == True) &
                                                                (tmp_df['eVeto'] == False) &
                                                                (tmp_df['veryloose_eVeto'] == True) &
                                                                (tmp_df['os_leptau'] == True) ]
            if ch != 'Data':
                if len(tmp_df.index)>0:
                    tmp_df['totalWeight_Ele'] = tmp_df.apply(lambda x: reeval_wg_wo_jetRNN_SF(x['totalWeight_Ele'], x['tau_0_NOMINAL_TauEffSF_JetRNNtight']), axis=1, raw=True)
            samples[key]['data_etau_VBF_fakes_antiID_Zee_antiID'  ] = tmp_df[(tmp_df['tau_0_jet_rnn_tight'] == False)&
                                                                             (tmp_df['tau_0_jet_rnn_veryloose'] == True)&
                                                                             (tmp_df['eVeto'] == False) &
                                                                             (tmp_df['veryloose_eVeto'] == True)]
            if args.realData == False and key != 'Zee':
                if len(samples[key]['data_etau_VBF_Zee_antiID'  ].index) != 0:
                    fail = samples[key]['data_etau_VBF_Zee_antiID'  ][ np.vectorize(cut_pdgID)(samples[key]['data_etau_VBF_Zee_antiID'  ].tau_0_matched_pdgId)].index
                    samples[key]['data_etau_VBF_Zee_antiID'  ].drop(fail, inplace=True)

        if len(samples[key]['data_etau_nonVBF'  ])>0:
            tmp_df = pd.concat( samples[key]['data_etau_nonVBF'  ], axis=0, sort=False, ignore_index=True)
            samples[key]['data_etau_nonVBF'  ]            = tmp_df[(tmp_df['tau_0_jet_rnn_tight'] == True)&
                                                                   (tmp_df['eVeto'] == True) &
                                                                   (tmp_df['os_leptau'] == True)]
            tmp_df2 = tmp_df.copy()
            if ch != 'Data':
                if len(tmp_df2.index)>0:
                    tmp_df2['totalWeight_Ele'] = tmp_df2.apply(lambda x: reeval_wg_wo_jetRNN_SF(x['totalWeight_Ele'], x['tau_0_NOMINAL_TauEffSF_JetRNNtight']), axis=1, raw=True)
            samples[key]['data_etau_nonVBF_fakes_antiID'] = tmp_df2[(tmp_df['tau_0_jet_rnn_tight'] == False)&
                                                                    (tmp_df['tau_0_jet_rnn_veryloose'] == True)&
                                                                    (tmp_df['eVeto'] == True) &
                                                                    (tmp_df['os_leptau'] == True)]
            if ch != 'Data':
                #remove the eVeto SF
                if len(tmp_df.index)>0:
                    tmp_df['totalWeight_Ele'] = tmp_df.apply(lambda x: reeval_wg_wo_eVeto_SF(x['totalWeight_Ele'], x['tau_0_n_charged_tracks'],  x['tau_0_matched_pdgId'],  x['tau_0_decay_mode'],  x['tau_0_pt'],  x['tau_0_eta']), axis=1, raw=True)

            samples[key]['data_etau_nonVBF_Zee_antiID'  ] = tmp_df[(tmp_df['tau_0_jet_rnn_tight'] == True) &
                                                                   (tmp_df['eVeto'] == False) &
                                                                   (tmp_df['veryloose_eVeto'] == True)]

            if ch != 'Data':
                if len(tmp_df.index)>0:
                    tmp_df['totalWeight_Ele'] = tmp_df.apply(lambda x: reeval_wg_wo_jetRNN_SF(x['totalWeight_Ele'], x['tau_0_NOMINAL_TauEffSF_JetRNNtight']), axis=1, raw=True)
            samples[key]['data_etau_nonVBF_fakes_antiID_Zee_antiID'  ] = tmp_df[(tmp_df['tau_0_jet_rnn_tight'] == False)&
                                                                                (tmp_df['tau_0_jet_rnn_veryloose'] == True)&
                                                                                (tmp_df['eVeto'] == False) &
                                                                                (tmp_df['veryloose_eVeto'] == True)]
            if args.realData == False and key != 'Zee':
                if len(samples[key]['data_etau_nonVBF_Zee_antiID'  ].index) != 0:
                    fail = samples[key]['data_etau_nonVBF_Zee_antiID'  ][ np.vectorize(cut_pdgID)(samples[key]['data_etau_nonVBF_Zee_antiID'  ].tau_0_matched_pdgId)].index
                    samples[key]['data_etau_nonVBF_Zee_antiID'  ].drop(fail, inplace=True)
        if (args.debug == True): print("concat 2")
        if len(samples[key]['data_etau_preEVeto'])>0:
            samples[key]['data_etau_preEVeto'] = pd.concat(samples[key]['data_etau_preEVeto'], axis=0, sort=False, ignore_index=True)
            
        if (args.debug == True): print("concat 3")
        if len(samples[key]['data_mutau_VBF'    ]) > 0: 
            tmp_df = pd.concat(samples[key]['data_mutau_VBF'    ], axis=0, sort=False, ignore_index=True)
            samples[key]['data_mutau_VBF'    ]          = tmp_df[(tmp_df['tau_0_jet_rnn_tight'] == True) &
                                                                (tmp_df['os_leptau'] == True)]
            if ch != 'Data':
                if len(tmp_df.index)>0:
                    tmp_df['totalWeight_Mu'] = tmp_df.apply(lambda x: reeval_wg_wo_jetRNN_SF(x['totalWeight_Mu'], x['tau_0_NOMINAL_TauEffSF_JetRNNtight']), axis=1, raw=True)
            samples[key]['data_mutau_VBF_fakes_antiID'] = tmp_df[(tmp_df['tau_0_jet_rnn_tight'] == False) &
                                                                (tmp_df['os_leptau'] == True)]
        #
        if len(samples[key]['data_mutau_nonVBF' ]) > 0: 
            tmp_df = pd.concat(samples[key]['data_mutau_nonVBF' ], axis=0, sort=False, ignore_index=True)
            samples[key]['data_mutau_nonVBF' ]             = tmp_df[(tmp_df['tau_0_jet_rnn_tight'] == True) &
                                                                    (tmp_df['os_leptau'] == True)]

            if ch != 'Data':
                if len(tmp_df.index)>0:
                    tmp_df['totalWeight_Mu'] = tmp_df.apply(lambda x: reeval_wg_wo_jetRNN_SF(x['totalWeight_Mu'], x['tau_0_NOMINAL_TauEffSF_JetRNNtight']), axis=1, raw=True)
            samples[key]['data_mutau_nonVBF_fakes_antiID'] = tmp_df[(tmp_df['tau_0_jet_rnn_tight'] == False)&
                                                                    (tmp_df['tau_0_jet_rnn_veryloose'] == True) &
                                                                    (tmp_df['os_leptau'] == True)]
            
        if (args.debug == True): print("concat 4")
        #        if key == 'Zee':
        if len(samples[key]['data_mutau_ZeeCR'  ]) > 0: 
            tmp_df = pd.concat(samples[key]['data_mutau_ZeeCR'  ], axis=0, sort=False, ignore_index=True)
            if ZeeCR_wp == 'medium':
                samples[key]['data_mutau_ZeeCR'         ] = tmp_df[(tmp_df['ZeeCR_ID'] == True ) & 
                                                                   (tmp_df['tau_0_jet_rnn_medium'] == True)  & 
                                                                   (tmp_df['tau_0_jet_rnn_tight'] == False)]

                #ZeeCR_antiID Zee
                samples[key]['data_mutau_ZeeCR_antiID'  ] = tmp_df[(tmp_df['ZeeCR_ID'] == False)& 
                                                                   (tmp_df['tau_0_jet_rnn_medium'] == True) &
                                                                   (tmp_df['tau_0_jet_rnn_tight'] == False) &
                                                                   (tmp_df['tau_0_ele_bdt_score_trans_retuned'] >  min_eVetoBDT_cut) ]
                #Zee_antiID for the fakes
                samples[key]['data_mutau_ZeeCR_fakes_antiID'  ] = tmp_df[(tmp_df['ZeeCR_ID'] == True) &
                                                                         (tmp_df['tau_0_jet_rnn_medium'] == False)&
                                                                         (tmp_df['tau_0_jet_rnn_veryloose'] == True) ]
            
                samples[key]['data_mutau_ZeeCR_antiID_fakes_antiID'  ] = tmp_df[(tmp_df['ZeeCR_ID'] == False) & 
                                                                                (tmp_df['tau_0_jet_rnn_medium'] == False) &
                                                                                (tmp_df['tau_0_jet_rnn_veryloose'] == True) &
                                                                                (tmp_df['tau_0_ele_bdt_score_trans_retuned'] >  min_eVetoBDT_cut) ]
            elif ZeeCR_wp == 'tight':
                samples[key]['data_mutau_ZeeCR'         ] = tmp_df[(tmp_df['ZeeCR_ID'] == True ) & 
                                                                   (tmp_df['tau_0_jet_rnn_tight'] == True)]#   
                #ZeeCR_antiID Zee
                samples[key]['data_mutau_ZeeCR_antiID'  ] = tmp_df[(tmp_df['ZeeCR_ID'] == False)& 
                                                                   (tmp_df['tau_0_jet_rnn_tight'] == True) &
                                                                   # (tmp_df['tau_0_jet_rnn_medium'] == True) &
                                                                   # (tmp_df['tau_0_jet_rnn_tight'] == False) &
                                                                   (tmp_df['tau_0_ele_bdt_score_trans_retuned'] >  min_eVetoBDT_cut) ]
                # samples[key]['data_mutau_ZeeCR_antiID'  ] = samples[key]['data_mutau_ZeeCR_antiID'][(samples[key]['data_mutau_ZeeCR_antiID']['tau_0_ele_bdt_score_trans_retuned']> min_eVetoBDT_cut)]
                #Zee_antiID for the fakes
                samples[key]['data_mutau_ZeeCR_fakes_antiID'  ] = tmp_df[(tmp_df['ZeeCR_ID'] == True) &
                                                                         # (tmp_df['tau_0_jet_rnn_medium'] == False)&
                                                                         (tmp_df['tau_0_jet_rnn_tight'] == False) &
                                                                         (tmp_df['tau_0_jet_rnn_veryloose'] == True) ]
            
                samples[key]['data_mutau_ZeeCR_antiID_fakes_antiID'  ] = tmp_df[(tmp_df['ZeeCR_ID'] == False) & 
                                                                                # (tmp_df['tau_0_jet_rnn_medium'] == False) &
                                                                                (tmp_df['tau_0_jet_rnn_tight'] == False) &
                                                                                (tmp_df['tau_0_jet_rnn_veryloose'] == True) &
                                                                                (tmp_df['tau_0_ele_bdt_score_trans_retuned'] >  min_eVetoBDT_cut) ]
        #
        if len(samples[key]['data_etau_ZeeCR'   ]) > 0: 
            tmp_df = pd.concat(samples[key]['data_etau_ZeeCR'  ], axis=0, sort=False, ignore_index=True)                     
            if ZeeCR_wp == 'medium':
                if ch != 'Data':
                    if len(tmp_df.index)>0:
                        tmp_df['totalWeight_Ele'] = tmp_df.apply(lambda x: reeval_wg_wo_jetRNN_SF(x['totalWeight_Ele'], x['tau_0_NOMINAL_TauEffSF_JetRNNtight']), axis=1, raw=True)

                samples[key]['data_etau_ZeeCR'         ] = tmp_df[(tmp_df['ZeeCR_ID'] == True ) & 
                                                                  (tmp_df['tau_0_jet_rnn_medium'] == True) & 
                                                                  (tmp_df['tau_0_jet_rnn_tight'] == False)
                                                              ]
                #antiID for the fakes
                samples[key]['data_etau_ZeeCR_fakes_antiID'  ] = tmp_df[(tmp_df['ZeeCR_ID'] == True) & 
                                                                        (tmp_df['tau_0_jet_rnn_medium'] == False) &
                                                                        (tmp_df['tau_0_jet_rnn_veryloose'] == True) ]
                #ZeeCR_antiID Zee
                if ch != 'Data':
                    #remove the eVeto SF
                    if len(tmp_df.index)>0:
                        tmp_df['totalWeight_Ele'] = tmp_df.apply(lambda x: reeval_wg_wo_eVeto_SF(x['totalWeight_Ele'], x['tau_0_n_charged_tracks'],  x['tau_0_matched_pdgId'],  x['tau_0_decay_mode'],  x['tau_0_pt'],  x['tau_0_eta']), axis=1, raw=True)
                    
                samples[key]['data_etau_ZeeCR_antiID'  ] = tmp_df[(tmp_df['ZeeCR_ID'] == False) & 
                                                                  (tmp_df['tau_0_jet_rnn_medium'] == True ) &
                                                                  (tmp_df['tau_0_jet_rnn_tight' ] == False) &
                                                                  (tmp_df['tau_0_ele_bdt_score_trans_retuned'] >  min_eVetoBDT_cut) ]
                #
                samples[key]['data_etau_ZeeCR_antiID_fakes_antiID'  ] = tmp_df[(tmp_df['ZeeCR_ID'] == False) & 
                                                                               (tmp_df['tau_0_jet_rnn_medium'] == False) &
                                                                               (tmp_df['tau_0_jet_rnn_veryloose'] == True) &
                                                                               (tmp_df['tau_0_ele_bdt_score_trans_retuned'] >  min_eVetoBDT_cut)]
            elif ZeeCR_wp == 'tight':
                samples[key]['data_etau_ZeeCR'         ] = tmp_df[(tmp_df['ZeeCR_ID'] == True ) & 
                                                                  (tmp_df['tau_0_jet_rnn_tight'] == True) ]
                #ZeeCR_antiID Zee
                tmp_df2 = tmp_df.copy()
                if ch != 'Data':
                    #remove the eVeto SF
                    if len(tmp_df2.index)>0:
                        tmp_df2['totalWeight_Ele'] = tmp_df2.apply(lambda x: reeval_wg_wo_eVeto_SF(x['totalWeight_Ele'], x['tau_0_n_charged_tracks'],  x['tau_0_matched_pdgId'],  x['tau_0_decay_mode'],  x['tau_0_pt'],  x['tau_0_eta']), axis=1, raw=True)
                
                samples[key]['data_etau_ZeeCR_antiID'  ] = tmp_df2[(tmp_df['ZeeCR_ID'] == False) & 
                                                                   (tmp_df['tau_0_jet_rnn_tight'] == True ) &
                                                                   (tmp_df['tau_0_ele_bdt_score_trans_retuned'] >  min_eVetoBDT_cut) ]

                #antiID for the fakes
                if ch != 'Data':
                    if len(tmp_df.index)>0:
                        tmp_df['totalWeight_Ele'] = tmp_df.apply(lambda x: reeval_wg_wo_jetRNN_SF(x['totalWeight_Ele'], x['tau_0_NOMINAL_TauEffSF_JetRNNtight']), axis=1, raw=True)
                samples[key]['data_etau_ZeeCR_fakes_antiID'  ] = tmp_df[(tmp_df['ZeeCR_ID'] == True) & 
                                                                        (tmp_df['tau_0_jet_rnn_tight'] == False) &
                                                                        (tmp_df['tau_0_jet_rnn_veryloose'] == True) ]
                if ch != 'Data':
                    #remove the eVeto SF
                    if len(tmp_df.index)>0:
                        tmp_df['totalWeight_Ele'] = tmp_df.apply(lambda x: reeval_wg_wo_eVeto_SF(x['totalWeight_Ele'], x['tau_0_n_charged_tracks'],  x['tau_0_matched_pdgId'],  x['tau_0_decay_mode'],  x['tau_0_pt'],  x['tau_0_eta']), axis=1, raw=True)
                
                samples[key]['data_etau_ZeeCR_antiID_fakes_antiID'  ] = tmp_df[(tmp_df['ZeeCR_ID'] == False) & 
                                                                               (tmp_df['tau_0_jet_rnn_tight'] == False) &
                                                                               (tmp_df['tau_0_jet_rnn_veryloose'] == True) &
                                                                               (tmp_df['tau_0_ele_bdt_score_trans_retuned'] >  min_eVetoBDT_cut)]

        
            
 
    elapsed = time.time() - start
    print("Files found: "+str(files_counter)+" , Time taken: %10.3f"%elapsed)

    # print("Dataset ")
    # for s in samples:
    #     nFiles = len(samples[s]['list'])
    #     print(str(s)+"   "+str(nFiles))

    print("Apply cuts...")

    for key in samples:
        if samples[key]['cut_flow_etau_VBF_nowg'][0] != 0 or samples[key]['cut_flow_mutau_VBF_nowg'][0] != 0:
            printCutFlow(key)
  
    # print("       Cut step           etau-yield        etau-events           mutau-yield        mutau-events")
    # for i in range(len(cut_flow_etau_VBF)):
    #     print("%22s  %12.3f %16d  %22.3f %16d"% (cut_flow_etau_VBF_labels[i], cut_flow_etau_VBF[i], cut_flow_etau_VBF_nowg[i], cut_flow_mutau_VBF[i], cut_flow_mutau_VBF_nowg[i]) )
    # print("//--------------------------------------------------------------------------------//")
    # print("//                              CONTROL REGION: Zee                               //")
    # print("//--------------------------------------------------------------------------------//")
    # for i in range(len(cut_flow_etau_ZeeCR)):
    #     print("%22s  %12.3f %16d  %22.3f %16d"% (cut_flow_ZeeCR_labels[i], cut_flow_etau_ZeeCR[i], cut_flow_etau_ZeeCR_nowg[i], cut_flow_mutau_ZeeCR[i], cut_flow_mutau_ZeeCR_nowg[i]) )
    

    #    return data, data_mutau

################################################################################
def printCutFlow(ch):
    print("//--------------------------------------------------------------------------------//")
    print("//                                  Channel: %s                                   //"%ch)
    print("//--------------------------------------------------------------------------------//")
    print("       Cut step           etau-yield        etau-events           mutau-yield        mutau-events")
    
    flow_etau_VBF            = samples[ch]['cut_flow_etau_VBF']
    flow_etau_VBF_nowg       = samples[ch]['cut_flow_etau_VBF_nowg']
    flow_mutau_VBF      = samples[ch]['cut_flow_mutau_VBF']
    flow_mutau_VBF_nowg = samples[ch]['cut_flow_mutau_VBF_nowg']
    
    for i in range(len(cut_flow_etau_VBF)):
        print("%22s  %8.6f %16d  %19.6f %16d"% (cut_flow_etau_VBF_labels[i], flow_etau_VBF[i], flow_etau_VBF_nowg[i], flow_mutau_VBF[i], flow_mutau_VBF_nowg[i]) )
   

def cut_run_number(NOMINAL_pileup_random_run_number):
    return NOMINAL_pileup_random_run_number <= 0

def cut_Sherpa_weight_mc(weight_mc):
    return np.abs(weight_mc) > 100

def cut_bad_batman(event_is_bad_batman):
    return event_is_bad_batman !=0
################################################################################
# baseline cuts
################################################################################
def cut_n_taus(n_taus):
    return n_taus != 1

def cut_etau_channel(n_electrons,n_muons, ch, fileName, dsid):
    val = (n_electrons == 1)

    #check in case of signal file, which channel is actually simulated
    if ('LFV_ggH' in ch) or ('LFV_VBFH' in ch):
        if 'etau' in fileName:
            return not(val)
        else:
            return True
    elif ('LFV_VH' in ch):
        if int(dsid) in LFV_VH_etau:
            return not(val)
        else:
            return True

    return not(val)

def cut_mutau_channel(n_electrons,n_muons, ch, fileName, dsid):
    val = (n_muons ==1)

    #check in case of signal file, which channel is actually simulated
    if ('LFV_ggH' in ch) or ('LFV_VBFH' in ch):
        if 'mutau' in fileName:
            return not(val)
        else:
            return True
    elif ('LFV_VH' in ch):
        if int(dsid) in LFV_VH_mutau:
            return not(val)
        else:
            return True

    return not(val)

def cut_trigger(tau_1_pt,
                NOMINAL_pileup_random_run_number, 
                tau_1_electron_trig_HLT_e24_lhmedium_L1EM20VH, tau_1_electron_trig_HLT_e60_lhmedium, tau_1_electron_trig_HLT_e60_lhmedium_nod0,
                tau_1_electron_trig_HLT_e120_lhloose, tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose, tau_1_electron_trig_HLT_e140_lhloose_nod0,
                tau_1_muon_trig_HLT_mu20_iloose_L1MU15, tau_1_muon_trig_HLT_mu50,
                tau_1_muon_trig_HLT_mu26_ivarmedium ):
    # 2015 
    if NOMINAL_pileup_random_run_number <= 284484:
        condition_etau  = (tau_1_electron_trig_HLT_e24_lhmedium_L1EM20VH or tau_1_electron_trig_HLT_e60_lhmedium or tau_1_electron_trig_HLT_e120_lhloose) and tau_1_pt > 25
        condition_mutau = (tau_1_muon_trig_HLT_mu20_iloose_L1MU15 or tau_1_muon_trig_HLT_mu50) and tau_1_pt > 21
        condition = condition_etau or condition_mutau
        return not(condition)

    # 2016
    if (NOMINAL_pileup_random_run_number >= 297730 and NOMINAL_pileup_random_run_number <= 311481) or (NOMINAL_pileup_random_run_number >= 324320):
        condition_etau   = (tau_1_electron_trig_HLT_e60_lhmedium_nod0 or tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose or tau_1_electron_trig_HLT_e140_lhloose_nod0) and tau_1_pt > 27
        condition_mutau  = (tau_1_muon_trig_HLT_mu26_ivarmedium or tau_1_muon_trig_HLT_mu50) and tau_1_pt > 27.3
        condition = condition_etau or condition_mutau
        return not(condition)

    return True



def cut_trigger_etau(tau_1_pt,
                     NOMINAL_pileup_random_run_number, 
                     tau_1_electron_trig_HLT_e24_lhmedium_L1EM20VH, 
                     tau_1_electron_trig_HLT_e60_lhmedium, 
                     tau_1_electron_trig_HLT_e60_lhmedium_nod0,
                     tau_1_electron_trig_HLT_e120_lhloose,
                     tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose,
                     tau_1_electron_trig_HLT_e140_lhloose_nod0
                 ):
    if NOMINAL_pileup_random_run_number < 284484 and tau_1_pt > 25:
        return not(tau_1_electron_trig_HLT_e24_lhmedium_L1EM20VH or tau_1_electron_trig_HLT_e60_lhmedium or tau_1_electron_trig_HLT_e120_lhloose)

    if (NOMINAL_pileup_random_run_number > 297730 and NOMINAL_pileup_random_run_number < 311481) or (NOMINAL_pileup_random_run_number > 324320):
        if tau_1_pt > 27:
            return not(tau_1_electron_trig_HLT_e60_lhmedium_nod0 or tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose or tau_1_electron_trig_HLT_e140_lhloose_nod0)
    return True

def cut_trigger_mutau(tau_1_pt,
                      NOMINAL_pileup_random_run_number, 
                      tau_1_muon_trig_HLT_mu20_iloose_L1MU15, tau_1_muon_trig_HLT_mu50,
                      tau_1_muon_trig_HLT_mu26_ivarmedium):
    if NOMINAL_pileup_random_run_number < 284484 and tau_1_pt > 21:
        return not( tau_1_muon_trig_HLT_mu20_iloose_L1MU15 or tau_1_muon_trig_HLT_mu50 )

    if (NOMINAL_pileup_random_run_number > 297730 and NOMINAL_pileup_random_run_number < 311481) or (NOMINAL_pileup_random_run_number > 324320):
        if tau_1_pt > 27.3:
            return not(tau_1_muon_trig_HLT_mu26_ivarmedium or tau_1_muon_trig_HLT_mu50)
    return True

def isSignal(dsid): 
    higgs_list = ['LFV_ggH', 'LFV_VBFH', 'LFV_VH']
    for h in higgs_list:
        for ds in samples[h]['list']:
            if dsid == str(ds):
                print("dsid {} is a MC signal file!".format(dsid))
                return True    
    return False
    
def isVBFFilter(fileName):
    args = parseArgs(sys.argv[1:])
    #    info = infofile(args.input_DSIDs)

    newstr = ''.join((ch if ch in '0123456789' else ' ') for ch in fileName)
    listOfNumbers = [str(i) for i in newstr.split()]
    dsid   = listOfNumbers[3]
    ch     = info.get_channel_from_DSID(dsid)
    conditionVBF = (str(ch) == 'VBFH') and 'filt' in fileName
    if ( (str(ch) == 'Ztautau') | (str(ch) == 'Zee') | conditionVBF | (str(ch) == 'Zmumu')):
        return True
    
    return False
    
def cut_pdgID(tau_0_pdgID):
    condition = np.abs(tau_0_pdgID) != 11
    return not(condition)

def cut_VBFFilter(truth_passedVBFFilter):
    return truth_passedVBFFilter==1 
     

def cut_hadronic_tau(tau_0_matched_pdgId):
     return not (np.abs(tau_0_matched_pdgId)==15)


def cut_npvx(n_pvx):
    return n_pvx ==0
    
def cut_lepton_quality_mutau(n_muons,
                             tau_1_pt, 
                             #      tau_1_id_medium,
                             tau_1_eta,
                             tau_1_iso_FCTight_FixedRad,
                             tau_1_iso_Gradient):
    condition = (tau_1_pt > 27.3)# and (tau_1_id_medium == 1) 
    
    # mutau
    condition = condition and (np.abs(tau_1_eta)< 2.5) #and ( np.abs(tau_1_eta)<=1.37 or abs(tau_1_eta) >= 1.52) # remove the crack region
    #    condition = condition and tau_1_iso_FCTightTrackOnly_FixedRad == 1
    
    #    if (n_muons == 1):
    condition = condition and (tau_1_iso_FCTight_FixedRad == 1)
    #condition =  tau_1_iso_FCTight_FixedRad == 1
    return not(condition)

def cut_lepton_medium_id(tau_1_id_medium):
    return tau_1_id_medium != 1

def cut_lepton_quality_etau(tau_1_pt, 
                            #                            tau_1_id_medium,
                            tau_1_eta,
                            tau_1_iso_Gradient):
    condition = (tau_1_pt > 27.3) #and (tau_1_id_medium == 1) 
    
    #etau
    condition = condition and np.abs(tau_1_eta)< 2.47 and ( np.abs(tau_1_eta)<=1.37 or abs(tau_1_eta) >= 1.52) # remove the crack region
    #condition = condition and tau_1_iso_FCHighPtCaloOnly == 1
    condition = condition and tau_1_iso_Gradient == 1

    return not(condition)


def cut_OS_leptau(tau_0_q, tau_1_q):
    return not(tau_0_q*tau_1_q < 0)


def cut_tau_quality(tau_0_pt, tau_0_q, tau_0_n_charged_tracks, tau_0_eta):
    condition = (tau_0_pt > 25) and (np.abs(tau_0_q) == 1) and (tau_0_n_charged_tracks == 1 or tau_0_n_charged_tracks ==3) 
    condition = condition and np.abs(tau_0_eta) < 2.4 and ( np.abs(tau_0_eta)<=1.37 or np.abs(tau_0_eta) >= 1.52) # remove the crack region
    return not(condition)

def cut_jet_pt(jet_0_pt):
    return jet_0_pt < 40.0


def pass_eBDTVeto(tau_0_n_charged_tracks, tau_0_ele_bdt_score):
    if tau_0_n_charged_tracks == 1:
        return tau_0_ele_bdt_score > 0.15
    return True

def veryloose_eBDTVeto(tau_0_n_charged_tracks, tau_0_ele_bdt_score):
    if tau_0_n_charged_tracks == 1:
        return tau_0_ele_bdt_score > min_eVetoBDT_cut
    return True

def cut_eBDTVeto(tau_0_n_charged_tracks, tau_0_ele_bdt_score_trans):
    #the newest BDT from Terry provides 3 working points: 
    # - Tight  (75%): 0.25 
    # - Medium (85%): 0.15 
    # - Loose  (95%): 0.05 
    # the BDT doesn't differenciate the cases 3p from 1p
    if tau_0_n_charged_tracks == 1:
        return tau_0_ele_bdt_score_trans <= 0.15
    # elif tau_0_n_charged_tracks == 3:
    #     return tau_0_ele_bdt_score_trans <= 0.05
    return False

#used only in the definition of the ZeeCR antiID region
def cut_eBDTVeto_antiIDRegion(tau_0_n_charged_tracks, tau_0_ele_bdt_score_trans):
    #the newest BDT from Terry provides 3 working points: 
    # - Tight  (75%): 0.25 
    # - Medium (85%): 0.15 
    # - Loose  (95%): 0.05 
    # the BDT doesn't differenciate the cases 3p from 1p
    if tau_0_n_charged_tracks == 1:
        return tau_0_ele_bdt_score_trans > 0.15
    # elif tau_0_n_charged_tracks == 3:
    #     return tau_0_ele_bdt_score_trans <= 0.05
    return False

# def cut_LFV_signal(tau_0_matched_pdgId):
#     return not((np.abs(tau_0_matched_pdgId) < 7 or tau_0_matched_pdgId == 21) == 0)

def cut_1P(tau_0_n_charged_tracks):
    condition = tau_0_n_charged_tracks == 1
    return not(condition)

def cut_bJetsVeto(n_bjets):
    return n_bjets !=0

# cut on deltaphi

def cut_ditau_met_sum_cos_dphi(ditau_met_sum_cos_dphi):
    return ditau_met_sum_cos_dphi <= -0.35
                  

def cut_absDEta_lep_tau(ditau_deta):
    return np.abs(ditau_deta) >= 2

def cut_ditau_vis_mass(ditau_matched_vis_mass):
    condition = (ditau_matched_vis_mass <=90.) or (ditau_matched_vis_mass >=100.)
    return not(condition)

def cut_ditau_mmc(ditau_mmc_mlm_fit_status):
    return ditau_mmc_mlm_fit_status != 1 
    
def cut_ditau_mt_lep1_met(ditau_mt_lep1_met):
    return ditau_mt_lep1_met > 70


def cut_jetFakingTau(tau_0_matched_pdgId):
    condition = (np.abs(tau_0_matched_pdgId)<7 or tau_0_matched_pdgId==21)
    if condition:
        return True
    return False


################################################################################
# cuts for Zee control region definition
################################################################################
                  
def cut_ZeeCR_ditau_vis_mass(ditau_vis_mass):
    # condition = np.abs(ditau_vis_mass - z_mass) < 5.
    # condition = ditau_vis_mass  <= 110. and ditau_vis_mass >= 80.#test made on 2020-09-24
    condition = ditau_vis_mass  <= 110. and ditau_vis_mass >= 85.#test made on 2020-10-10
    return not(condition)
                  
def cut_ZeeCR_tauID(tau_0_jet_rnn_medium, tau_0_jet_rnn_tight):
    condition = tau_0_jet_rnn_medium and not(tau_0_jet_rnn_tight)
    return not(condition)

def cut_ZeeCR_ditau_mt_lep0_met(ditau_mt_lep0_met):
    return not(ditau_mt_lep0_met < 60.)
    
def cut_ZeeCR_ditau_mt_lep1_met(ditau_mt_lep1_met):
    return not(ditau_mt_lep1_met < 40)
    
################################################################################
# VBF cuts
################################################################################
def cut_n_jet_30(n_jets):
    return n_jets < 2

def cut_jet_pt(jet_1_pt):
    return jet_1_pt <= 30.

def cut_jet_0_pt(jet_0_pt):
    return jet_0_pt <= 40.

def cut_dijetMass(dijet_m):
    return dijet_m <= 400. #350.

def cut_dijet_absdeta(dijet_absDEta):
    return dijet_absDEta <= 3

def cut_jets_opposite_hem(jet_0_eta,jet_1_eta):
    return jet_0_eta*jet_1_eta > 0

def cut_dijet_centrality(is_dijet_centrality):
    return is_dijet_centrality != 1

################################################################################

################################################################################
# tau selection
################################################################################
def cut_tau_eta(tau_0_leadTrk_eta):
    return np.abs(tau_0_leadTrk_eta)>=2.47

def cut_tauID(tau_0_jet_rnn_tight):
    return tau_0_jet_rnn_tight != 1

def cut_crack_region(tau_0_leadTrk_eta):
    return np.abs(tau_0_leadTrk_eta)>=1.37 and np.abs(tau_0_leadTrk_eta)<=1.52

def cut_tau_jet_tight(tau_0_jet_bdt_tight):
    return tau_0_jet_bdt_tight < 1

def cut_tau_n_tacks(tau_0_allTrk_n):
    return tau_0_allTrk_n != 1 and tau_0_allTrk_n != 3

def cut_tau_leadTrk_pT(tau_0_leadTrk_pt):
    return tau_0_leadTrk_pt<=25

def cut_ditau_charge(ditau_qxq):
    return not(ditau_qxq == -1)

def cut_met_pt(met_pt):
    return met_pt< 20

#-------------------------------------------------------------------------------

################################################################################
# Scale factors
################################################################################

def calc_SF_Ele(NOMINAL_pileup_random_run_number,
                tau_1_NOMINAL_EleEffSF_offline_RecoTrk, 
                tau_1_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v13, 
                tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient,
                tau_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v13_Gradient,
                tau_1_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_Gradient,
                tau_1_matched_mother_pdgId,
                tau_1_pt):
    result  = 1.
    result *= tau_1_NOMINAL_EleEffSF_offline_RecoTrk
    result *= tau_1_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v13
    result *= tau_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v13_Gradient 

    result *= tau_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolGradient
    result *= tau_1_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_Gradient

    # if abs(tau_1_matched_mother_pdgId) == 15:
    #     result *= (0.051779/(math.exp(-0.141434*(tau_1_pt-13.4))+1)+0.923748+0.000110131*tau_1_pt)

    return result

def calc_SF_Mu(NOMINAL_pileup_random_run_number, 
               tau_1_NOMINAL_MuEffSF_Reco_QualMedium,
               tau_1_NOMINAL_MuEffSF_IsoFCTight_FixedRad,
               tau_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium,
               tau_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium):
    result  = 1.
    result *= tau_1_NOMINAL_MuEffSF_Reco_QualMedium
    result *= tau_1_NOMINAL_MuEffSF_IsoFCTight_FixedRad

    if NOMINAL_pileup_random_run_number <= run_margin_2015[0]:
        result *= tau_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium
    else :
        result *= tau_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium

    return result

def calc_SF_Tau(tau_0_NOMINAL_TauEffSF_reco, 
                tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad,
                tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron, 
                tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron, 
                tau_0_NOMINAL_TauEffSF_JetRNNtight,
                n_electrons, n_muons, tau_0_n_charged_tracks,
                tau_0_pdg, tau_0_decay_mode, tau_0_pt, tau_0_eta):
    if (n_electrons==1 and tau_0_n_charged_tracks == 1):
        tauEffSF_MediumEleBDT_electron = getEVetoSF(tau_0_pdg, tau_0_decay_mode, tau_0_pt, tau_0_eta) #tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron 
    else:
        tauEffSF_MediumEleBDT_electron = 1

    if( (n_electrons==1 and tau_0_n_charged_tracks==3) or (n_muons==1) ):
        tauEffSF_MediumEleBDT_electron = 1.0  # Should only be use for etau channel 1p

    return tau_0_NOMINAL_TauEffSF_reco*tauEffSF_MediumEleBDT_electron*tau_0_NOMINAL_TauEffSF_JetRNNtight

def calc_SF_Jet(jet_NOMINAL_central_jets_global_effSF_JVT,
                jet_NOMINAL_central_jets_global_ineffSF_JVT,
                jet_NOMINAL_forward_jets_global_effSF_JVT,
                jet_NOMINAL_forward_jets_global_ineffSF_JVT,
                jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_85,
                jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_85): 
    return jet_NOMINAL_central_jets_global_effSF_JVT*jet_NOMINAL_central_jets_global_ineffSF_JVT*jet_NOMINAL_forward_jets_global_effSF_JVT*jet_NOMINAL_forward_jets_global_ineffSF_JVT*jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_85*jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_85

def reeval_wg_wo_eVeto_SF(weight, tau_0_n_charged_tracks,
                          tau_0_pdg, tau_0_decay_mode, tau_0_pt, tau_0_eta):
    if tau_0_n_charged_tracks == 1:
        tauEffSF_MediumEleBDT_electron = getEVetoSF(tau_0_pdg, tau_0_decay_mode, tau_0_pt, tau_0_eta)
        weight = weight/tauEffSF_MediumEleBDT_electron
    return weight

def reeval_wg_wo_jetRNN_SF(weight, tau_0_jetRNN_SF):
    return weight/tau_0_jetRNN_SF

def calc_weight(sf_jet, sf_tau, scale_factor, weight,
                NOMINAL_pileup_combined_weight, 
                cross_section, kfactor, geneff, metadata):
    
    return sf_jet*sf_tau*scale_factor*weight*NOMINAL_pileup_combined_weight*lumi*cross_section*kfactor*geneff/metadata

def n_weighted_evts(data):
    if len(data.index) != 0:
        return sum(data['totalWeight_Mu'])
    return 0

def calc_dR(eta_0, phi_0, eta_1, phi_1):
    delR=np.sqrt(((eta_0 - eta_1)**2)+(((phi_0 - phi_1 + np.pi)%(2*np.pi)-np.pi)**2))
    return delR

def calc_dEta(eta_0, eta_1):
    dEta=eta_0 - eta_1
    return dEta

def calc_ditau_vis_mass(tau_0_pt, tau_1_pt, tau_0_eta, tau_1_eta, tau_0_phi, tau_1_phi):
    return math.sqrt(2*tau_1_pt*tau_0_pt*(math.cosh(tau_1_eta-tau_0_eta)-math.cos(tau_1_phi-tau_0_phi)))

def calc_mll(lep_pts,lep_etas,lep_phis):
    mll = 2*lep_pts[0]*lep_pts[1]
    cosh = math.cosh(lep_etas[0]-lep_etas[1])
    cos = math.cos(lep_phis[0]-lep_phis[1])
    mll *= ( cosh - cos )
    return math.sqrt(mll)/1000.

def get_dsid_from_name_int(name):
    newstr = ''.join((ch if ch in '0123456789' else ' ') for ch in name)
    listOfNumbers = [int(i) for i in newstr.split()]
    return listOfNumbers[3]

def get_dsid_from_name_str(name):
    newstr = ''.join((ch if ch in '0123456789' else ' ') for ch in name)
    listOfNumbers = [str(i) for i in newstr.split()]
    return listOfNumbers[3]

def get_metadata(path, dsid):

    if  isHiggsFile(path, dsid):
        hMetadata = uproot.open(path)["h_metadata_theory_weights"] # to be used in V04
        print("[get_metadata] Higgs data set found! getting metadata...")
        if "ggH" in path:
            print("[get_metadata] ggH metadata...")
            metadata = hMetadata[152]
            if hMetadata[152] < 1e-6:
                print("data file: "+str(path)+" has metaData[152] = "+str(hMetadata[152]))
        else:
            print("[get_metadata] non-ggH metadata...")
            metadata = hMetadata[110]
            if hMetadata[110] < 1e-6:
                print("data file: "+str(path)+" has metaData[110] = "+str(hMetadata[110]))
    else:
        print("[get_metadata] Non-Higgs data set found! getting metadata...")
        hMetadata = uproot.open(path)["h_metadata"] # to be used in V04
        metadata = hMetadata[8]

        if hMetadata[8] < 1e-6:
            print("data file: "+str(path)+" has metaData[8] = "+str(hMetadata[8]))

    return metadata
    

def read_file(path,sample, metadata, ch):
    #    info = infofile(args.input_DSIDs)

    newstr = ''.join((ch if ch in '0123456789' else ' ') for ch in sample)
    listOfNumbers = [str(i) for i in newstr.split()]
    dsid = -1
    if len(listOfNumbers) > 3:
        dsid   = listOfNumbers[3]
    

    #check if it's an empty file
    filesize = os. path. getsize(path) 
    if filesize == 0: 
        print("[eval_yields::read_file] file {} is empty: {}".format(path, filesize))
        data = []
        goodDataFrame = False
        return goodDataFrame, data, data, data, data, data, data, data

    #get the TTree
    executor = ThreadPoolExecutor(4)
    mc = uproot.open(path)["NOMINAL"]
    data = mc.pandas.df(variables, executor=executor, flatten=False) 
    goodDataFrame = True

    if (len(data.index) == 0):
        dsid   = get_dsid_from_name_int(sample)
        # print ("%6s %10.3f"% (dsid, len(data.index)))
        print("Processing DSID: %i \n\t file:%s "%(dsid, path))
        print("\n\t data.index = %i "%len(data.index))
        data = []
        goodDataFrame = False
        return goodDataFrame, data, data, data, data, data, data, data
        
    if (len(mc.array("tau_0_p4")) == 0):
        dsid   = get_dsid_from_name_int(sample)
        # print ("%6s %10.3f"% (dsid, len(data.index)))
        print("Processing DSID: %i \n\t file:%s "%(dsid, path))
        print("\n\t tau_0_p4 = %i "%len(mc.array("tau_0_p4")))
        data = []
        goodDataFrame = False
        return goodDataFrame, data, data, data, data, data, data, data
    
    get_reco_dat(data, mc)

    #add some key instances for identifying the CR and antiID regions
    add_ID_keys(data)

    #crop bad events
    # fail = data[ np.vectorize(cut_bad_batman)(data.event_is_bad_batman)].index
    # data.drop(fail, inplace=True)

    if ch != 'Data':
        get_MC_data(data, mc)       
    else:
        data['NOMINAL_pileup_random_run_number'] = mc.array('run_number')

    eval_weights(ch, mc, data, path, sample, dsid, metadata)

    if (args.debug == True): print("[READ_FILE::CUT_FLOW] Starting...")

    #--------------------------------------------------------------------------------
    # get rid of the events with negative run number
    #--------------------------------------------------------------------------------
    if ch != 'Data':
        fail = data[ np.vectorize(cut_run_number)(data.NOMINAL_pileup_random_run_number)].index
        data.drop(fail, inplace=True)

    # if 'Sh' in path:
    #     fail = data[ np.vectorize(cut_Sherpa_weight_mc)(data.weight_mc)].index
    #     data.drop(fail, inplace=True)

    #crop bad events
    fail = data[ np.vectorize(cut_bad_batman)(data.event_is_bad_batman)].index
    data.drop(fail, inplace=True)

    nIn              = sum(data.totalWeight_Ele) 
    cutId = 0
    cut_flow_etau_VBF   [cutId] += nIn
    cut_flow_etau_VBF_nowg[cutId] += len(data.index)

    cut_flow_mutau_VBF   [cutId] += sum(data.totalWeight_Mu)
    cut_flow_mutau_VBF_nowg[cutId] += len(data.index)
    if (args.debug == True): print("cutId: "+str(cutId)+" entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    cutId += 1

    #--------------------------------------------------------------------------------
    # some debugging; print the DSID and the number of events
    #--------------------------------------------------------------------------------
    # newstr = ''.join((ch if ch in '0123456789' else ' ') for ch in sample)
    # listOfNumbers = [str(i) for i in newstr.split()]
    # dsid   = listOfNumbers[3]
    # print ("%6s %10.3f"% (dsid, len(data.index)))
    #--------------------------------------------------------------------------------

    #--------------------------------------------------------------------------------
    # Trigger cuts
    #--------------------------------------------------------------------------------
    if len(data.index) != 0:
        fail = data[ np.vectorize(cut_trigger)(data.tau_1_pt,
                                               data.NOMINAL_pileup_random_run_number, 
                                               data.tau_1_electron_trig_HLT_e24_lhmedium_L1EM20VH, 
                                               data.tau_1_electron_trig_HLT_e60_lhmedium, 
                                               data.tau_1_electron_trig_HLT_e60_lhmedium_nod0,
                                               data.tau_1_electron_trig_HLT_e120_lhloose,
                                               data.tau_1_electron_trig_HLT_e26_lhtight_nod0_ivarloose,
                                               data.tau_1_electron_trig_HLT_e140_lhloose_nod0,
                                               data.tau_1_muon_trig_HLT_mu20_iloose_L1MU15, 
                                               data.tau_1_muon_trig_HLT_mu50,
                                               data.tau_1_muon_trig_HLT_mu26_ivarmedium 
                                           )].index
        data.drop(fail, inplace=True)
        
    cut_flow_etau_VBF   [cutId] += sum(data.totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data.index)

    cut_flow_mutau_VBF   [cutId] += sum(data.totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data.index)
    if (args.debug == True): print("cutId: "+str(cutId)+" entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    cutId += 1
    

    #--------------------------------------------------------------------------------
    # !JetFakingTau
    #--------------------------------------------------------------------------------
    if ch != 'Data':
        if len(data.index) != 0:
            fail = data[ np.vectorize(cut_jetFakingTau)(data.tau_0_matched_pdgId)].index
            data.drop(fail, inplace=True)

    cut_flow_etau_VBF   [cutId] += sum(data.totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data.index)

    cut_flow_mutau_VBF   [cutId] += sum(data.totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data.index)
    if (args.debug == True): print("cutId: !jetFacking, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    cutId += 1

    #********************************************************************************
    #--------------------------------------------------------------------------------
    # from now, we need to separate the cuts applied for the etau and mutau channels
    #--------------------------------------------------------------------------------
    #********************************************************************************

    data_mutau = data.copy()
    if (args.debug == True): print("copied dataframe data to create dataframe data_mutau")

    #--------------------------------------------------------------------------------
    # Select the channel
    #--------------------------------------------------------------------------------
    is_etau_channel = False
    if len(data.index) != 0:
        fail = data[ np.vectorize(cut_etau_channel)(data.n_electrons, data.n_muons, ch, sample, dsid)].index
        data.drop(fail, inplace=True)
        if (len(data.index) != 0):
            is_etau_channel = True

    cut_flow_etau_VBF   [cutId] += sum(data.totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data.index)
    if (args.debug == True): print("cutId: eTau channel, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))

    is_mutau_channel = False
    if len(data_mutau.index) != 0:
        fail = data_mutau[ np.vectorize(cut_mutau_channel)(data_mutau.n_electrons, data_mutau.n_muons, ch, sample, dsid)].index
        data_mutau.drop(fail, inplace=True)
        if (len(data_mutau.index) != 0):
            is_mutau_channel = True

    cut_flow_mutau_VBF   [cutId] += sum(data_mutau.totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau.index)
    cutId += 1
  

    #--------------------------------------------------------------------------------
    # isHadronicTau: only for signal samples
    #--------------------------------------------------------------------------------
    if ch != 'Data':
        if isSignal(dsid):
            if len(data.index) != 0:
                fail = data[ np.vectorize(cut_hadronic_tau)(data.tau_0_matched_pdgId)].index
                data.drop(fail, inplace=True)

            if len(data_mutau.index) != 0:
                fail = data_mutau[ np.vectorize(cut_hadronic_tau)(data_mutau.tau_0_matched_pdgId)].index
                data_mutau.drop(fail, inplace=True)

    cut_flow_etau_VBF   [cutId] += sum(data.totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data.index)
    if (args.debug == True): print("cutId: isHadronicTau, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    cut_flow_mutau_VBF   [cutId] += sum(data_mutau.totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau.index)

    cutId += 1


    #--------------------------------------------------------------------------------
    # N-PVx
    #--------------------------------------------------------------------------------
    if len(data.index) != 0:
        fail = data[ np.vectorize(cut_npvx)(data.tau_1)].index
        data.drop(fail, inplace=True)

    cut_flow_etau_VBF   [cutId] += sum(data.totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data.index)
    if (args.debug == True): print("cutId: nPVX, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))

    if len(data_mutau.index) != 0:
        fail = data_mutau[ np.vectorize(cut_npvx)(data_mutau.tau_1)].index
        data_mutau.drop(fail, inplace=True)

    cut_flow_mutau_VBF   [cutId] += sum(data_mutau.totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau.index)
    cutId += 1

    #--------------------------------------------------------------------------------
    # Nbjets
    #--------------------------------------------------------------------------------
    if len(data.index) != 0:
        fail = data[ np.vectorize(cut_bJetsVeto)(data.n_bjets_DL1r_FixedCutBEff_85)].index
        data.drop(fail, inplace=True)
                  
    cut_flow_etau_VBF   [cutId] += sum(data.totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data.index)
    if (args.debug == True): print("cutId: Nbjets, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    if len(data_mutau.index) != 0:
        fail = data_mutau[ np.vectorize(cut_bJetsVeto)(data_mutau.n_bjets_DL1r_FixedCutBEff_85)].index
        data_mutau.drop(fail, inplace=True)
                  
    cut_flow_mutau_VBF   [cutId] += sum(data_mutau.totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau.index)
    cutId += 1

    #--------------------------------------------------------------------------------
    # lepton quality cuts
    #--------------------------------------------------------------------------------
    if len(data.index) != 0:
        fail = data[ np.vectorize(cut_lepton_medium_id)(data.tau_1_id_medium)].index
        data.drop(fail, inplace=True)
        if is_etau_channel:
            if len(data.index) != 0:
                fail = data[ np.vectorize(cut_lepton_quality_etau)(data.tau_1_pt, data.tau_1_eta, data.tau_1_iso_Gradient)].index
                data.drop(fail, inplace=True)
            
    cut_flow_etau_VBF   [cutId] += sum(data.totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data.index)
    if (args.debug == True): print("cutId: leptopn quality, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))

    if len(data_mutau.index) != 0:
        fail = data_mutau[ np.vectorize(cut_lepton_medium_id)(data_mutau.tau_1_id_medium)].index
        data_mutau.drop(fail, inplace=True)
        #        if is_mutau_channel:
        if len(data_mutau.index) != 0:
            fail = data_mutau[ np.vectorize(cut_lepton_quality_mutau)(data_mutau.n_muons, data_mutau.tau_1_pt, data_mutau.tau_1_eta, data_mutau.tau_1_iso_FCTight_FixedRad, data_mutau.tau_1_iso_Gradient)].index
            data_mutau.drop(fail, inplace=True)
            
    cut_flow_mutau_VBF   [cutId] += sum(data_mutau.totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau.index)

    cutId += 1




    #--------------------------------------------------------------------------------
    # tau quality
    #--------------------------------------------------------------------------------
    if len(data.index) != 0:
        fail = data[ np.vectorize(cut_tau_quality)(data.tau_0_pt, data.tau_0_q, data.tau_0_n_charged_tracks, data.tau_0_eta)].index
        data.drop(fail, inplace=True)
            
    cut_flow_etau_VBF   [cutId] += sum(data.totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data.index)
    if (args.debug == True): print("cutId: tau quality, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    if len(data_mutau.index) != 0:
        fail = data_mutau[ np.vectorize(cut_tau_quality)(data_mutau.tau_0_pt, data_mutau.tau_0_q, data_mutau.tau_0_n_charged_tracks, data_mutau.tau_0_eta)].index
        data_mutau.drop(fail, inplace=True)
            
    cut_flow_mutau_VBF   [cutId] += sum(data_mutau.totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau.index)
    cutId += 1
 

    # 
    data['os_leptau'] = False
    data_mutau['os_leptau'] = False

    if len(data.index) != 0:
        data['os_leptau']       = data.apply(lambda x: os_leptau(x['tau_0_q'],x['tau_1_q']), axis=1, raw=True)
    if len(data_mutau.index) != 0:
        data_mutau['os_leptau'] = data_mutau.apply(lambda x: os_leptau(x['tau_0_q'],x['tau_1_q']), axis=1, raw=True)

    #let's copy the data frames for the control region
    data_etau_ZeeCR         = data.copy()
    data_mutau_ZeeCR        = data_mutau.copy()
 
    #--------------------------------------------------------------------------------
    # OS
    #--------------------------------------------------------------------------------
    # if len(data.index) != 0:
    #     fail = data[ np.vectorize(cut_OS_leptau)(data.tau_0_q, data.tau_1_q)].index
    #     data.drop(fail, inplace=True)
            
    cut_flow_etau_VBF   [cutId] += sum(data[(data['os_leptau'] == True)].totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data[(data['os_leptau'] == True)].index)
    if (args.debug == True): print("cutId: OS, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    # if len(data_mutau.index) != 0:
    #     fail = data_mutau[ np.vectorize(cut_OS_leptau)(data_mutau.tau_0_q, data_mutau.tau_1_q)].index
    #     data_mutau.drop(fail, inplace=True)
            
    cut_flow_mutau_VBF     [cutId] += sum(data_mutau[(data_mutau['os_leptau'] == True)].totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau[(data_mutau['os_leptau'] == True)].index)
    cutId += 1
                    
    #--------------------------------------------------------------------------------
    # Tau ID
    #--------------------------------------------------------------------------------
    # if len(data.index) != 0:
    #     fail = data[ np.vectorize(cut_tauID)(data.tau_0_jet_rnn_tight)].index
    #     data.drop(fail, inplace=True)
            
    cut_flow_etau_VBF     [cutId] += sum(data[(data['os_leptau'] == True) & (data['tau_0_jet_rnn_tight'] == True)].totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data[(data['os_leptau'] == True) & (data['tau_0_jet_rnn_tight'] == True)].index)
    if (args.debug == True): print("cutId: OS, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    # if len(data_mutau.index) != 0:
    #     fail = data_mutau[ np.vectorize(cut_tauID)(data_mutau.tau_0_jet_rnn_tight)].index
    #     data_mutau.drop(fail, inplace=True)
            
    cut_flow_mutau_VBF     [cutId] += sum(data_mutau[(data_mutau['os_leptau'] == True) & (data_mutau['tau_0_jet_rnn_tight'] == True)].totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau[(data_mutau['os_leptau'] == True) & (data_mutau['tau_0_jet_rnn_tight'] == True)].index)
    cutId += 1
    
    #--------------------------------------------------------------------------------
    # eVeto
    #--------------------------------------------------------------------------------
    data_etau_preEVeto = pd.DataFrame() #data.copy()

    if len(data.index) != 0:
        data['eVeto']           = data.apply(lambda x: pass_eBDTVeto(x['tau_0_n_charged_tracks'],x['tau_0_ele_bdt_score']), axis=1, raw=True)
        data['veryloose_eVeto'] = data.apply(lambda x: veryloose_eBDTVeto(x['tau_0_n_charged_tracks'],x['tau_0_ele_bdt_score']), axis=1, raw=True)
        # fail = data[ np.vectorize(cut_eBDTVeto)(data.tau_0_n_charged_tracks, data.tau_0_ele_bdt_score)].index
        # data.drop(fail, inplace=True)
            
    cut_flow_etau_VBF   [cutId] += sum(data[(data['os_leptau'] == True) & (data['tau_0_jet_rnn_tight'] == True) & (data['eVeto'] == True)].totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data[(data['os_leptau'] == True) & (data['tau_0_jet_rnn_tight'] == True)& (data['eVeto'] == True)].index)
    if (args.debug == True): print("cutId: eVeto, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    # we dont' apply the eVeto to the mutau
    # if len(data_mutau.index) != 0:
    #     fail = data_mutau[ np.vectorize(cut_eBDTVeto)(data_mutau.tau_0_n_charged_tracks, data_mutau.tau_0_ele_bdt_score)].index
    #     data_mutau.drop(fail, inplace=True)
            
    cut_flow_mutau_VBF   [cutId] += sum(data_mutau[(data_mutau['os_leptau'] == True) & (data_mutau['tau_0_jet_rnn_tight'] == True)].totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau[(data_mutau['os_leptau'] == True) & (data_mutau['tau_0_jet_rnn_tight'] == True)].index)
    cutId += 1

    #--------------------------------------------------------------------------------
    # Zll VBF filter samples
    #--------------------------------------------------------------------------------
    if args.realData == False:
        if isVBFFilter(sample):
            if len(data.index) != 0:
                fail = data[ np.vectorize(cut_VBFFilter)(data.truth_passedVBFFilter)].index
                data.drop(fail, inplace=True)
            
    cut_flow_etau_VBF   [cutId] += sum(data[(data['os_leptau'] == True) & (data['tau_0_jet_rnn_tight'] == True) & (data['eVeto'] == True)].totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data[(data['os_leptau'] == True) & (data['tau_0_jet_rnn_tight'] == True) & (data['eVeto'] == True)].index)
    if (args.debug == True): print("cutId: VBFFilter, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))

    if args.realData == False:
        if isVBFFilter(sample):
            if len(data_mutau.index) != 0:
                fail = data_mutau[ np.vectorize(cut_VBFFilter)(data_mutau.truth_passedVBFFilter)].index
                data_mutau.drop(fail, inplace=True)
            
    cut_flow_mutau_VBF   [cutId] += sum(data_mutau[(data_mutau['os_leptau'] == True) & (data_mutau['tau_0_jet_rnn_tight'] == True)].totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau[(data_mutau['os_leptau'] == True) & (data_mutau['tau_0_jet_rnn_tight'] == True)].index)
    cutId += 1

    #--------------------------------------------------------------------------------
    # ditau_met_sum_cos_dphi
    #--------------------------------------------------------------------------------
    if len(data.index) != 0:
        fail = data[ np.vectorize(cut_ditau_met_sum_cos_dphi)(data.ditau_met_sum_cos_dphi)].index
        data.drop(fail, inplace=True)
            
    cut_flow_etau_VBF   [cutId] += sum(data[(data['os_leptau'] == True) & (data['tau_0_jet_rnn_tight'] == True) & (data['eVeto'] == True)].totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data[(data['os_leptau'] == True) & (data['tau_0_jet_rnn_tight'] == True) & (data['eVeto'] == True)].index)
    if (args.debug == True): print("cutId: ditau_met_sum, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    if len(data_mutau.index) != 0:
        fail = data_mutau[ np.vectorize(cut_ditau_met_sum_cos_dphi)(data_mutau.ditau_met_sum_cos_dphi)].index
        data_mutau.drop(fail, inplace=True)
            
    cut_flow_mutau_VBF   [cutId] += sum(data_mutau[(data_mutau['os_leptau'] == True) & (data_mutau['tau_0_jet_rnn_tight'] == True)].totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau[(data_mutau['os_leptau'] == True) & (data_mutau['tau_0_jet_rnn_tight'] == True)].index)    
    cutId += 1

    #--------------------------------------------------------------------------------
    # ditau_deta
    #--------------------------------------------------------------------------------
    if len(data.index) != 0:
        fail = data[ np.vectorize(cut_absDEta_lep_tau)(data.ditau_deta)].index
        data.drop(fail, inplace=True)
            
    cut_flow_etau_VBF   [cutId] += sum(data[(data['os_leptau'] == True) & (data['tau_0_jet_rnn_tight'] == True) & (data['eVeto'] == True)].totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data[(data['os_leptau'] == True) & (data['tau_0_jet_rnn_tight'] == True) & (data['eVeto'] == True)].index)
    if (args.debug == True): print("cutId: ditau_deta, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    if len(data_mutau.index) != 0:
        fail = data_mutau[ np.vectorize(cut_absDEta_lep_tau)(data_mutau.ditau_deta)].index
        data_mutau.drop(fail, inplace=True)
            
    cut_flow_mutau_VBF   [cutId] += sum(data_mutau[(data_mutau['os_leptau'] == True) & (data_mutau['tau_0_jet_rnn_tight'] == True)].totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau[(data_mutau['os_leptau'] == True) & (data_mutau['tau_0_jet_rnn_tight'] == True)].index)
    cutId += 1

    apply_ZeeCR_cuts(sample,
                     data_etau_ZeeCR, data_mutau_ZeeCR, 
                     cut_flow_etau_ZeeCR, cut_flow_etau_ZeeCR_nowg,
                     cut_flow_mutau_ZeeCR, cut_flow_mutau_ZeeCR_nowg, 
                     ch)
    
    print("[READ_FILE::CUT_FLOW] applying VBF cuts...")

    dataNonVBF = data.copy()
    #--------------------------------------------------------------------------------
    # VBF cuts for the etau channel
    #--------------------------------------------------------------------------------
    apply_VBF_cuts(data)

    fail = data.copy()
    dataNonVBF.drop(fail.index, inplace=True)

    if 'Sh' in path:
        if len(dataNonVBF.index) != 0:
            fail = dataNonVBF[ np.vectorize(cut_Sherpa_weight_mc)(dataNonVBF.weight_mc)].index
            dataNonVBF.drop(fail, inplace=True)

    cut_flow_etau_VBF   [cutId] += sum(dataNonVBF[(dataNonVBF['os_leptau'] == True) & (dataNonVBF['tau_0_jet_rnn_tight'] == True) & (dataNonVBF['eVeto'] == True)].totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(dataNonVBF[(dataNonVBF['os_leptau'] == True) & (dataNonVBF['tau_0_jet_rnn_tight'] == True) & (dataNonVBF['eVeto'] == True)].index)
    if (args.debug == True): print("cutId: nonVBF, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))

    data_mutauNonVBF = data_mutau.copy()
    #--------------------------------------------------------------------------------
    # VBF cuts for the mutau
    #--------------------------------------------------------------------------------
    apply_VBF_cuts(data_mutau)

    fail = data_mutau.copy()
    data_mutauNonVBF.drop(fail.index, inplace=True)

    if 'Sh' in path:
        if len(data_mutauNonVBF.index) != 0:
            fail = data_mutauNonVBF[np.vectorize(cut_Sherpa_weight_mc)(data_mutauNonVBF.weight_mc)].index
            data_mutauNonVBF.drop(fail, inplace=True)

    cut_flow_mutau_VBF   [cutId] += sum(data_mutauNonVBF[(data_mutauNonVBF['os_leptau'] == True) & (data_mutauNonVBF['tau_0_jet_rnn_tight'] == True)].totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutauNonVBF[(data_mutauNonVBF['os_leptau'] == True) & (data_mutauNonVBF['tau_0_jet_rnn_tight'] == True)].index)

    cutId += 1

    #--------------------------------------------------------------------------------
    # ditau_vis_mass: applied only for the nonVBF region
    #--------------------------------------------------------------------------------
    if len(dataNonVBF.index) != 0:
        dataNonVBF['pass_ditauVisMassCut'] = dataNonVBF.apply(lambda x: (x['ditau_vis_mass'] <=90.) or (x['ditau_vis_mass'] >=100.), axis=1, raw=True)
        # fail = dataNonVBF[ np.vectorize(cut_ditau_vis_mass)(dataNonVBF.ditau_vis_mass)].index 
        # dataNonVBF.drop(fail, inplace=True)
        
    cut_flow_etau_VBF     [cutId] += sum(dataNonVBF[(dataNonVBF['os_leptau'] == True) & (dataNonVBF['tau_0_jet_rnn_tight'] == True) & (dataNonVBF['eVeto'] == True) & (dataNonVBF['pass_ditauVisMassCut'] == True)].totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(dataNonVBF[(dataNonVBF['os_leptau'] == True) & (dataNonVBF['tau_0_jet_rnn_tight'] == True) & (dataNonVBF['eVeto'] == True) & (dataNonVBF['pass_ditauVisMassCut'] == True)].index)
    # # print("cutId: ditau_vis_mass, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    # print("cutId: VBF, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))

    # if args.realData == False:
    #     if len(data_mutauNonVBF.index) != 0:
    #         fail = data_mutauNonVBF[ np.vectorize(cut_ditau_vis_mass)(data_mutauNonVBF.ditau_matched_vis_mass)].index
    #         data_mutauNonVBF.drop(fail, inplace=True)
        
    cut_flow_mutau_VBF     [cutId] += sum(data_mutauNonVBF[(data_mutauNonVBF['os_leptau'] == True) & (data_mutauNonVBF['tau_0_jet_rnn_tight'] == True)].totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutauNonVBF[(data_mutauNonVBF['os_leptau'] == True) & (data_mutauNonVBF['tau_0_jet_rnn_tight'] == True)].index)
    # # print("cutId: ditau_vis_mass, entries:" +str(cut_flow_etau_VBF_nowg[cutId])+" weight sum: "+str(cut_flow_etau_VBF[cutId]))
    cutId += 1


    cut_flow_etau_VBF     [cutId] += sum(data[(data['os_leptau'] == True) & (data['tau_0_jet_rnn_tight'] == True) & (data['eVeto'] == True)].totalWeight_Ele) 
    cut_flow_etau_VBF_nowg[cutId] += len(data[(data['os_leptau'] == True) & (data['tau_0_jet_rnn_tight'] == True) & (data['eVeto'] == True)].index)

    #debug block
    if args.realData == False:
        print("gen_eff = %2.3f xsec = %2.3f kFactor = %2.3f"%(info.get_genEff(sample), info.get_xsec(sample), info.get_kfactor(sample)) )
    # print(dataNonVBF.scaleFactor_Jet)
    # print(dataNonVBF.scaleFactor_Tau)
    # print(dataNonVBF.scaleFactor_Ele)
    # print(dataNonVBF.weight_mc)
    # print(dataNonVBF.NOMINAL_pileup_combined_weight)
    # print(dataNonVBF.totalWeight_Ele)
    cut_flow_mutau_VBF   [cutId] += sum(data_mutau[(data_mutau['os_leptau'] == True) & (data_mutau['tau_0_jet_rnn_tight'] == True)].totalWeight_Mu) 
    cut_flow_mutau_VBF_nowg[cutId] += len(data_mutau[(data_mutau['os_leptau'] == True) & (data_mutau['tau_0_jet_rnn_tight'] == True)].index)
    if (args.debug == True): print("nonVBF: %4.3f"%cut_flow_etau_VBF   [cutId-1])

    if (args.debug == True): print("[READ_FILE::CUT_FLOW] END")

    
    # make sure we are not counting the signal samples in the wrong channels
    if 'LFV' in ch:
        if '_mt' in path or '_mutau' in path:
            data            = pd.DataFrame(columns=data.columns)
            dataNonVBF      = pd.DataFrame(columns=dataNonVBF.columns)
            data_etau_ZeeCR    = pd.DataFrame(columns=data_etau_ZeeCR.columns)
            # data_etau_ZeeCR_antiID    = pd.DataFrame(columns=data_etau_ZeeCR_antiID.columns)
            for i in range(cutId):
                cut_flow_etau_VBF     [i] = 0.
                cut_flow_etau_VBF_nowg[i] = 0

        elif '_et' in path:
            data_mutau       = pd.DataFrame(columns=data_mutau.columns)
            data_mutauNonVBF = pd.DataFrame(columns=data_mutauNonVBF.columns)
            data_mutau_ZeeCR    = pd.DataFrame(columns=data_mutau_ZeeCR.columns)
            # data_mutau_ZeeCR_antiID    = pd.DataFrame(columns=data_mutau_ZeeCR_antiID.columns)
            for i in range(cutId):
                cut_flow_mutau_VBF     [i] = 0.
                cut_flow_mutau_VBF_nowg[i] = 0

    #remove Sherpa events with large weight
    if 'Sh' in path:
        if len(data.index) != 0:
            fail = data[np.vectorize(cut_Sherpa_weight_mc)(data.weight_mc)].index
            data.drop(fail, inplace=True)
        if len(data_mutau.index) != 0:
            fail = data_mutau[np.vectorize(cut_Sherpa_weight_mc)(data_mutau.weight_mc)].index
            data_mutau.drop(fail, inplace=True)
        if len(data_etau_ZeeCR.index) != 0:
            fail = data_etau_ZeeCR[np.vectorize(cut_Sherpa_weight_mc)(data_etau_ZeeCR.weight_mc)].index
            data_etau_ZeeCR.drop(fail, inplace=True)
        if len(data_mutau_ZeeCR.index) != 0:
            fail = data_mutau_ZeeCR[np.vectorize(cut_Sherpa_weight_mc)(data_mutau_ZeeCR.weight_mc)].index
            data_mutau_ZeeCR.drop(fail, inplace=True)


    #add the info of the DSID
    df_list = [data, data_mutau, data_etau_preEVeto, dataNonVBF, data_mutauNonVBF, 
               data_etau_ZeeCR, data_mutau_ZeeCR]
    #               data_etau_ZeeCR_antiID, data_mutau_ZeeCR_antiID]
    for df in df_list:
        df['DSID'] = dsid
    
    return goodDataFrame, data, data_mutau, data_etau_preEVeto, dataNonVBF, data_mutauNonVBF, data_etau_ZeeCR, data_mutau_ZeeCR



#--------------------------------------------------------------------------------
def fillhist(pthist, ptarray):
    minH = pthist.GetBinLowEdge(1)
    maxH = pthist.GetBinLowEdge(pthist.GetNbinsX())+pthist.GetBinWidth(1)
    for v in ptarray:
        if v<minH:
            pthist.Fill(minH)
        elif v>maxH:
            pthist.Fill(maxH)
        else:            
            pthist.Fill(v)
    return pthist
            
def fillhistwg(pthist, ptarray, wgarray):
    minH = pthist.GetBinLowEdge(1)
    maxH = pthist.GetBinLowEdge(pthist.GetNbinsX())+pthist.GetBinWidth(1)
    for v, wg in zip(ptarray, wgarray):
        if v<minH:
            pthist.Fill(minH, wg)
        elif v>maxH:
            pthist.Fill(maxH, wg)
        else:          
            pthist.Fill(v, wg)
    return pthist
            

#-------------------------------------------------------------------------------
def paveText(text, left=0.15, right=0.5, bottom=0.9, top=0.94, size=0.045, align=13):
    pt = TPaveText(left, bottom, right, top, "NDC")
    pt.SetBorderSize(0)
    pt.SetFillStyle(0)
    pt.SetLineStyle(0)
    
    text = pt.AddText(text)
    text.SetTextSize(size)
    text.SetTextAlign(align)
    
    return pt


#--------------------------------------------------------------------------------
# overlay the BDT scores from the old and the new algorithm (from Terry Chan)
#--------------------------------------------------------------------------------
def plot_ROOT_eVetoScores(args, use_wg):
    vars  = import_module(args.vars).vars
    var_old   = vars['tau_0_ele_bdt_score_trans']
    var_new   = vars['tau_0_ele_bdt_score_trans_retuned']

    h_old     = TH1D(var_old.var, "{};{}".format(var_old.label,var_old.label), var_old.bins, var_old.min, var_old.max)
    h_new     = TH1D(var_new.var, "{};{}".format(var_new.label,var_new.label), var_new.bins, var_new.min, var_new.max)

    #fill the histogram
    data = samples['Zee']['data_etau_preEVeto']
    if len(data.index) == 0:
        return
    #    print (data)

    if use_wg == False:
        fillhist(h_old, data["{}".format(var_old.var)]) 
        fillhist(h_new, data["{}".format(var_new.var)]) 
    else:
        h_old.Sumw2()
        h_new.Sumw2()
        fillhistwg(h_old, data["{}".format(var_old.var)], data['totalWeight_Ele']) 
        fillhistwg(h_new, data["{}".format(var_new.var)], data['totalWeight_Ele'])  

    #    hists     = [ h_old, h_new ]
    hists     = [ h_new, h_old ]
    
    #search for the min and max y
    miny = float('inf')
    maxy = -float('inf')
    for h in hists:
        miny = min(miny, h.GetBinContent(h.GetMinimumBin()))
        maxy = max(maxy, h.GetBinContent(h.GetMaximumBin()))
        miny = max(miny, 1e-6)

    c = TCanvas("c", "c", 800, 700)
    for i, h in enumerate(hists):
        c.SetLeftMargin(0.15)
        c.SetBottomMargin(0.10)
        c.SetRightMargin(0.05)
        c.SetTopMargin(0.05)
        c.SetLogy(var_new.logy)
        
        h.SetLineColor(colours[i])
        h.SetLineWidth(2)
        h.SetMarkerStyle(markers[i])
        h.SetMarkerColor(colours[i])
        
        h.GetXaxis().SetTitle("eVeto BDT score")
        h.GetXaxis().SetTitleSize(0.042)
        h.GetXaxis().SetLabelSize(0.037)
        
        h.GetYaxis().SetTitle("Events / {:.2f}".format((var_new.max-var_new.min)/var_new.bins))
        h.GetYaxis().SetTitleSize(0.042)
        h.GetYaxis().SetLabelSize(0.037)
        
        if var_new.logy:
            h.SetMaximum((maxy/miny)**1.5 * miny)
        else:
            h.SetMaximum(1.10 * maxy)
            
        if i ==0 :
            h.Draw("EO")
        else:
            h.Draw("EO same")
            
    atlas_label = paveText("#font[72]{ATLAS} #font[42]{Simulation Internal}")
    atlas_label.Draw()

    legend = TLegend(0.7, 0.7, 0.925, 0.925)
    for i,h in enumerate(hists):
        legend.AddEntry(h, h.GetName(), "lp")
    legend.Draw()
    
    if use_wg == False:
        c.Print("%s/plot_eVeto_comparison.png"%args.output_dir)
    else:
        c.Print("%s/plot_eVeto_comparison_weight.png"%args.output_dir)
        
    c.Close()
        
#--------------------------------------------------------------------------------
def plot_ROOT(args, channel, tag, wg):
    vars  = import_module(args.vars).vars
    wgtag = 'unweighted'
    if wg == True:
        wgtag = 'weighted'
        if channel == 'etau':
            wg_ch = 'totalWeight_Ele'
        elif channel == 'mutau':
            wg_ch = 'totalWeight_Mu'


    # if tag != 'ZeeCR':
    #     chList = stack_order
    # else:
    #     chList = ['Zee']
    chList = stack_order

    if args.realData == True:
        chList = ['Data']
   
    for s in chList:
        data = samples[s]['data_{}_{}'.format(channel,tag)]
        if len(data) == 0:
            continue
        #print(data)
        #if data.empty :
        # ch_samples_cutflow = samples[s]['cut_flow_{}_VBF_nowg'.format(channel)]
        
        # if (tag == 'nonVBF'and ch_samples_cutflow[16] == 0):
        #     continue
        # if (tag == 'VBF'and ch_samples_cutflow[17] == 0):
        #     continue
            
        # if tag == 'ZeeCR':
        #     if ( channel == 'etau' and cut_flow_etau_ZeeCR_nowg[7] == 0) or (channel == 'mutau' and cut_flow_etau_ZeeCR_nowg[7] == 0):
        #         continue
        #create if necessary the directories for storing the plots
        output_dir = "{}/{}/{}/{}".format(args.output_dir, channel, s, tag)
        if not os.path.exists(output_dir):
            os.makedirs("{}".format(output_dir))
 
        rootFile = TFile("{}/{}_{}_{}_{}_hists.root".format(output_dir, channel, s, tag, wgtag), "RECREATE")
        fileName = rootFile.GetName()
        # print("channel = {}, filename: {} created!".format(channel,fileName))
        for var_name, var in vars.iteritems():
            #create the histogram

            if '_eta' not in var_name:
                h= TH1D("{}_{}_{}_{}_{}".format(channel,tag,s,var_name,wgtag), 
                        ";{}".format(var.label), var.bins, var.min, var.max)
            else:
                edges = eta_edges
                # if 'rebin' in var_name:
                #     edges = eta_edges_rebin
                nbins = len(edges) - 1
                h= TH1D("{}_{}_{}_{}_{}".format(channel,tag,s,var_name,wgtag), 
                        ";{}".format(var.label), nbins, edges)
            if wgtag == "weighted":
                h.Sumw2()

            #fill the histogram
            # fillhist(h, data[s]["{}".format(var_name)]) 
            if wg == False or s == 'Data':
                try:
                    vec = data["{}".format(var_name)]
                except:
                    print("[] no data found for Sample = {}, Channel = {}, Var = {}".format(s, channel, var_name))
                    continue
                fillhist(h, vec) #data["{}".format(var_name)]) 
            else:
                try:
                    vec = data["{}".format(var_name)] #, data["{}".format(wg_ch)]
                except:
                    print("[] no data found for Sample = {}, Channel = {}, Var = {}".format(s, channel, var_name))
                    continue
                fillhistwg(h, vec, data["{}".format(wg_ch)])  #data["{}".format(var_name)], data["{}".format(wg_ch)]) 
                

            #search for the min and max y
            miny = float('inf')
            maxy = -float('inf')

            #normalize?
            # if (h.Integral(0, h.GetNbinsX()+1) > 0):
            #     h.Scale(1/h.Integral(0, h.GetNbinsX()+1))
            # h.GetYaxis().SetTitle('Normalised number of events')
            miny = min(miny, h.GetBinContent(h.GetMinimumBin()))
            maxy = max(maxy, h.GetBinContent(h.GetMaximumBin()))
            miny = max(miny, 1e-6)
   
            c = TCanvas("c", "c", 800, 700)
            c.SetLeftMargin(0.15)
            c.SetBottomMargin(0.10)
            c.SetRightMargin(0.05)
            c.SetTopMargin(0.05)
            c.SetLogy(var.logy)

            i=0
            #            for i, h in enumerate(hists):
            h.SetLineColor(colours[i])
            h.SetLineWidth(2)
            h.SetMarkerStyle(markers[i])
            h.SetMarkerColor(colours[i])
            h.SetStats()

            h.GetXaxis().SetTitle(var.label.replace('.', ' '))
            h.GetXaxis().SetTitleSize(0.042)
            h.GetXaxis().SetLabelSize(0.037)
            
            if var.unit != "":
                h.GetYaxis().SetTitle("Events/({:.2f} {})".format((var.max-var.min)/var.bins, var.unit))
            else:
                h.GetYaxis().SetTitle("Events/{:.2f}".format((var.max-var.min)/var.bins))
            h.GetYaxis().SetTitleSize(0.042)
            h.GetYaxis().SetLabelSize(0.037)

            if var.logy and miny<1:
                miny=1
            if var.logy:
                h.SetMaximum((maxy/miny)**1.5 * miny)
            else:
                h.SetMaximum(1.5 * maxy)

            h.Draw("EO same")

            atlas_label = paveText("#font[72]{ATLAS} #font[42]{Simulation Internal}")
            atlas_label.Draw()

            legend = TLegend(0.7, 0.85, 0.925, 0.925)
            #            for i,h in enumerate(hists):
            legend.AddEntry(h, s, "lp")
            legend.Draw()
            c.Print("{}/plot_{}_{}_{}_{}_{}.png".format(output_dir, channel, tag, s, var.var, wgtag))

            rootFile.cd()
            h.Write()
            c.Close()

        rootFile.Close()
        print("file: {} saved!".format(fileName));

################################################################################

def plot_cutFlow_weighted(args, channel, ch):
    
    plt.clf()
    plt.rcdefaults()
    
    flow_etau_VBF = samples[ch]['cut_flow_etau_VBF']

    ax = plt.gca()

    y_pos = np.arange(len(cut_flow_etau_VBF_labels))

    ax.barh(y_pos, flow_etau_VBF, align='center')
    ax.set_yticks(y_pos)
    ax.set_yticklabels(cut_flow_etau_VBF_labels,fontsize=7)
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.set_xlabel('Events weighted')
    ax.set_title('Cut flow')

    for i, v in enumerate(flow_etau_VBF):
        ax.text(v + 3, i + .25, str("%6.2f"%v), color='blue', fontweight='bold')

    # top = len(y_pos)*1.1
    # plt.ylim(bottom=1,top=top)

    # plt.text(0.05,0.97,r'$\mathbf{{ATLAS}}$ Simulation Internal',ha="left",va="top",family='sans-serif',transform=ax.transAxes,fontsize=13)
    # plt.text(0.05,0.92,'Higgs LFV',ha="left",va="top",family='sans-serif',transform=ax.transAxes,style='italic',fontsize=8)
    # plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)

    if flow_etau_VBF[0]>0:
        plt.xscale('log')

    plt.savefig("{}/{}/{}/plot_cut_flow_weighted.pdf".format(args.output_dir,channel,ch))

    return

def plot_cutFlow(args, channel, ch):
    
    plt.clf()
    plt.rcdefaults()

    ax = plt.gca()

    flow_etau_VBF_nowg = samples[ch]['cut_flow_etau_VBF_nowg']
    # Example data
    y_pos = np.arange(len(cut_flow_etau_VBF_labels))

    ax.barh(y_pos, flow_etau_VBF_nowg, align='center')
    ax.set_yticks(y_pos)
    ax.set_yticklabels(cut_flow_etau_VBF_labels,fontsize=7)
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.set_xlabel('Events')
    ax.set_title('Cut flow')

    for i, v in enumerate(flow_etau_VBF_nowg):
        ax.text(v + 3, i + .25, str("%6i"%v), color='blue', fontweight='bold')

    # top = len(y_pos)*1.1
    # plt.ylim(bottom=1,top=top)

    # plt.text(0.05,0.97,r'$\mathbf{{ATLAS}}$ Simulation Internal',ha="left",va="top",family='sans-serif',transform=ax.transAxes,fontsize=13)
    # plt.text(0.05,0.92,'Higgs LFV',ha="left",va="top",family='sans-serif',transform=ax.transAxes,style='italic',fontsize=8)
    # plt.text(0.05,0.88,r'$\sqrt{s}=13\,\mathrm{TeV}$',ha="left",va="top",family='sans-serif',transform=ax.transAxes)

    if flow_etau_VBF_nowg[0] > 0:
        plt.xscale('log')

    plt.savefig("{}/{}/{}/plot_cut_flow.pdf".format(args.output_dir, channel, ch))

    return

def eval_yields_V05(args):
    start = time.time()
    get_data_from_files(args)

    #plot the cut-flow for each category
    print("[eval_yields_V05] ZeeCR wp = {}".format(ZeeCR_wp))
    channels = ['etau','mutau']
    for i in channels:
        for s in samples:
            if samples[s]['cut_flow_etau_VBF_nowg'][0]>0:
                plot_cutFlow(args, i, s)
                plot_cutFlow_weighted(args, i, s)

    # save the data frames
    for s in samples:
        for ch in samples[s]:
            if len(samples[s][ch])>0:
                if "etau" in ch:
                    channel = "etau"
                elif "mutau" in ch:
                    channel = "mutau"                    
                try :
                    samples[s][ch].to_csv("{}/{}/{}/csv/{}_filtered.csv".format(args.output_dir, channel, s, ch))
                    print("df lenght = {}, dataframe {} saved!".format(len(samples[s][ch]), ch))
                except:
                    pass
                    #print("df lenght = {}, dataframe {} not saved...".format(len(samples[s][ch]), ch))
                
    channels = ['etau', 'mutau']
    sel_regs = ['VBF', 'nonVBF', 'ZeeCR', 'ZeeCR_antiID',
                'VBF_fakes_antiID', 'nonVBF_fakes_antiID', 'ZeeCR_fakes_antiID' ]

    #make the plots weighted and unweighted
    for ch in channels:
        for sr in sel_regs:
            plot_ROOT(args, ch , sr, True)
            plot_ROOT(args, ch , sr, False)

    #plot the eVeto scores before the cut
    # plot_ROOT_eVetoScores(args, False)
    # plot_ROOT_eVetoScores(args, True )
    elapsed = time.time() - start

    print("Time taken: %10.2f "% elapsed)



################################################################################
if __name__=="__main__":
    args = parseArgs(sys.argv[1:])
    info = infofile(args.input_DSIDs)

    for mc,lum in zip(mc_tuples, lumi_vec):
        if args.mc_data == mc:
            #            tuple_path = "{}/LFV_Htaulep_R21/V0401/mc/lephad/{}/nom/".format(working_node,args.mc_data)
            #  tuple_path = "{}/LFV_Htaulep_R21/V04/mc/lephad/{}/nom/".format(working_node,args.mc_data)
            tuple_path = "{}/LFV_Htaulep_R21/V05/mc/lephad/{}/nom/".format(working_node,args.mc_data)
            lumi = lum

    for data in data_tuples:
        if args.mc_data == data:
            # tuple_path = "{}/LFV_Htaulep_R21/V0401/data/lephad/{}/".format(working_node,args.mc_data)
            # tuple_path = "{}/LFV_Htaulep_R21/V04/data/lephad/{}/".format(working_node,args.mc_data)
            tuple_path = "{}/LFV_Htaulep_R21/V05/data/lephad/{}/".format(working_node,args.mc_data)
            lumi  = 1

    if tuple_path == "":
        raise Exception('{} does not point to any MC or Data directory available'.format(args.mc_data))

    #create if necessary the directories for storing the plots
    if not os.path.exists("{}".format(args.output_dir)):
        os.makedirs("{}".format(args.output_dir))
        
    channels = ['etau', 'mutau']
    for ch in channels:
        if not os.path.exists("{}/{}".format(args.output_dir,ch)):
            os.makedirs("{}/{}".format(args.output_dir, ch))
            
        for s in samples:
            if not os.path.exists("{}/{}/{}".format(args.output_dir,ch,s)):
                os.makedirs("{}/{}/{}".format(args.output_dir,ch,s))
            if not os.path.exists("{}/{}/{}/csv".format(args.output_dir,ch,s)):
                os.makedirs("{}/{}/{}/csv".format(args.output_dir,ch,s))
    
    # if not os.path.exists("{}/etau".format(args.output_dir)):
    #     os.makedirs("{}/etau".format(args.output_dir))
    # if not os.path.exists("{}/mutau".format(args.output_dir)):
    #     os.makedirs("{}/mutau".format(args.output_dir))
    eval_yields_V05(args)

