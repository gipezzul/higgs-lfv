#!/bin/bash
#shopt -s expand_aliases
#list_dsids=(308092)

# export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# setupATLAS
# LCGENV_VER="LCG_95"
# LCGENV_ARCH="x86_64-centos7-gcc8-opt"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH ROOT"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH numpy"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH pip"

inputs=('LFV')
#inputs=('Zll')
mc_tuples=('mc16a')

for  i in "${inputs[@]}"
do
    for j in "${mc_tuples[@]}"
    do
	python eval_yields_V05.py -i inputs_V0401_${i}.txt -o /eos/user/g/gipezzul/plots_T05_${j} -mc ${j} >| test_${i}_${j}_`date +"%y-%m-%d"`.log 2>&1 &
    done
done


