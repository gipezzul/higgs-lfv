#!/bin/bash

list_dsids=(308092 345101 345102 364114 364115 364116 364117 364118 364119 364120 364121 364122 364123 364124 364125 364126 364127 364204 364205 364206 364207 364208 364209)

#list_dsids=(308092)

for  i in "${list_dsids[@]}"
do
    python eval_yields.py -i inputs_Zee.txt -o plots_Zee_newEVeto_test_20191106_$i -s $i>| test_Zee_newEVeto_test_20191106_$i.log 2>&1 &
done
