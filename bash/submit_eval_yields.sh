#!/bin/bash
shopt -s expand_aliases

source ~/.bash_profile

source setup.sh

#python eval_yields.py -i inputs_V0401.txt -o plots_V0401
inputs=('SM' 'Top' 'Zll' 'LFV' 'W_Jets')
for  i in "${inputs[@]}"
do
    python eval_yields.py -i inputs_V0401_${i}.txt -o plots_V0401 >| test_${i}_20191112.log 2>&1 &
done
