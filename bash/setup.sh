export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase


export FONTCONFIG_PATH=/etc/fonts

setupATLAS
LCGENV_VER="LCG_95"
LCGENV_ARCH="x86_64-centos7-gcc8-opt"
lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH ROOT"
lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH numpy"
lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH pip"

PATH=$PATH:$PWD/test:$PWD/bash:$PWD/python
#if missing, install uproot
# pip install uproot --user
