#!/bin/bash
#shopt -s expand_aliases
#list_dsids=(308092)

# export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# setupATLAS
# LCGENV_VER="LCG_95"
# LCGENV_ARCH="x86_64-centos7-gcc8-opt"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH ROOT"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH numpy"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH pip"

#inputs=('SM' 'Top' 'Zll' 'LFV' 'W_Jets')
selections=('preselection')
#selections=('SR2')
# mc_tuples=('mc16d')
#mc_tuples=('mc16a' 'mc16d' 'mc16e')
eff_types=('1D' '3EtaBins')
#eff_types=('2D' '3EtaBins')
working_node="/home/gpezz/project"

for  i in "${selections[@]}"
do
#    python eval_yields.py -i inputs_V0401_${i}.txt -o /project/hep/demers/gp364/plots_V0401_mc16e -mc mc16e >| test_${i}_mc16e_20191117.log 2>&1 &
    for j in "${eff_types[@]}"
    do
    	# python python/make_stack_plots.py -mc ${j} -sr ${i} -sr2 'preselection'  --save-fakes True > /dev/null 2>&1 &
    	# python python/make_stack_plots.py -mc ${j} -sr 'nonVBF'  -sr2 ${i} --data-driven True >| make_plots_dd_${i}_${j}_`date +"%y-%m-%d"`.log 2>&1 &
    	python python/make_stack_plots.py -mc 'all' -sr 'VBF'  -sr2 ${i} --eFF-version ${j} --data-driven True -t 'V05_v1ZeeCRTight' >| make_VBF_plots_all_dd_${j}_3P_`date +"%y-%m-%d"`.log 2>&1 &
    	# pids[${j}] = $!
    done

    # for  pid in ${pids[*]}
    # do
    # 	wait pid
    # done

    python python/make_stack_plots.py -mc 'all' -sr 'VBF'  -sr2 ${i} -t 'V05_v1ZeeCRTight' >| make_VBF_plots_${i}_${j}_`date +"%y-%m-%d"`.log 2>&1 &
    
done


