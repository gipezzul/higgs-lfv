#!/bin/bash
#shopt -s expand_aliases
#list_dsids=(308092)

# export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# setupATLAS
# LCGENV_VER="LCG_95"
# LCGENV_ARCH="x86_64-centos7-gcc8-opt"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH ROOT"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH numpy"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH pip"

#inputs=('SM' 'Top' 'Zll' 'LFV' 'W_Jets')
#mc_tuples=('mc16a' 'mc16d' 'mc16e')
data_tuples=('data15' 'data16' 'data17' 'data18')
#data_tuples=('data17') # 'data18')
working_node="/home/gpezz/project"

for j in "${data_tuples[@]}"
do
    python python/eval_yields_V05.py -i test/inputs_V0401_SM.txt -o ${working_node}/plots_V05_v1_${j} -mc ${j} --realData True >| test_${j}_`date +"%y-%m-%d"`.log 2>&1 &
done


