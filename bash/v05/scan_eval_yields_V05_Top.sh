#!/bin/bash
#shopt -s expand_aliases
#list_dsids=(308092)

# export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# setupATLAS
# LCGENV_VER="LCG_95"
# LCGENV_ARCH="x86_64-centos7-gcc8-opt"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH ROOT"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH numpy"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH pip"

#inputs=('SM' 'Top' 'Zll' 'LFV' 'W_Jets')
inputs=('Top')
#mc_tuples=('mc16d' 'mc16e')
mc_tuples=('mc16a' 'mc16d' 'mc16e')
working_node="/home/gpezz/project"

for  i in "${inputs[@]}"
do
#    python eval_yields.py -i inputs_V0401_${i}.txt -o /project/hep/demers/gp364/plots_V0401_mc16e -mc mc16e >| test_${i}_mc16e_20191117.log 2>&1 &
    for j in "${mc_tuples[@]}"
    do
	python python/eval_yields_V05.py -i test/inputs_V0401_${i}.txt -o ${working_node}/plots_V05_v1_${j} -mc ${j} >| test_${i}_${j}_`date +"%y-%m-%d"`.log 2>&1 &
    done
done


