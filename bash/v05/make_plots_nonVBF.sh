#!/bin/bash
#shopt -s expand_aliases
#list_dsids=(308092)

# export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# setupATLAS
# LCGENV_VER="LCG_95"
# LCGENV_ARCH="x86_64-centos7-gcc8-opt"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH ROOT"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH numpy"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH pip"

#inputs=('SM' 'Top' 'Zll' 'LFV' 'W_Jets')
selections=('preselection')
#selections=('SR2')
# mc_tuples=('mc16d')
# mc_tuples=('mc16a' 'mc16d' 'mc16e')
eff_types=('3EtaBins')
#eff_types=('2D' '3EtaBins')
working_node="/home/gpezz/project"

for  i in "${selections[@]}"
do
    # for j in "${mc_tuples[@]}"
    # do
    # 	python python/make_stack_plots.py -mc ${j} -sr 'nonVBF'  -sr2 ${i} -mt 0 >| make_plots_nonVBF_${i}_${j}_`date +"%y-%m-%d"`.log 2>&1 &
    # # 	# pids[${j}] = $!
    # done

    # sleep 900

    for j in "${eff_types[@]}"
    do
    	# python python/make_stack_plots.py -mc ${j} -sr ${i} -sr2 'preselection'  --save-fakes True > /dev/null 2>&1 &
    	python python/make_stack_plots.py -mc 'all'  -sr 'nonVBF'  -sr2 preselection --eFF-version ${j} --data-driven True -mt 0 -t 'V05_v1'>| make_plots_nonVBF_dd_allData_eFF_${j}_`date +"%y-%m-%d"`.log  2>&1 &
	# sleep 4000
    done
    
    python python/make_stack_plots.py -mc 'all'  -sr 'nonVBF'  -sr2 'preselection'  -mt 0 -t 'V05_v1'>| make_plots_nonVBF_allData_${j}_`date +"%y-%m-%d"`.log 2>&1 &
done


