#!/bin/bash
#shopt -s expand_aliases
#list_dsids=(308092)

# export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# setupATLAS
# LCGENV_VER="LCG_95"
# LCGENV_ARCH="x86_64-centos7-gcc8-opt"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH ROOT"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH numpy"
# lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH pip"

#inputs=('SM' 'Top' 'Zll' 'LFV' 'W_Jets')
#inputs=(344775 344772 344779)
inputs=(308094 344772 344774 344775 344776 344778 344779 344780 344781 344782 364137 364138 364139 364140 364141 364210 364211 364212 364213 364214 364215)
mc_tuples=('mc16a' 'mc16d' 'mc16e')

for  i in "${inputs[@]}"
do
    for j in "${mc_tuples[@]}"
    do
	python eval_yields_test.py -i inputs_V0401_Ztautau_debug.txt --single-DSID ${i} -o /eos/user/g/gipezzul/plots_V0401_Ztautau_debug_dsid_${i}_${j} -mc ${j} >| test_${i}_${j}_20200122.log 2>&1 &
    done
done


